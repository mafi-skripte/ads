%!tex root = ./skript.tex


\chapter{$(a, b)$-Trees}

We will now see a different way to approximate
a perfect binary  tree. Recall that in AVL-trees, we have relaxed the requirement that
all leaves are at the same level, and we allow (controlled) differences in the
heights. In $(a,b)$-trees, however, we insist that all leaves are at the same level,
just as in a perfect tree.
Instead, to ensure more flexibility, we relax the requirement that
the tree must be  \emph{binary}. Now, a node can have more than two children, and
the number of children can vary among different nodes. 

We now describe the details.
There are in fact many different kinds of $(a, b)$-trees, 
depending on the choice of two parameters:
$a, b \in \mathbb{N}$. We require that $a \geq 2$ and that $b \geq 2a-1$.
Once $a$ and $b$ are chosen, we can define the conditions for a valid
$(a, b)$-tree:
\begin{itemize}
	  \item All leaves are on the same level.
	\item Different inner nodes can have a different number of children,
		according to the following rules:
	if the root is not a leaf, it has at least $2$ children and at most $b$ children.
         Every other inner node has at least $a$ children and most 
                  $b$ children.
        \item The entries are stored in the inner nodes and in the leaves.
		In an inner node, the number of entries is exactly the number of children minus one.
		In a leaf, the number of entries must be between $a-1$ and $b-1$, except if the
		leaf is also the root, in which case the number of entries must be between $1$ and $b - 1$.
         \item The children and entries in every inner node are ordered such that the 
		 \emph{generalized search-tree property} is fulfilled for every inner
		 node $v$ of the tree: 
write $v$ as
$v = (w_1, k_1, w_2, k_2, \dots, w_{\ell}, k_{\ell-1}, w_\ell)$,
where $w_1, w_2, \dots, w_\ell$ are the children of $v$,
and $k_1, \dots, k_{\ell-1}$ are the keys that are stored in $v$.
We assume that the keys and the children appear in this order in $v$.
Then, we require that (i) $k_1 < k_2 < \dots < k_{\ell-1}$; 
(ii) all keys in the subtree rooted at
$w_1$ are smaller in $k_1$; (iii) for $i = 2, \dots, \ell - 1$, all keys in the subtree rooted
$w_i$ are larger than $k_{i - 1}$ and smaller than $k_i$; and (iv) all keys in
the subtree rooted at $w_{\ell}$ are larger than $k_{\ell - 1}$.
\end{itemize}

\begin{center}
  \includegraphics{figs/02-ordereddict/26-ab tree definition.pdf}
\end{center}

Depending on the choice of $a$ and $b$, there are different kinds of $(a, b)$-trees.
Popular variants are $(2,3)$-trees and $(2,4)$-trees, which  can be used as alternatives
to AVL-trees. Another common variant is to choose $a$ and $b$ very large 
(e.g., a $(1024, 2048)$-tree)
and to store the tree in external memory. See below for more details.

First, we explain how the operations in an $(a,b)$-tree are implemented.

\paragraph{Lookup.} A lookup operation for a key $k$ in an $(a,b)$-tree $T$ is a straightforward
generalization of a lookup in a binary search tree. Starting at the root,
suppose we are currently at a node $v$ of $T$.

If $v$ is an inner node of $T$, 
let $k_1, \dots, k_{\ell - 1}$ be the keys in $v$,
ordered from smallest to largest.
If $k < k_1$, we go to the leftmost child $w_1$ of $v$.
If $k > k_{\ell - 1}$, we go to the rightmost child $w_\ell$.
Otherwise, 
we scan $k_1, \dots, k_\ell$, until we find the largest key $k_{i - 1}$ that
is smaller than or
equal to $k$. If $k_{i - 1} = k$, we return the value that is stored with $k$.
Otherwise, we go to the child $w_i$ that is stored to the right of $k_{i - 1}$. 

If $v$
is a leaf, we scan the entries in $v$ for $k$. If $k$ is present, we report
the corresponding value. Otherwise, 
we report that the key $k$ is not present.
The pseudocode is as follows:
\begin{verbatim}
get(k)
  v <- root  
  while v is not a leaf do
    write v as (w[1], k[1], w[2], k[2], ...., w[l-1], k[l-1], w[l])
    if k < k[1] then 
      v <- w[1]
    else if k > k[l-1] then
      v <- w[l]
    else if there is an i such that k[i-1] < k < k[i] then
      v <- w[i]
    else // there is an i with k = k[i] 
      return the value stored with k[i]
  // now v is a leaf
  if v contains k
    return the value stored with k
  else
    throw new NoSuchElementException
\end{verbatim}


\paragraph{Update.}
To update an entry $(k, \texttt{val})$ in an $(a,b)$-tree $T$, 
we use the lookup-procedure to locate the entry 
for the key $k$. If this entry is found, we update the value and return.
If not, the search ends in a leaf $v$ where the key $k$ should be located.
We add the entry  $(k, \texttt{val})$ to $v$. 

Now, if the leaf $v$ still has at most $b - 1$ entries,
there is nothing more to do. Otherwise, the leaf $v$ contains
exactly $b$ entries (because before creating the entry, there $(a,b)$-tree
invariant was fulfilled). We say that there is an \emph{overflow} at $v$. 
In this case, we perform a \emph{split}-operation
that splits the entries in  $v$ into two new nodes $v'$ and $v''$ and an entry $(k', \texttt{val})$, such
that (i) all keys in  $v'$ are smaller than $k'$; (ii) all keys in $v''$ are larger than
$k'$; and (iii) $v'$ and $v''$ each contain at least $a-1$ entries. The key $k'$ and references
to the new nodes $v'$ and $v''$ are inserted into the parent
node $p$ of $v$, replacing the reference to $v$. This operation may in turn lead to
and overflow  in 
the parent node $p$,
and we repeat the split operation on $p$. This process continues until we reach a parent node
that does not have an overflow, or until we reach the root. In the latter case, we create a new root
with a single entry, and $T$ grows by one more level.

Note that our requirements on $a$ and $b$ ensure that a split is always possible: to obtain
new nodes $v'$ and $v''$ and an entry $(k', \texttt{val})$ that fulfill the requirements
above, we need that $v$ contains at least $(a - 1) + (a - 1) + 1 = 2a -1$ entries. This is
the case, because an overflow occurs only if $v$ contains $b$ entries, and we require that 
$b \geq 2a - 1$.
The pseudocode is as follows:
\begin{verbatim}
put(k, val)
  v <- root  
  while v is not a leaf do
    write v as (w[1], k[1], w[2], k[2], ...., w[l-1], k[l-1], w[l])
    if k < k[1] then 
      v <- w[1]
    else if k > k[l-1] then
      v <- w[l]
    else if there is an i such that k[i-1] < k < k[i] then
      v <- w[i]
    else // there is an i with k = k[i] 
      update the entry for k with value val
      return
  // now v is a leaf
  if v contains k
    update the entry for k with value val
    return
  insert the entry (k, val) into v
  // now we need to split the nodes that overflow
  while v contains b keys do
    split v into v', k', v'' // see below
    if v is not the root then
      let p be the parent of v 
      insert v',k',v'' into p // see below
      v <- p
    else
      create a new root (v', k', v'')
      return
\end{verbatim}
The details of the split operation and of the insertion into the
parent node can be implemented as follows:
write $v = (w_1, k_1, \dots, w_{b-1}, k_b, w_{b+1})$ and
set $m = \lfloor (b + 1)/2 \rfloor$.
Then, the new nodes are given by $v'  = (w_1, k_1, \dots, w_m)$ and
$v'' = (w_{m+1}, \dots, w_{b+1})$, and
the middle key is $k'= k_m$.
The parent node of $v$ is updated as 
$p =  (\dots, k_{r-1}, v, k_r, \dots)
 \rightarrow (..., k_{r-1}, v', k', v'', k_r, \dots)$

\begin{center}
  \includegraphics{figs/02-ordereddict/26-put.pdf}
\end{center}

\paragraph{Deletion.}
To delete a key $k$ from an $(a, b)$-tree $T$, we first locate $k$ in 
$T$, using the lookup-method from above. If $k$ lies in an inner node, we determine
the predecessor $k'$  of $k$ by finding the largest
key in the subtree to the left of $k$,
we replace $k$ by $k'$, and we remove $k'$ from 
the leaf that contains it. Note that $k'$ must always be stored in a leaf (since
$k'$ is located by repeatedly following the rightmost child until
reaching a leaf, starting in the subtree to the left of $k$). Note also
that $k'$ must always exist (because if $k$ lies in an inner
node, there is a subtree to the left of $k$, and this subtree contains $k'$).

Thus, we can focus on the case that the key $k$ must be deleted from
a leaf node $v$ of $T$. For this, we simply remove the
entry for $k$ from $v$. Afterwards, if $v$ contains at least $a - 1$ entries,
there is nothing left to do.
Otherwise, if $v$ contains precisely $a - 2$ entries, we must find another 
entry that we can add to $v$, in order to restore the $(a, b)$-tree invariant.
We say that there is an \emph{underflow} in $v$.

To fix the underflow, we first consider the \emph{direct siblings} of $v$ in $T$,
i.e., the nodes that lie directly to the left or directly
to the right of $v$ in the parent node of $v$.
The node $v$
has at most two direct siblings.  If one of the siblings contains 
at least $a$ entries, we can use one of these entries to fix the underflow in $v$.
This is called a \emph{borrowing operation}.

\begin{center}
  \includegraphics{figs/02-ordereddict/26-borrowing.pdf}\\
  \vspace{0.5cm}
  \includegraphics{figs/02-ordereddict/26-subtrees warning.pdf}
\end{center}

The details of this step are given below.

If each direct sibling of $v$ contains only $a - 1$ entries, we need to \emph{join}
$v$ with a direct sibling. For concreteness, say that this is the right sibling $v'$
of $v$. We take the entry 
$(k', \texttt{val})$ in the parent of $v$ that separates $v$ from $v'$, and
we merge $v$, $(k', \texttt{val})$ and $v'$ into a node with 
$(a - 1) + 1 + (a - 2) = 2a - 2 \leq b - 1$ entries. This operation reduces the number of
entries in the parent node by one, so is is possible that we have created another underflow.
\begin{center}
  \includegraphics{figs/02-ordereddict/26-reverse split.pdf}
\end{center}
Thus, we proceed up the root, fixing the underflows with join operations, 
until we can either apply a successful borrowing operation, 
or until we reach the root.  If we perform a join operation with an entry
from the root, it may happen that the root becomes empty (does not contain any more entries).
In this case, we delete the empty root and make the (single) child of the old root the new
root of the tree.

\begin{center}
  \includegraphics{figs/02-ordereddict/26-root merge.pdf}
\end{center}


The pseudocode is as follows:

\begin{verbatim}
remove(k)
  find the node v that contains k (as above)
  if v is not a leaf then
    find the successor or predecessor k' of k // it does not matter 
                                              // which one we take
    replace k by k' in v
    let k <- k' and v <- the leaf that contains k'
  // now v is a leaf
  remove k from v
  // now we need to fix the underflow 
  while v contains a-2 keys and v is not the root do
    if v has a sibling with >=a keys then
      borrow a key from the sibling // see below
      return
    else
      // both siblings contain a-1 keys
      join v with a sibling // either left or right sibling is fine. See below
      v <- parent of v 
  if v is the root and v contains no keys then
    remove v and let the new root be the child of v
\end{verbatim}

We describe the details of a borrowing operation with the 
right direct sibling $v'$ of $v$:
suppose the parent node $p$ of $v$ and $v'$ contains  $(\dots, v, k'', v', \dots)$,
where $v = (w_1, k_1,  \dots, k_{a-2}, w_{a-1})$ 
and $v' = (w'_1, k'_1, \dots)$ has at least $a$ entries.
Then, we update
$v \leftarrow (w_1, k_1, \dots, k_{a-2}, w_{a-1}, k'', w'_1)$
and $v' \leftarrow  (w'_2, k'_2, \dots)$,
and $p$  changes to 
$p \leftarrow (\dots,  v, k'_1, v', \dots)$.
In words: the entry in $p$ between $v$ and $v'$ moves to  $v$,
the leftmost child of $v'$ becomes the leftmost child of $v$, and the leftmost entry
from $v'$ moves to $p$, between $v$ and $v'$.
A borrowing operation with the left direct sibling works analogously.

Next, we describe the details of a join
operation with the direct right sibling $v'$ of $v$:
suppose the parent node $p$ of $v$ and $v'$ contains $(\dots, v, k'', v', \dots)$,
and write  $v = (w_1, k_1,  \dots, k_{a-2}, w_{a-1})$
and $v' = (w'_1, k'_1, \dots, k'_{a-1}, w'_{a})$.
We join  $v$, $k''$,  and $v'$  to 
$v'' = (w_1, k_1, \dots, k_{a-2}, w_{a-1}, k'', w'_{1}, k'_{1}, \dots, k'_{a-1}, w'_{a})$.
The parent node $p$ changes to
$p \leftarrow  (\dots, v'', \dots)$.
In words,
we concatenate $v$ and $v'$, and the entry that separates $v$ from $v'$ in $p$
moves down.
A join operation with the left direct sibling works analogously.

Attention: If the parent node $p$ has two children (and hence one entry), 
it may happen that a join operations leads to $p$ being empty (containing no entries),
with exactly one child.
In this case, if $p$ is the root, we can 
simply delete it.
If $p$ is in inner node (this may happen if $a = 2$), we proceed according to the
algorithm and restore the entry in $p$ using another borrowing or join operation.

\begin{center}
  \includegraphics{figs/02-ordereddict/26-removing example.pdf}
\end{center}

\paragraph{Analysis.}
We analyze the running time of the operations on an $(a, b)$-tree.
For this, just in the case of AVL-trees, we will compute the 
minimum and the maximum number of entries that an $(a, b)$-tree of
a given height $h$ can store.

The minimum number of entries is attained if every node in $T$ stores
as few entries as possible. That is, the root of $T$ stores only one
entry, and all other nodes store exactly $a-1$ entries.
If this is the case, the root has exactly two children (if it is not
a leaf), and every other inner
node has exactly $a$ children.
Thus, at level $0$, we have one node, at level $1$, we have
two nodes, and at every other level $i = 2, \dots, h$, we have 
$2 \cdot a^{i - 1}$ nodes, because then at each additional level the number of nodes
is multiplied by a factor of $a$. Multiplying the number of nodes with
the number of entries ($1$ for the root, $a - 1$ for every other node),
we obtain the total number of entries as
\begin{align*}
	&1 + 2 \cdot (a - 1) + \sum_{i = 2}^h 2 \cdot a^{i - 1} \cdot (a - 1)\\
	&= 1 + 2(a - 1) \cdot \sum_{i = 0}^{h - 1} a^i\\
	&= 1 + 2 \cdot (a - 1) \cdot \frac{a^h - 1}{a - 1}\\
	&= 2 \cdot a^h - 1.
\end{align*}

Similarly, the maximum number of entries is attained if every node
stores as many entries as possible, $b - 1$. Then, every node
has exactly $b$ children, and the number of nodes at level $i$,
for $i = 0, \dots, h$, if $b^i$, since we start with one root and
since the number of nodes increases by a factor of $b$ at every level.
Multiplying the number of nodes with
the number of entries $b - 1$,
we obtain the total number of entries as
\begin{align*}
	& (b - 1) \cdot \sum_{i = 0}^{h} b^i\\
	&=  (b - 1) \cdot \frac{b^{h + 1}- 1}{b - 1}\\
	&= b^{h + 1} - 1.
\end{align*}
Thus, for the number of entries $n_h$ that can be stored in  an $(a, b)$-tree
of height $h$,
we have
\[
	2 \cdot a^h - 1 \leq n \leq 
	b^{h + 1} - 1.
\]
Solving for $h$, we obtain the minimum and the maximum possible height that
and $(a, b)$-tree with $n$ entries can have:
\[
	n \geq 2 \cdot a^h - 1 \Rightarrow h \leq \log_a \frac{n + 1}{2} = O(\log_a n),
\]
and 
\[
	n \leq b^{h + 1} - 1 \rightarrow h \geq \log_b (n + 1) - 1 = \Omega(\log_b n).
\]

Considering the operations above on an $(a, b)$-tree $T$ that stores
$n$ entries, we see that each operation
visits a constant number of nodes at each level, and in each node $v$, an operation performs
a constant number of scans keys and child references that are stored at $v$.
Multiplying the maximum number of levels with the maximum number of entries that a node
can have, we see that every operation takes at most $O(b \log_a n)$ time.  On
the other hand, multiplying the minimum number of levels with the minimum number of
entries that a node can have, we see that for every $(a, b)$-tree, there are operations
that take $\Omega(a \log b_n)$ time.

\paragraph{B-Trees.} The variant of $(a,b)$-trees where we fix $b = 2a - 1$, for
a given parameter $a$ are called \emph{B-trees}. Historically, they were
developed before the general notion of $(a, b)$-trees. B-trees are applied
widely in computer science, e.g., to represent indexes in database management systems
or in file-systems (e.g., in the Linux-file system \texttt{btrfs} = \emph{B-tree file system}).

For large choices of $a$, B-trees (and $(a, b)$-trees) are useful for \emph{external data structures},
data structures for data that is so large that it cannot be stored in main memory but needs
to be kept in external memory (i.e., the hard disk). In this case, each node of the tree is stored
at a different location of the external memory, and every visit to a node requires a
read/write-operation on the external memory.  Accessing the external memory is much slower
than manipulating data in internal memory, so it is advantagous to keep the height of the
tree small (whereas, in comparison, the cost for scanning a large number of keys in a given node,
which can be done in internal memory, is negligible). This is exactly what B-trees achieve.
