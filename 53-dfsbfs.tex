%!tex root = ./skript.tex

\chapter{Searching a Graph}

We will now consider two algorithms to search through the
nodes of a graph $G$ in a systematic manner. The precise
task is as follows: given a graph $G = (V, E)$ and a starting
node $s \in V$, visit all nodes that can be reached from $v$.
This makes sense in both undirected and directed graphs,
and our algorithms work for both variants.

The two algorithms are \emph{depth-first search} (DFS) and 
\emph{breadth-first search} (BFS). In a DFS, we start from
$s$ and walk into the graph as far as possible, until there is no
new node to be discovered. Then, we back up to the most recent node
where an unvisited neighbor is still available, and we follow this path
until there are no new nodes, and so on. We continue until all nodes
have been explored. In contrast, a BFS tries to explore all directions
simultaneously: first, we visit all nodes that can be reached in one step
from $s$, then we visit all nodes that can be reached in two steps from $s$, and so on.
Again, we continue until all nodes have been visited.

We now give the details of the two algorithms. The DFS is usually implemented
recursively, using a function that calls itself to visit all the nodes. It can 
also be realized with an explicit stack, without recursion. For a BFS, we usually
maintain the nodes that need to be visited in a FIFO-queue.

\paragraph{Recursive DFS.}
\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/22-01_DFS}
	\end{center}
	\caption{DFS example.}
\end{figure}


There is a straightforward implementation of DFS that uses a recursive
function.
Every node $v \in G$ has an additional attribute
$v$\texttt{.found} of type Boolean.
The attribute 
$v$\texttt{.found}
indicates whether $v$ has already been visited by the
DFS.
Initially, we have
$v\texttt{.found} = \texttt{false}$ for all vertices  $v \neq s$,
and $s\texttt{.found} = \texttt{true}$. 
The function $\texttt{dfs}(v)$ explores all neighbors of $v$ and
immediately proceeds to every neighbor that has not been visited
before.

\begin{verbatim}
dfs(v)
  //process v
  // Recursively visit all neighbors of v
  // that have not yet been visited
  for w in v.neighbors() do
    if not w.found then
      w.found <- true
      dfs(w)
\end{verbatim}

The DFS is started by invoking the function $\texttt{dfs}(s)$.
Since every node is visited at most once, and since every
edge is inspected at most twice in the undirected case
and at most once in the directed case, the running time
is $O(|V| + |E|)$, if $G$ is given as an adjacency list.

\textbf{TODO: Add story about Ariadne and Theseus}
\textbf{TODO: Example}
\textbf{TODO: Check with MafI 1 for consistency}

\paragraph{Iterative DFS.}
We can also implement DFS \emph{iteratively}, using
an explicit stack.
The stack stores those vertices of $G$ that we have
already been visited, but for which we have not yes
inspected all the out-neighbors.
Together with each vertex $w$, the stack stores an \emph{iterator}
for the out-neighbors of $w$. This iterator is used to continue the traversal
of the out-neighbors when the DFS returns to the vertex.
Initially, we have $v\texttt{.found} = \texttt{false}$, for all $v \in V$.

\begin{verbatim}
// Initialize S
S <- new Stack
// process s
s.found <- true
S.push((s, s.neighbors()))
while not S.isEmpty() do
  (v, neighors) <- S.pop()
  // find the next unvisited neighbor
  // of the current node 
  while neighbors.hasNext() do 
    w <- neighbors.next()
    // if the neighbor has not yet been found,
    // go there
    if not w.found then
      w.found <- true
      S.push(v, neighbors)
      v <- w
      neighbors <- v.neighbors()
  // if there are no neighbors left, go back
\end{verbatim}

The stack corresponds to the ``Ariadne-thread'' that is maintained
by the DFS:: the nodes in \texttt{S}, from bottom to top, constitute 
the path from  $s$ to $v$ that has been traversed by the DFS.
Again, the running time of the iterative DFS is $O(|V| + |E|)$, if
$G$ is given as an adjacency list.

DFS is a simple and surprisingly effective algorithm to search a graph $G$.
It has many applications, and it forms the basis for many efficient grkaph algorithms.
This was first realized in a seminal paper by Robert E.~Tarjan from 1972.
In this class, we will not pursue this further, but these algorithms will be discussed
in more advanced classes.

Sometimes, DFS is also called \emph{backtracking}. This is also the name of a general
optimization technique that is closely related to DFS. In backtracking, we construct a solution
to an optimization problem by adding elements one by one. If we get stuck in this process, we discard
the last element of the solution, and try another one. We continue, until a solution is found.

We emphasize that DFS does not necessarily visit all nodes in $G$, only the nodes
that are \emph{reachable}  from $s$, i.e., those nodes $v \in V$ for which there
exists a (directed) path from $s$ to $v$ in $G$.

\paragraph{Breadth-first search.}
BFS is implemented using a queue. The queue stores all nodes that have been discovered
by the algorithm but that have not been visited yet. First, the queue contains only $s$.
Then, we add all nodes that are one step away from $s$, then all nodes that
are two steps away from $s$, and so on. We again use an attribute \texttt{found} for each
vertex $v \in v$ that indicates whether $v$ has already been visited by the BFS.
Initially, we have $v\texttt{.found} = \texttt{false}$ for all vertices  $v \in V$.
\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/22-02_BFS}
	\end{center}
	\caption{BFS example.}
\end{figure}



\begin{verbatim}
Q <- new Queue
for v in vertices() do
  v.found <- false
s.found <- true
Q.enqueue(s)
while not Q.isEmpty() do
  v <- Q.dequeue()
  //process v
  for w in v.neighbors() do
    if not w.found then
      w.found <- true
      Q.enqueue(w)
\end{verbatim}

Since every node is visited at most once, and since every
edge is inspected at most twice in the undirected case
and at most once in the directed case, the running time
is $O(|V| + |E|)$, if $G$ is given as an adjacency list.


