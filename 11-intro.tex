%!tex root = ./skript.tex

\chapter{Introduction}

\paragraph{Data and Algorithms.}
The main topic of this class will be ways to
deal with \emph{data}.

In particular, we will learn how to organize data in a
computer memory
(\emph{data structures}), and we will look at methods
to manipulate the data and to answer questions about
it efficiently (\emph{algorithms}).

Our goal will be not only to 
see how these methods work, but also to reason
about them rigorously and to develop ways for analyzing
their properties. These properties 
include, for example, the \emph{correctness} (does the method
always work?), the \emph{running time} (how fast is the method?), 
or the \emph{space usage} (how much memory is required?).
For our analysis, we will use mathematical methods and aim for 
rigorous proofs. As such, the topics of this
class belong to the field of \emph{theoretical} computer science,
but we will always try to keep the applications in mind.

\paragraph{Algorithms.}
Before we begin, let us take a closer look at the notion of an
algorithm and at ways to evaluate the quality of a given
algorithm. 

\textbf{Definition: }
An algorithm is a \emph{finitely described}, \emph{general}, and
\emph{effective} procedure that transforms an \emph{input} into an
\emph{output}.

Let us see what these terms mean:

\begin{itemize}
	\item \textbf{input/output}: an algorithm always receives an \emph{input} and
		produces an \emph{output}. Depending on the algorithm, the input can be
		of different types, e.g., a text string, a sequence of numbers, a graph,
		or a map, and the same holds for the output. Some algorithms produce only
		two types of output: \texttt{Yes} or \texttt{No}. Such algorithms are called
		\emph{decision algorithms}.
	\item \textbf{general}: an algorithm typically does not work only for a specific
		input, but it has  a large class of possible inputs, usually infinitely 
		many.\footnote{Of course, this is only in theory. In practice, there are
		limitations on the input size, due to the physical properties of the hardware
		and the time at our disposal. However, saying that there can be infinitely many
		inputs is a good mathematical abstraction for the fact that the inputs can
		be \emph{arbitrarily large}.}
		The algorithm must work for all of these inputs.
	\item \textbf{finitely described}: an algorithm has a finite description, either in
		a semi-formal (verbal description, pseudo-code) or a formal language (programming
		language, Turing machine). Ideally,
		the algorithm is very short and fits on a single piece of paper. Even though
		the algorithm is finite, it usually can deal with arbitrarily large
		inputs that can be much larger than the algorithm itself.
	\item \textbf{effective}: an algorithm usually works by prescribing a sequence of \emph{steps}
		on a given input. Each such step must be \emph{effective}, i.e., it must be possible
		to carry out theses steps in ``the physical world''.\footnote{Of course, what
		we can do in the physical world depends on the current state of our technology. 
		About 200 years ago, it was not possible to build precise electronic computers
		that we take for granted today. In the future, we may be able to construct quantum
		computers that allow for additional \emph{efficient}  operations that are not available
		today.}
\end{itemize}

A \emph{good} algorithm usually has (some of) the following properties:
\begin{itemize}
   \item \textbf{correct}: for every input, the
	   algorithm terminates after a finite number of steps, and it provides the specified result.
   \item \textbf{fast}:  for every input, the algorithm provides the answer
	   after a short time. Of course, we expect that the algorithm will take longer
   	as the inputs get larger, but this increase in the running time should be
		``reasonable''.
	\item \textbf{space efficient}: similarly to the previous point, the
		memory usage of the algorithm should be small and increase ``reasonably''
		as the input grows.
	\item \textbf{easy to understand}: it should not be too difficult to understand how and why a given
		algorithm works.
	\item \textbf{easy to implement}: it should not be too difficult to program the algorithm in a 
		given programming language.
\end{itemize}

How can we evaluate the quality of an algorithm?

\textbf{Possibility A: Benchmarking (using experiments).}
We run the algorithm on several ``representative'' input instances and
observe how it behaves (by measuring the running time, the
space requirement, etc.)

The big advantage of benchmarking  is that it gives the most direct information about how 
an algorithm behaves ``in the real world''.
However, there are several disadvantages. 
First, using only experiments, we cannot prove that an algorithm is correct
for every possible input. Experiments can only show definitely that an algorithm
is \emph{incorrect}. Second, it is not clear how we choose the
``representative'' input instances for our experiments. How do we
know that they are representative? For which scenario?
Third, when performing an experiment, we typically do not just 
evaluate the algorithm, but other factors also come into play,
such as the skill of the programmer who implemented the algorithm,
the quality of the programming language, the quality of the compiler,
the underlying hardware, etc.  Fourth, it is not clear how to compare
the results of different experiments over time, since a lot of factors
can vary. When we have a new algorithm that we would like to compare to
existing ones, how do we make sure that we can recreate the conditions of
the previous experiments?

Thus, even though benchmarking is a good approach, it has many drawbacks. 
For a satisfactory theoretical understanding of our algorithms,
we need to proceed differently.

\textbf{Possibility B: Theoretical Analysis (using Mathematics).}
To analyze our algorithm theoretically, it is sufficient to
have an abstract description of the algorithm in \emph{pseudo-code} or for an 
\emph{abstract machine model}. An actual implementation is not necessary. 

Typically, the goal of a theoretical analysis understand 
how the performance (running time, space requirement) of an algorithm behaves as
a function of the \emph{input size}. Thus, for each possible input size,
we would like to assign a number that represents the behavior of the algorithm for this
input size, and we analyze how this function behaves as the input size grows.
There are many different ways to define such a number, but typically we focus
on the \emph{worst-case performance} of the algorithm, that is, the maximum
possible running time/space requirement/etc.~for any given input size.

There are many different abstract machine models that can used to describe
an algorithm, e.g., Turing machines, $\lambda$-calculus, Markov algorithms,
formal grammars of Type-0, etc. For our purposes, the most useful abstract
machine model is the \emph{random access machine} (RAM). The RAM is a mathematical
abstraction of an actual (simple) computer with a small instruction set 
and infinite memory that is structured in cells and that allows for random access.

In principle, once we have formally specified the properties of a RAM, 
we can formulate our algorithm as code for the RAM and then,
for every given input, count the number of RAM instructions that the algorithm
executes (notably, this is done in Donald Knuth's famous books on \emph{The Art of
Computer Programming}). This gives a detailed theoretical picture on the performance
of the algorithm. However, this analysis can still be very cumbersome and may
be full of unnecessary details

Thus, we typically represent our algorithm in \emph{pseudo-code}, an abstraction
of an imperative programming language that allows for ``shortcuts'' when it helps
in understanding the algorithm. When analyzing the pseudo-code, we have an implementation
of a RAM in mind, and we expect that one line of 
pseudo-code corresponds to a constant number of RAM operations.

Thus, when analyzing the running time of an algorithm theoretically as a function of
the input size, constant factors do not matter, because they can depend on the specifics
of the pseudo-code or the chosen instruction set of the RAM. In order to not be distracted
by these effects (and also in order to be able to ignore terms that become less influential
as the input size grows), we use 
\emph{O-notation} to represent the performance concisely.

\textbf{Definition}: Let $f, g: \mathbb{N} \rightarrow \mathbb{R}^+$ be two functions.
Then, we write
\begin{itemize}
	\item $f(n) = O(g(n)$ if and only if there exists a constant $c > 0$ and a number 
		$n_0 \in \mathbb{N}$, such that for all $n \geq n_0$, we have
		$f(n) \leq c \cdot g(n)$.
	\item $f(n) = \Omega(g(n)$ if and only if there exists a constant $c > 0$ and a number 
		$n_0 \in \mathbb{N}$, such that for all $n \geq n_0$, we have
		$f(n) \geq c \cdot g(n)$.

	\item $f(n) = \Theta(g(n)$ if and only we have $f(n) = O(g(n))$ and $f(n) = \Omega(g(n))$.
\end{itemize}

\begin{center}
	\includegraphics{figs/01-intro/11-01-O_notation}
\end{center}

To conclude, the goal of a theoretical analysis of an algorithm is to derive
a function in O-notation that represents the worst-case performance of the algorithm
as the input size grows.
This  yields a short and easily understood performance measure for an algorithm, and
it allows for an easy comparison between algorithms..
However, this simplicity comes at a cost. The statement may be too concise, and it may hide 
too many details. We only analyze the worst-case, so it may be possible that for ``many interesting''
inputs, the algorithm is much better than our analysis suggests. Also, the O-notation gives only 
an \emph{asymptotic estimate} of the performance of an
algorithm, and it is thus only useful for inputs that are ''large enough''. 
Many efforts have been made to address these issues, and we will see some of them in later classes.
For now, however, this approach will be a very good way to obtain a first understanding of
how different algorithms behave.

\paragraph{Data structures and Abstract Data Types.}
A \emph{data structure} is a way to organize data in a computer memory in order
to allow for efficient operations on the data. Typically, for a given task,
there are many ways how the data can be organized, with different advantages and
disadvantages concerning, e.g.,  the memory requirement or the time needed for supporting 
different operations (where different choices can result in some operations being faster
and other operations being slower).

\emph{Abstract Data Types} (ADTs) are a way hide these choices behind a layer of abstraction,
separating the desired functionality from the choice of implementation.
To understand the motivation, we take a digression into the realm of good software design.

When undertaking a large programming task, our goal is to develop software that is
\begin{itemize}
  \item \textbf{easy to maintain:} when new features need to be added or when existing bugs should be fixed
	  it should be possible to effect these changes without the need to change too much
		of the existing code.
  \item \textbf{easy to understand:} when new people are added to the project (or when the original
	  developers revisit the project after a longer absence), it should be possible to
  		get a quit overview of the functionality and of the structure of code.
   \item \textbf{flexible:} it should be easy to change the functionality and to develop
	   different versions of the software.
   \item \textbf{free from bugs:} The software should not have any serious flaws. If there are
	   flaws (as will almost certainly be the case), they should have only a limited
		effect on the whole system, and it should be easy to locate and fix them
		once they are discovered.
\end{itemize}
The field of \emph{software engineering} tries to address these issues and to find ways to
develop better software.  One very important principle from software engineering that is
relevant in our context was identified in 1972 by David Parnas. It is called ``information hiding''
and calls for a strict separation of the code in different, isolated parts. Though controversial
at the time, it is now a well-accepted method in software development.

The idea of information hiding is to partition a large software project into multiple parts. 
Each part has a clearly defined purpose and a precisely defined \emph{interface} that 
allows it to communicate with the rest of the system. Crucially, the communication with the
outside happens \emph{only} through the interface. The interior of the part remains hidden 
and can be modified and exchanged at will, as long as the interface is not affected.

In the realm of data structures, this idea is specialized in the notion of an 
\emph{abstract data type}. 

\textbf{Definition:} An \emph{abstract data type} (ADT) is a collection of
data whose objects can be accessed only through a clearly specified interface
that provides a fixed set of operations. The implementation cannot be accessed
directly and can be replaced, if desired. The individual operations are specified
precisely by giving a precondition, and effect and a return value for each operation.

There are many different abstract data types, depending on the kind of data and on the
specific operations that are supported. In \emph{Konzepte der Programmierung}, we have
already encountered some examples of abstract data types, such as \texttt{Stack} (Last-In-First-Out
storage), 
\texttt{Queue} (First-In-First-Out storage) or \texttt{Priority Queue}.

By using abstract data types in our algorithms, we gain several advantages. First, 
it increases the safety of our code, because the inner workings of the data structure
cannot be disturbed from the outside by accidental programming errors. Second, we gain
flexibility, because it is easy to replace one implementation of an ADT by another, which makes
it easy to adapt our code to different needs. Third, ADTs make it easier to understand an algorithm,
because the make the design more modular and let us separate the data structure logic from
the rest of the algorithm. Fourth, ADTs are easy to use, because we only need to understand a
simple interface.

Since information hiding and abstract data types are by now well-accepted notions
in software design, most modern programming languages have constructs to support
these ideas, e.g., traits in Scala, interfaces in Java, or virtual classes in C++.

We will now talk about the implementation of two specific ADTs.

