%!tex root = ./skript.tex


\chapter{Splay Trees}

Splay trees are a \emph{self-organizing} data structure that tries
to adapt automatically to a sequence of operations. In AVL-trees,
restructuring is rare and  occurs only in order to enforce a fixed
balancing condition on the tree structure. In splay trees, the tree
is reorganized after \emph{every} operation, not just after \texttt{put}
and \texttt{remove}, but also after \texttt{get}. The main idea is that
if an operation accesses a node $x$ in a search tree $T$, then it may be likely that
$x$, or a node that is close to $x$, will be accessed not much later. Thus,
it can be helpful to reorganize $T$ such that $x$ becomes the new root of $T$,
because then subsequent operations can access $x$ very quickly.

In order to make $x$ the root, we can use the search path for $x$ in the
tree, since we have computed this path already when accessing $x$.
A natural idea would be to use the simple ($\ell$- and $r$-) rotations
that we know from AVL-trees, and to apply them repeatedly to the nodes 
along the search path (from the parent of $x$ to the root). In this way,
$x$ moves up the tree and becomes the new root. This is called the \emph{move-to-root}
heuristic. 

However, already simple examples show that this heuristic
does not result in a good performance. The main innovation of Sleator and Tarjan,
who presented the splay tree method in 1985, was to replace this simple
move-to-root heuristic by a slightly more elaborate \emph{splay}-operation.
This operation works amazingly well and leads to probably good guarantees.

\paragraph{The splay-operation.}
Let $T$ be a binary search tree with root $r$, and let $x$ be a node in $T$. The splay operation,
\texttt{splay}$(x)$, applies a sequence of local operations along the search path from $x$ to 
$r$ in order to move $x$ to the root of $T$. If $x$ is the root of $T$, there is nothing
to do. Otherwise, let $y$ be the current parent of $x$. There are three possible cases, depending 
on the situation of $y$ and $x$:
\begin{itemize}
	\item \textbf{zig-case}: If $y$ is the root of $T$, we apply an  appropriate simple
		($\ell$- or $r$)-rotation to $y$ in order to make $x$ the root of $T$.
		This case occurs only once and finishes the splay-operation.
		\begin{center}
			\includegraphics{figs/02-ordereddict/25-zig.pdf}
		\end{center}
	\item \textbf{zig-zig-case}: If $y$ is not the root of $T$, let $z$ be the parent of
		$y$ (and the grand-parent of $x$). If $y$ and $x$ are one the same side of
		their respective parents (i.e., either $y$ is the left child of $z$ and $x$ is
		the left child  of $y$, or $y$ is the right child of $z$ and $x$ is the right child
		of $y$), the \emph{zig-zig-case} applies: First, we perform an appropriate 
		rotation on $z$, to make $y$ the new root of the subtree of $z$, and then
		we rotate on $y$, to bring $x$ to the root. 
		\begin{center}
			\includegraphics{figs/02-ordereddict/25-zigzig.pdf}
		\end{center}
	\item \textbf{zig-zag-case}: Suppose again that $y$ is not the root of $T$, 
		and let $z$ be the parent of
		$y$. If $y$ and $x$ are one different sides of
		their respective parents (i.e., either $y$ is the left child of $z$ and $x$ is
		the right child  of $y$, or $y$ is the right child of $z$ and $x$ is the left child
		of $y$), the \emph{zig-zag-case} applies: First, we perform an appropriate 
		rotation on $y$, to make $x$ the new root of the subtree of $y$, and then
		we rotate on $z$, to bring $x$ to the root. 
		\begin{center}
			\includegraphics{figs/02-ordereddict/25-zigzag.pdf}
		\end{center}
\end{itemize}

We note that the only place where \texttt{splay}$(x)$ differs from the move-to-root heuristic lies
in the zig-zig-case: in this case, move-to-root would first rotate on $y$ and then on $z$. The main
trick of splay trees is to reverse this order, making sure that not just $x$, but also the
subtrees of $x$ move up the tree. Those nodes that were moved down in a single step
are moved up again in the next step(s), so splaying ensures that the nodes in the subtrees
of $x$ and along the search path to $x$ move much closer to the root, while the other nodes
only move down by a constant number of levels./

\paragraph{Splay tree operations.} The operations on a splay tree work like in a binary
search tree, with an additional splay step after each operation: in a \texttt{get}$(k)$ operation,
we follow the search path for the key $k$. If $k$ is present in the tree, we perform a splay
operation on the node that contains it. If $k$ is not present, we perform a splay operation
on the leaf-node that has the empty subtree where $k$ should lie (i.e., the last node
on the search path). For \texttt{put}$(k, v)$, the situation is similar: after locating/creating
the node $x$ for $k$, we perform a splay operation on $x$. For a deletion, we splay the
parent node of the node that was removed.

\paragraph{Analysis.} Unlike AVL-trees, splay trees do not enforce a global invariant on 
the structure of the tree. This comes at the benefit that we do not need to store any
additional information in the nodes (in AVL-trees, we must keep track of the heights of
the subtrees). 

However, throughout the lifetime of a splay tree, it can happen that the tree becomes 
very unbalanced.
It may well be the case that at some point a node lies at level $\Omega(n)$, and that
a single access and splay operation can take $\Omega(n)$ steps. However, these situations
are very rare, and they can occur only after a long sequence of operations that were very 
efficient.  Thus, even though it may happen that \emph{in the worst-case}, splay trees
are very slow in a single operation, \emph{in the amortized  sense} (that is, over a 
sequence of operations) splay trees behave very well. Let us mention here that the notion
of \emph{amortized analysis} of a data structure is a general concept that has
wide applicability in the design and analysis of data structures. We will look at it more
closely (and with precise definitions) later in this class.

Splay trees have been analyzed extensively, and there are many theoretical
results that make the statements from the previous paragraph precise. 
Here, we state (but do not prove) two theorems about splay trees that give an idea
of the kinds of results that are possible. They both consider only the case that
we have a splay tree that contains a fixed set of  $n$ entries and that we perform
\texttt{get}-operations to only these entries. The first theorem shows that (in the
amortized sense) splay trees are never worse than AVL-trees:
\begin{theorem}
Let $T$ be a splay tree that contains a set $S$ of $n$ entries, and suppose we perform
an arbitrary  sequence $\pi$ of $m$ \texttt{get}-operations to keys that appear
in $S$. Then, the total running time for $\pi$ is $O(m \log n + n \log n)$.
\end{theorem}
We can say even more: if there is structure in the sequence $\pi$ of operations,
then splay trees can learn how to exploit this structure. For example, if some
entries are accessed much more frequently than others, splay trees will adapt
to make accessed to the more popular elements faster. This is made precise in the
second theorem:
\begin{theorem}
Let $T$ be a splay tree that contains a set $S$ of $n$ entries, and suppose we perform
an arbitrary  sequence $\pi$ of $m$ \texttt{get}-operations to keys that appear
in $S$. For an entry $x \in S$, let $m_x$ be the number of times that
$x$ is accessed in $\pi$. Suppose that $m_x \geq 1$, for all
$x \in S$, i.e., that every entry is accessed at least once.
	Then, the total running time for $\pi$ is $O(m + \sum_{x \in S} m_x  \log \frac{m}{m_x})$.
\end{theorem}
The term $m/m_x$ becomes smaller as $m_x$ is larger. More precisely, we can think of $m_x/m$ as
the \emph{probability} that a randomly chosen \texttt{get} in the access sequence $\pi$ 
concerns the entry $x$. The larger this probability becomes (i.e., the more popular the entry $x$
is), the faster the query becomes. There are many more interesting properties of splay trees,
but these will be discussed in more advanced classes (together with the proofs for these two
theorems).

To conclude, splay trees are an easy and efficient variant of binary search trees. They are
easier to implement than AVL-trees and yield similar performance bounds. Even more,
since splay trees adapt in response to the query sequence, they can learn from and adapt
to structure that occurs in the input. The main drawback is that splay-trees provide
only amortized guarantees: a single operation can be slow, but an average operation in any
sequence of operations will be efficient.

In the next chapter, we will see yet another tree structure that yields good worst-case performance
and that is optimized for very large data sets.
