%!tex root = ./skript.tex

\chapter{Data Compression}

Now we consider the problem of representing a 
string over some alphabet $\Sigma$ ``efficiently''
in memory. 

For example, consider the alphabet $\Sigma = \{a, b, c, d\}$
and the string $w = aaaaabbaaacd$ over $\Sigma$.

What is a ``good'' way to represent $w$ in a computer?
For this, we first note that in today's computer systems,
data is represented in binary. That is, we need to find a
way to encode $w$ as a bit string (an element from $\{0, 1\}^*$)
that is ``good''. What exactly we mean by ``good'' depends on the
context, but here are a few typical goals that we would like to
achieve:

\begin{itemize}
  \item \textbf{Space efficiency:} The bit string that encodes
	  $w$ should be as short as possible.
  \item \textbf{Fast encoding and decoding:} If should be possible to
	  derive quickly the contents of $w$ from the bit encoding.
  \item \textbf{Uniqueness:} It should be possible to reconstruct $w$
	   from its bit encoding without ambiguities.
\end{itemize}

A simple encoding scheme would proceed like this:
suppose that $\Sigma$ consists or $k \geq 2$ symbols, and let
$\ell = \lfloor \log (k - 1) \rfloor + 1$. We number the elements
from $\Sigma$ arbitrarily from $0$ to $k - 1$. 
These numbers can be represented with $\ell$ bits. Now encode
each symbol with the $\ell$-bit binary representation of its number, filled
with leading zeroes. To encode the whole string $w$, we encode each symbol individually
with the corresponding bit string, and we concatenate the strings.

In our example, we have $k = 4$ and $\ell = 2$. We might number $a: 0, b: 1, c: 2, d: 3$
and assign the binary encodings $a: 00, b: 01, c: 10, d: 11$. The representation for 
$w = aaaaabbaaacd$ is then 
$00000000000101000000000111$.

This bit representation is called the \emph{block code}, and it has many advantages:
it is simple, regular, and fast. In many scenarios, it cannot be improved. This is why
it is used widely throughout computer science. However, with our example string $w$, one
could also imagine different, more efficient representations. 

One idea would be to abandon
the idea all the binary encodings for the symbols of $\Sigma$ need to have the same length.
Then, we could, e.g., use a shorter bit string to represent $a$, which occurs very frequently,
at the expense of using longer bit strings for, say, $c$ and  $d$, which occur only once.
For example, if we assign $a: 0, b: 10, c: 110, d: 111$, we could represent 
$w = aaaaabbaaacd$ as  $000001010000110111$, which needs only 18 instead of 26
bits. The disadvantage is that the encoding is less regular: without know the contents
of $w$, we cannot predict where in the bit string the, say, tenth symbol of $w$ will lie.
But for pure representation purposes, this is certainly more efficient than the block code.

Another idea would be to exploit patterns in the string $w$. For example, we notice
that $w$ contains long sequences where the symbol $a$is repeated (such a sequence
is called a \emph{run}. Instead of storing
$w$ explicitly, we could store the information that $w$ consists of 
$5$ a's, followed by $2$ b's, followed by $3$ a's, followed by $1$ c and $1$ d.
Again, the representation is not as explicit and regular as in the block code, but
if the input string contains many long runs, the space savings may be significant.

The field of computer science that explores these ideas in more details is called
\emph{data compression}.

\paragraph{Data compression.}
The general goal of data compression is this: we are given a string $w$ over some
alphabet $\Sigma$. The goal is to represent $w$ as a bit string that is as short
as possible, while retaining the goals of efficiency and generality 
for the encoding and decoding algorithms. The string $w$ can be of many different types:
it might represent text, program code, audio, pictures (photos and computer generated), 
videos, etc. Depending on the type of data, different approaches are possible:

\begin{itemize}
	\item \textbf{Vary encoding length:} Encode the symbols of $\Sigma$ individually
		by bit strings. Vary the length of the bit strings depending on the
		frequency with which a symbol occurs in $w$: more frequent symbols
		receive shorter bit strings, less frequent symbols receive longer bit strings.
	\item \textbf{Exploit redundancies:} Look for repeating patterns in $w$ and try to
		use shortcuts to represent them more efficiently. One such pattern
		are long \emph{runs} (the same symbol is repeated many times), and one may
		represent the pattern by simply storing the length of the run and the symbol.
		This method is called \emph{run-length encoding}, and it is often used to represent
		image data for images with few colors). Another pattern occurs if the same word
		or phrase is used very often in a text-file. Then, one might introduce a special
		symbol to represent this phrase, along with a dictionary that translates between
		these symbols and the corresponding phrases. This idea is exploited, e.g., by the
		Lempel-Ziv-Welch family of compression algorithms, and is one of the main
		methods for file compression.
	\item \textbf{Lossy compression:} In certain kinds of data (photos, videos, sound),
		it may be acceptable if some information gets lost in the compression, as long
		as the main features are preserved. In this case, we can save a lot of space
		by discarding ``unnecessary'' information. The main task is now to identify
		which information is relevant and which information is redundant. There are
		many ways to do this, typically exploiting mathematical operations like the
		Fourier transformation, the discrete cosine transformation, or the wavelet 
		transformation. This techniques are the main way to represent photos (e.g., JPEG),
		sound (e.g., MP3) and videos (e.g., MPEG-4).
\end{itemize}
In this class, we will look closer at Huffman-codes, a data compression method that exploits the first idea:
varying the coding length. The other methods are treated in later classes on data compression.

\paragraph{Prefix-free codes and Huffman codes.}
Before discussing Huffman-codes, we first need some basic definitions and facts about 
codes. Let $\Sigma$ be an alphabet, such that $\Sigma$ contains at least two symbols.\footnote{If 
$\Sigma$ has only one symbol, data compression is not too interesting, since then we could represent
each string by its length, leading to exponential space savings.}

A \emph{code} for $\Sigma$ is an injective function $C: \Sigma \rightarrow \{0, 1\}^+$ that
assigns to each symbol of $\Sigma$ a unique non-empty bit string.  The images of a code
are called \emph{codewords}.

For example, for code for the alphabet $\Sigma = \{a, b, c, d\}$ could be $C(a) = 0$, $C(b) = 10$, 
$C(c) = 110$, $c(d) = 111$.

Given an alphabet $\Sigma$ and a code $C: \Sigma \rightarrow \{0, 1\}^+$, we encode a string
$w \in \Sigma^*$ by concatenating the codes for the individual symbols in $w$:
if $w = \sigma_1 \sigma_2 \dots \sigma_\ell$, then 
$C(w) = C(\sigma_1) C(\sigma_2) \dots C(\sigma_\ell)$. This defines a function 
$\widehat{C}: \Sigma^+  \rightarrow \{0, 1\}^+$.

For example, using the code above, we would encode $w = aabc$ as
$\widehat{C}(w) = C(a) C(a) C(b) C(c) = 0010110$.

To have a useful code, it must be possible to reconstruct the original string
$w$ from the encoding $C(w)$. For this, the code needs to be \emph{uniquely decodable}, that
is, for any two distinct strings $w_1, w_2 \in \Sigma^*$, $w_1 \neq w_2$, it must
be the case that $C(w_1) \neq C(w_2)$. In other words, not only should the code
$C: \Sigma \rightarrow \{0, 1\}^+$ be injective, but also the
extended encoding function 
$\widehat{C}: \Sigma^+  \rightarrow \{0, 1\}^+$.

For example, let $\Sigma = \{a, b\}$ and let $C(a) = 0$ and  $C(b) =  00$. Then,
the code $C$  is not uniquely decodable, because $C(ab) = C(ba) = 000$, so the encodings of
ab and ba are identical, even though $ab \neq ba$.

A useful condition that ensures that a code is uniquely decodable is \emph{prefix-freeness}.
A code $C: \Sigma \rightarrow \{0, 1\}^+$ is called \emph{prefix-free} if for any
pair $\sigma, \tau \in \Sigma$ of distinct symbols in $\Sigma$, we have that neither
$C(\sigma)$ is a prefix of $C(\tau)$ nor $C(\tau)$ is a prefix of $C(\sigma)$.\footnote{A prefix-free
code is sometimes also called a \emph{prefix code}.}

For example, let $\Sigma = \{a, b, c, d\}$. Then, the code $C_1: \Sigma \rightarrow \{0, 1\}^+$ with
$C_1(a) = 00$, $C_1(b) = 101$, $C_1(c) = 11$, and $C_1(d) = 0101$ is prefix-free,
but the code $C_2 : \Sigma \rightarrow \{0, 1\}^+$ with
$C_2(a) = 010$, $C_2(b) =  0110$, $C_2(c) = 01010$ and $C(d) = 0$ is not.

\begin{theorem}
	Let $\Sigma$ be an alphabet and let $C: \Sigma \rightarrow \{0, 1\}^+$ be a prefix-free
	code. Then, $C$ is uniquely decodable.
\end{theorem}

\begin{proof}
We will prove the contrapositive of the statement: if $C$ is not uniquely decodable,
then $C$ is not prefix free.

Thus, suppose that $C$ is not uniquely decodable. Then, by definition, there are two
distinct strings $w_1, w_2 \in \Sigma^+$, $w_1 \neq w_2$, such that
$C(w_1) = C(w_2)$. Let $\sigma$ be the first symbol of $w_1$ and $\tau$ be the first
symbol of $w_2$. We can assume that $\sigma \neq \tau$, because if $\sigma = \tau$, we
can remove the first symbol from $w_1$ and from $w_2$, and we would still get two
words $w_1 \neq w_2$ with $C(w_1) = C(w_2)$.

Now, since $C$ is injective, $C(\sigma)$ and $C(\tau)$ must be different.
Furthermore, since $C(w_1) = C(w_2)$, it must be the case that 
$C(w_1)$ starts with both $C(\sigma)$ and $C(\tau)$. This implies that
$C(\sigma)$ is a proper prefix of $C(\tau)$, or vice versa. Hence, $C$ is
not prefix-free
\end{proof}

The proof also suggests an efficient greedy algorithm for decoding a bit string
$s \in \{0, 1\}^+$ that has been encoded with a prefix-free code $C$: we read
$s$ from the beginning, collecting bits until we have obtained a
codeword from $C$. We output the corresponding symbol, and we repeat this process
with the remaining bits. Since the code is prefix-free, we can be sure that when
we output a symbol, there is no other codeword that could have started at the
beginning of $s$.

\textbf{TODO}: Example

We emphasize that every prefix-free code is uniquely decodable, but not every uniquely decodable
code is prefix-free. For example, let $\Sigma = \{a, b\}$ and let $C: \Sigma \rightarrow \{0, 1\}^+$
with $C(a) = 0$ and $C(b) = 010$. Then, the code $C$ is not prefix-free ($0$ is a prefix of $010$),
but it is uniquely decodable (in an encoded bitstring, we can use the $1$'s to identify the codewords
for $b$. The remaining $0$'s must be the codewords for $a$).

Prefix-free codes naturally correspond to rooted binary trees: let $C: \Sigma \rightarrow \{0, 1\}^+$
be a prefix-free code, and represent all the codewords $C(\sigma)$, $\sigma \in \Sigma$, in 
an uncompressed trie $T$ (without adding the trailing \$-symbol). Since $T$ is over
the binary alphabet $\{0, 1\}$, it is a binary tree. We order the children such that the edges to the left
children are labeled with $0$ and the edges to the right children are labeled with $1$. Since 
$C$ is prefix-free, we can be sure that the leaves of $T$ correspond exactly to the codewords of $C$.
The tree $T$ is called the \emph{code tree} for $C$.

\paragraph{Huffman-Codes.} We can now formally state the algorithmic problem:
we are given an alphabet $\Sigma = \{\sigma_1, \dots, \sigma_n\}$, with $n \geq 2$,  and associated
\emph{frequencies} $h_{\sigma_1}, h_{\sigma_2}, \dots, h_{\sigma_n} \in \mathbb{N}_0$. 
The frequencies indicate
that we have a string in which $\sigma_1$ appears $h_1$ times, $\sigma_2$ appears $h_2$ times, etc.
The goal is to construct a prefix-free code $C: \Sigma \rightarrow \{0, 1\}^+$ such that
the \emph{total length} of $C$,
\[
	\ell(C) = 	\sum_{\sigma \in \Sigma} h_\sigma \cdot | C(\sigma) |
\]
is minimum among all possible prefix-free codes for $\Sigma$. Recall that $|C(\sigma)|$ denotes
the number of bits for $C(\sigma)$ and hence $\ell(C)$ is simply the number of bits
that are needed to encode a string that leads to the given frequencies. In the following,
we will also use the notation $\ell(T)$ to denote the \emph{total length} of a code tree $T$,
which is the total length of the prefix-free code that corresponds to $T$.

\textbf{TODO}: Example

The idea of the Huffman-algorithm is to construct a code tree for the desired code $C$
bottom-up, in a greedy fashion.

We create a leaf-node for each symbol in $\Sigma$, and we annotate the leaf-node with
the symbol and the corresponding frequency. In the following, we will not distinguish
between the leaf-node and the corresponding symbol, to make the notation easiert.

We choose two nodes $\sigma, \tau$ with the smallest frequencies (that is, 
in $\Sigma \setminus \{\sigma, \tau\}$ there is no symbol with a frequency that is smaller 
than $h_\sigma$ or $h_\tau$), and we join them together to a partial code tree. More precisely, 
we create a new
inner node $v$, and we make the node for $\sigma$ the left child of $v$ and the node for 
$\tau$ the right node of $v$. We annotate $v$ with the frequency $h_\sigma + h_\tau$.

We repeat this process, generalizing from nodes to trees: in each step, we choose two
partial code trees with smallest frequencies, and we unite them to a new partial code tree 
(by adding a new parent node), whose frequency is the sum of the frequencies for its subtrees.

The process ends once only one tree remains. This is a code tree for $\Sigma$, and we can 
read off a prefix-free code $C_H: \Sigma \rightarrow \{0, 1\}^*$. This prefix-free code
is called the \emph{Huffman code}. We will see that it 
is an optimal prefix-free code for $\Sigma$ and the
frequencies $h_\sigma$.

Before we can prove that Huffman codes are actually optimal, we first need two lemmas
that show that the first step of the Huffman algorithm is correct. More precisely, in the
first step, the Huffman algorithm takes two leaves with the smallest frequencies and 
unites them to a partial code tree. The lemmas state that there is
always an optimal code tree in which this partial code tree occurs.
First, we argue that there is always an optimal code tree in which
every leaf has a sibling node.

\begin{lemma}[Sibling lemma]
	\label{lem:sibling}
Let $\Sigma = \{\sigma_1, \dots, \sigma_n\}$ be an alphabet with $n \geq 2$, and let 
$h_{\sigma_1}, \dots, h_{\sigma_n} \in \mathbb{N}_0$ be associated frequencies.

There is an optimal code tree $T^*$ for $\Sigma$ and the
frequencies $h_\sigma$ such that every leaf in $T^*$ has a sibling (this sibling may be
an inner node or another leaf).
\end{lemma}

\begin{proof}
Let $T_1$ be an optimal code tree for $\Sigma$ and
the frequencies  $h_\sigma$. 
If every leaf in $T_1$ has a sibling, we set $T^* = T_1$ and
are done.
Otherwise, the tree $T_1$ contains a leaf  that does not have a sibling. We call this leaf
$\sigma$.
Since $n \geq 2$, the leaf $\sigma$ must have a parent node $w$, and $\sigma$ is the only
child of $w$. We remove the parent node $w$ from $T_1$, and we put the leaf $\sigma$ in its place.
We call the resulting tree $T_2$.
Then, $T_2$ is a valid code tree for $\Sigma$. In $T_2$, the length of the code word for $\sigma$ has
decreased by one, while all other code words are unchanged. Thus, 
we have $\ell(T_2) \leq \ell(T_1)$, and
 $T_2$ is also 
an optimal code tree for $\Sigma$ and the frequencies $h_\sigma$.\footnote{In fact, this situation can 
occur only if $h_\sigma = 0$, since otherwise we would get
	$\ell(T_2) < \ell(T_1)$, which is impossible
if $T_1$ is optimal.} 

Now, if every leaf in $T_2$ has a sibling, we set $T^* = T_2$ and are
	done. Otherwise, we repeat.
Eventually, this will terminate, because in
each step, a leaf moves closer to the root. In the end, we obtain an optimal
code  tree $T^*$ in which every leaf has a sibling, as desired.
\end{proof}

Second, we show that there is an optimal code in which the first two nodes
that are chosen by the Huffman algorithm are siblings.


\begin{lemma}[Exchange lemma]
	\label{lem:exchange}
Let $\Sigma = \{\sigma_1, \dots, \sigma_n\}$ be an alphabet with $n \geq 2$, and let 
$h_{\sigma_1}, \dots, h_{\sigma_n} \in \mathbb{N}_0$ be associated frequencies.
Let $\alpha, \beta \in \Sigma$ be two symbols such  that
$h_\alpha = \min_{\sigma \in \Sigma} h_\alpha$ and
$h_\beta = \min_{\sigma \in \Sigma \setminus \{\alpha\}} h_\sigma$,
that is, $\alpha$ has a smallest frequency and $\beta$ has a second smallest
frequency in $\Sigma$

Then, there exists an optimal code tree $T^*$ for $\Sigma$ and the frequencies $h_\sigma$ such
that $\alpha$ and $\beta$ occur as siblings in $T^*$.
\end{lemma}

\begin{proof}
By Lemma~\ref{lem:sibling}, there exists an optimal code
$T$ in which every leaf has a sibling.
Let $\gamma$ be a leaf in $T$ that has maximum
depth (i.e., no leaf in $T$ is deeper than $\gamma$).
By assumption, $\gamma$ has a sibling, and
because $\gamma$ was chosen with maximum depth,
the sibling must also be a leaf.  We call this sibling leaf $\delta$,
and we assume that the names 
are chosen such that
$h_{\gamma} \leq h_\delta$.
We exchange in $T_1$ the leaf for $\alpha$ with
the leaf for $\gamma$, and the leaf for $\beta$ with the leaf for $\delta$.
We call the resulting tree $T^*$. Then, $T^*$ is a valid code tree for $\Sigma$.

In $T^*$, the lengths of the codewords for $\alpha$ and $\beta$ may get larger.
In return,
the lengths of the codewords for $\gamma$ and $\delta$ may get smaller.
We will see that 
since $\alpha$ and $\beta$ are two symbols with the smallest frequencies,
the total length cannot get larger,  so that $T^*$ is still optimal.

More precisely, we will show by a calculation that 
$\ell(T^*) - \ell(T) \leq 0$.
For this, let $C$ be the code for $T$, and $C^*$ the code for $T^*$.
We have obtained $C^*$ from $C$ by  exchanging the codewords of $\alpha$ and $\gamma$, and
the codewords for $\beta$ and $\delta$. All other codewords are unchanged.
Thus, we have
	\begin{align*}
		&\ell(T_2) - \ell(T_1)\\ 
		&= 
		\sum_{\sigma \in \Sigma} h_\sigma \cdot | C^*(\sigma) |
		- \sum_{\sigma \in \Sigma} h_\sigma \cdot | C(\sigma) |
		& \text{(by definition)}\\
		&=
		\sum_{\sigma \in \Sigma} h_\sigma \cdot \left(| C^*(\sigma) | - |C(\sigma)| \right)
		& \text{(rearranging the sums)}\\
		&=
		\sum_{\sigma \in \{\alpha, \beta, \gamma, \delta\}} 
		h_\sigma \cdot \left(| C^*(\sigma) | - |C(\sigma)| \right)
		& \text{(only $\alpha, \beta, \gamma, \delta$ changed)}
		\intertext{By construction of $T^*$, 
we have $C^*(\alpha) = C(\gamma)$,
$C^*(\gamma) = C(\alpha)$, and
$C^*(\beta) = C(\delta)$,
		$C^*(\delta) = C(\beta)$.
		Thus,  this is}
		&= 
		 h_\alpha \cdot \left(|C(\gamma)| - |C(\alpha)|\right) +
		h_\beta  \cdot \left(|C(\delta)| - |C(\beta)|\right)\\
		& \phantom{= } + h_\gamma  \cdot \left(|C(\alpha)| - |C(\gamma)|\right) +
		h_\delta \cdot \left(|C(\beta)| - |C(\delta)|\right)\\
		&= 
		 h_\alpha \cdot (|C(\gamma)| - |C(\alpha)|) +
		h_\beta  \cdot (|C(\delta)| - |C(\beta)|)\\
		& \phantom{= } - h_\gamma  \cdot (|C(\gamma)| - |C(\alpha)|) -
		h_\delta \cdot (|C(\delta)| - |C(\beta)|)& \text{(switching signs)}\\
		&= 
		 (h_\alpha - h_\gamma) \cdot (|C(\gamma)| - |C(\alpha)|) \\
		& \phantom{= }  + 
		(h_\beta - h_\delta) \cdot (|C(\delta)| - |C(\beta)|).& \text{(rearranging)}
		\end{align*}
Since $\alpha$ is a symbol with minimum frequency, we have 
\[
	h_\alpha \leq h_\gamma \quad \Rightarrow \quad
h_\alpha - h_\gamma \leq 0.
	\]
Since $\gamma$ is a leaf of maximum depth in $T$, we have 
	\[
|C(\gamma)| \geq |C(\alpha)| \quad \Rightarrow \quad
|C(\gamma)| - |C(\alpha)| \geq 0.
\]
Together, this gives
\[
	(h_\alpha - h_\gamma) \cdot \left(|C(\gamma)| - |C(\alpha)|\right) \leq 0.
\]
Similarly, since $\beta$ is a symbol with second smallest frequency,
and since $h_\delta \geq h_\gamma$
 we have 
\[
	h_\beta \leq h_\delta \quad \Rightarrow \quad
h_\beta - h_\delta \leq 0.
	\]
Since $\gamma$ is a leaf of maximum depth in $T$, we have 
	\[
|C(\delta)| \geq |C(\beta)| \quad \Rightarrow \quad
|C(\delta)| - |C(\beta)| \geq 0.
\]
Together, this gives
\[
(h_\beta - h_\delta) \cdot (|C(\delta)| - |C(\beta)|).
\]
In total, we have 
\[
	\ell(T^*) - \ell(T) \leq 0 \quad \Rightarrow \quad
	\ell(T^*) \leq \ell(T).
\]
Thus, $T^*$ is optimal, and the lemma follows.
\end{proof}

We can now show that the Huffman-algorithm is indeed optimal.
\begin{theorem}
	\label{thm:huff_opt}
Let $\Sigma = \{\sigma_1, \dots, \sigma_n\}$ be an alphabet with $n \geq 2$, and let 
$h_{\sigma_1}, \dots, h_{\sigma_n} \in \mathbb{N}_0$ be associated frequencies.
Then, the Huffman-algorithm produces a prefix-free code that minimizes the total length
for the given frequencies, among all prefix-free codes for $\Sigma$.
\end{theorem}

\begin{proof}
The proof uses induction on $n$, the number of symbols in $\Sigma$.
For the base case, we have $n = 2$. In this case, $\Sigma$
consists of two symbols, say $\Sigma = \{\sigma, \tau\}$.
The Huffman-code for $\Sigma$ could be either $C_1$ with
$C_1(\sigma) = 0$ and $C_1(\tau) = 1$, or 
$C_2$ with $C_2(\sigma) = 1$ and $C_2(\tau) = 0$. In either case, both codewords
have length $1$, and 
the total length is $h_\sigma + h_\tau$. 
Let $C$ be an arbitrary prefix-free code for $\Sigma$. By definition of a code,
all codewords in $C$  must be nonempty, so they have length at least $1$.
Thus, the total length of $C$ is 
$h_\sigma \cdot |C(\sigma)| + h_\tau \cdot |C(\tau)| \geq h_\sigma + h_\tau$.
It follows that the Huffman-code is optimal.

For the inductive step, we go from $n - 1$ to $n$. 
Our inductive hypothesis states that for every alphabet with $n - 1$ symbols and 
for every sequence of associated frequencies, the Huffman-algorithm constructs an 
optimal prefix-free code. We need to show that the same holds fro every alphabet with
$n$ symbols and for every set of associated frequencies.

Thus, let $\Sigma$ be an alphabet with $n$ symbols, and let $h_\sigma$, $\sigma \in \Sigma$
be a sequence of associated frequencies. Suppose we run the Huffman-algorithm for $\Sigma$ 
and  the $h_\sigma$. Let $T_H$ be the resulting code tree, and let $\alpha, \beta$ be the
two symbols that are chosen first by the Huffman-algorithm. By Lemma~\ref{lem:exchange},
there is an optimal code tree $T_O$ for $\Sigma$ and the $h_\sigma$ such that $\alpha$ and
$\beta$ occur as siblings in $T_O$.

Now, we need to apply the inductive hypothesis. For this, let $\rho \not\in \Sigma$ be a new
symbol that does not occur in $\Sigma$. Let $\Sigma'$ be the alphabet is obtained by
removing $\alpha$ and $\beta$ from $\Sigma$ and by adding $\rho$. That is, 
\[
	\Sigma' = \Sigma \setminus \{\alpha, \beta\}.
\]
Furthermore, we set $h_\rho' = h_\alpha + h_\beta$, and  $h_\sigma' = h_\sigma$ for
all $\sigma \in \Sigma \setminus \{\alpha, \beta\}$. Then, $\Sigma'$ is an alphabet
with $n - 1$ symbols, and with associated frequencies $h'_\sigma$.

Now, we modify the tree $T_H$ as follows: we remove the children
for $\alpha$ and $\beta$. Since $\alpha$ and $\beta$ were chosen first by the Huffman-algorithm,
the leaves for $\alpha$ and $\beta$ are siblings in $T_H$, and they have a common parent node
$w$. Now, after removing the leaves for $\alpha$ and $\beta$, the parent node $w$ is a leaf,
and we label it with the new symbol $\rho$. We call the resulting tree $T_H'$. Then, $T_H'$ is
a code tree for $\Sigma'$. Even more, crucially, the tree $T_H'$ is actually a Huffman tree for
$\Sigma'$ and the frequencies $h_\sigma'$. This is because if we run the Huffman-algorithm 
on $\Sigma'$ and the frequencies $h_\sigma'$, it behaves in the same way as the Huffman-algorithm
on $\Sigma$ and the frequencies $h_\sigma$ after the first step.

Next, we modify the tree $T_O$ in the same way: since the leaves for  $\alpha$ and $\beta$
are siblings in $T_O$, we can remove them and turn their common parent into a leaf for the new
symbol $\rho$. We call the resulting tree $T_O'$. It is a code tree for $\Sigma'$.

We can now apply the inductive hypothesis. Since $T_H'$ is a Huffman-tree for $\Sigma'$ and
the frequencies $h_\sigma'$, we can conclude by induction that $T_H'$ is optimal. In particular,
since $T_O'$ is also a code tree for $\Sigma'$, this means that
\[
	\ell(T_H') \leq \ell(T_O').
\]
Now, by construction of $T_H'$ and $T_O'$, we have that $\ell(T_H) = \ell(T_H') + h_\alpha + h_\beta$,
and $\ell(T_O) = \ell(T_O') + h_\alpha + h_\beta$. Indeed, in both $T_H$ and $T_O$, the codewords
for $\alpha$ and $\beta$ have the same length, and in $T_H'$ and $T_O'$, they are replaced by
a single code word that is shorter by one bit and that has frequency $h_\alpha + h_\beta$.
Thus, we can conclude that
\[
	\ell(T_H) = \ell(T_H') + h_\alpha + h_\beta \leq
\ell(T_O') + h_\alpha + h_\beta  = \ell(T_O).
\] 
Since $T_O$ is an optimal tree, it now follows that $\ell(T_H) = \ell(T_O)$, and hence the
Huffman tree is also optimal. This concludes the proof.
\end{proof}

