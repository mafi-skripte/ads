%!tex root = ./skript.tex

\chapter{Hash Functions}

We will now take a closer look at hash functions.
Recall that given a set of keys $K$, and a table size $N$,
a hash function is a function $h: K \rightarrow \{0, \dots, N -1\}$
that maps every key from $K$ to a position in the hash table.

When defining a hash function, there are two main goals:
\begin{itemize}
	\item It should be possible to compute the hash function efficiently; and
	\item the hash function should behave like a ``random'' function. That
		is, for a ``typcial'' set $S$ of entries, the keys in $S$ should
              be distributed ``evenly'' over the hash table.
\end{itemize}

Typically, a hash function consists of two parts: a \emph{hash code} and a \emph{compression
function}:
\begin{itemize}
	\item \textbf{Hash code:} Given a set of keys $K$, a \emph{hash code}
		for $K$ is a function $\text{hc}: K \rightarrow \mathbb{Z}$ that
		converts the keys from $K$ to integer numbers.
	\item \textbf{Compression function:} Given a table size $N$, a compression
		function $c: \mathbb{Z} \rightarrow \{0, \dots, N-1\}$ is
		a function that maps (arbitrary) integer numbers to locations
		in the hash table.
\end{itemize}
Once a hash code $\text{hc}$ for $K$ and a compression function $c$ for a table size $N$ are chosen,
we can define the hash function as $h: k \mapsto c(\text{hc}(k))$.

The reason why we distinguish these two parts is a separation of concerns, to make
the design of our hash function modular. The \emph{hash code} depends only on the
set of keys. The main purpose of the hash code is to provide an encoding for our
keys as integer numbers, and we can use a single hash code for a given set of keys,
even if we need to implement multiple hash tables of different sizes. Since the hash
code maps a key to the integers, it can be (and typically is) injective.\footnote{Of course,
there are practical limitations to this claim, since actual computers do not work with
arbitrary integers but only with integers that can be represented by, say, 64 bits.
However, the number of possible 64-bit integers is huge, compared to the table
size, that we can typically ignore this issue in our considerations.}
The compression function does not depend on the keys, but only on the table size.
We can use the same compression function for different key sets, and it is possible
to choose the compression function dynamically when creating a new hash table
at runtime (whereas
the hash code is typically fixed when defining the key set, during the design phase).
It is in the compression function that we usually try to ensure that the hash function behaves
``randomly''.

\paragraph{Hash codes.} We give some examples of how a hash code code could be implemented,
for different kinds of key sets.

\begin{itemize}
	\item Suppose that $K$ is the set of integers (i.e.,
		in Scala, this would be $K = \texttt{Int}$).
		Then, there is nothing to do, we can directly use the key $k$ itself
		as a hash code for $k$: $\text{hc}(k) = k$.
	\item Suppose that $K$ is the set of floating point numbers (i.e., in Scala,
		this would be $K = \texttt{Float}$. Now, we need to do something
		to convert a key $k$ to an integer.
	        There are different ways how this could be done. The simplest method
		would be simply to round $k$ down to the nearest integer:
		$\text{hc}(k) = \lfloor k \rfloor$. However, this might not
		be a good choice, for example, if (for some reason) we expect that all of our 
		keys lie between $0$ and $1$. In this case, a slightly better method
		would be to first multiply $k$ with a sufficiently large number, 
		before rounding down to the nearest integer, e.g.,
                $\text{hc}(k) = \lfloor 1000 \cdot k \rfloor$. This makes it more likely
		that different keys receive different hash codes, and it could work
		quite well for certain situations. The most reliable method would
		be to consider the \emph{bit representation} of the floating point
		number $k$, and to interpret this bit representation as an integer,
		e.g. $\text{hc}(k) = \text{ieee}(k)$, where $\text{ieee}(k)$ yields
		the bit representation of the floating point number according to the
		IEEE standard.
	\item Suppose that $K$ is the set of strings over some \emph{alphabet} $\Sigma$.\footnote{An
		\emph{alphabet} is a non-empty finite set that is used to represent possible symbols
		in a String.} Typical cases would be $\Sigma = \{0, 1\}$ (the case of bit-strings),
		$\Sigma = \{A, B, C, \dots, Z\}$ (the usual Latin alphabet, with no additional
		characters),
		$\Sigma = \{C, G, T, A\}$ (the most common alphabet in computational biology),
		or $\Sigma = \text{UNICODE}$ (the most common standard for a wide set of
		characters and glyphs). 
		For example, typical strings over the Latin alphabet
                $\Sigma = \{A, B, C, \dots, Z\}$ would be ``HELLO'' or ''WORLD'',
		whereas typical strings over the binary alphabet $\Sigma = \{0, 1\}$
		would be ``1100101'' or ``10101`.

		To define a hash code for this case, we first pick an arbitrary,
		but fixed numbering $\| \cdot \|$ of the symbols in $\Sigma$ with
		the numbers from $0$ to $|\Sigma| -1$.
		For example, for the Latin alphabet, we could take 
		$\|A\| = 0, \|B\| = 1, \|C\| = 2, \dots, \|Z\| = 25$, or for the
		binary alphabet, we could take $\|0\| = 0, \|1\| = 1$. Now, 
		given a string $\tau$ over an alphabet $\Sigma$, we write
		$\tau$ as a sequence as symbols: $\tau = \sigma_0 \sigma_1 \dots \sigma_{\ell - 1}$,
		where $\ell$ is the total number of symbols in $\tau$. 
		For example, if $\tau = \text{HELLO}$,
		we have $\ell = 5$ and  
		$\sigma_0 = \text{H}, \sigma_1 = \text{E}, \sigma_2 = \text{L}, 
		\sigma_3 = \text{L}, \sigma_4 = \text{O}$.
		Then we define
		\[
			\text{hc}(\tau) = \sum_{i = 0}^{\ell - 1} \|\sigma_i\| \cdot |\Sigma|^i.
		\]
		In other words, we interpret $\tau$ as a ``number`` to base $|\Sigma|$, where the
		''digits'' are represented by the symbols of $\Sigma$. For example,
		\begin{align*}
			\text{hc}(\text{HALLO}) &= \|\text{H}\| \cdot 26^0 + 
					   \|\text{A}\| \cdot 26^1 +
					   \|\text{L}\| \cdot 26^2 +
					   \|\text{L}\| \cdot 26^3 +
					   \|\text{O}\| \cdot 26^4 \\
					   &= 7 \cdot 1 + 0 \cdot 26 + 11 \cdot 26^2 + 11 \cdot 26^3
					   + 14 \cdot 26^4\\ 
					   &= 6.598.443.
		\end{align*}
		This is an injective method for representing strings as numbers. We see
		that these numbers can become very big very quickly. In practice,
		the calculations are down with finite precision, e.g., with 64-bit integers,
		but the principle remains the same.
  \item We consider again the case that $K$ consists of Strings, but this time we 
	  focus on the binary alphabet, e.g., we assume that $K$ is the set of
	  bit strings.\footnote{Actually, since all data in a contemporary computer is represented
	  as bit strings, this is the most general practical case.}
	  Then, there is a different way to define a hash code, using \emph{cryptographic hash
	  functions}. Cryptographic hash functions are hash codes that are especially engineered
	  to map arbitrary bit strings to (large) integers (nowadays, 
	  typically of 256 or 512 bits) in a way 
	  that makes it very hard to find collisions. Even though we know that collisions must
	  exist (because there is an infinite number of finite bit strings, much more than $2^{256}$),
	  the range of $2^{256}$ possible values is huge and, without additional structure,
	  it will be very hard to find two bit strings that are hashed to the same position.
	  The idea is that cryptographic hash functions should behave randomly enough that there is
	  not enough structure to find a collision, while still being easy to compute. We know
	  of no constructions that provably have these properties, but there are many heuristic
	  candidates that are used in practice (e.g., SHA-2, SHA-3). 
	  However, these hash codes are relatively
	  slow compared to their (non-cryptographic) alternatives, which is why they are not used
	  in a typical hash table implementation.
\end{itemize}
In the JVM-context, objects must provide a function \texttt{hashCode} that is used in order
to  compute hash codes for storing the object in a hash table.

\paragraph{Compression function.} Now, we give some examples of possible compression functions,
for mapping an integer to a position in a hash table of size $N$.

\begin{itemize}
	\item The simplest way to implement a compression function 
		$c: \mathbb{Z} \rightarrow \{0, \dots, N - 1\}$ is to use
		modulo arithmetic: $c(z) = z \bmod N$. This is very fast to 
		compute, and widely used in practice. However, this does not introduce
		much randomness into the process, and the distribution of keys is very
		predictable.
	\item A slightly more complicated way is to choose a prime number $p > N$ and to 
		compute the compression function as
		$c(z) = (z \bmod p) \bmod N$. This creates a more unpredictable
		pattern.  We choose $p$ as a prime number to ensure that there are no common
		non-trivial factors between $p$ and $N$, leading  to a more ``unpredictable'' pattern.
       \item We can extend the previous method to obtain a choice of compression functions that is
	       provably good. Here, we exploit the fact that we can choose the compression
  	       function when the hash table is constructed, and that we can use randomness in
		doing so: we choose a prime number $p > N$. When the hash table is constructed,
		we pick two random numbers $a \in \{1, \dots, p - 1\}$ and $b \in \{0, \dots, p - 1\}$,
		and we use the compression function
		$c(z) = ((a \cdot z + b) \bmod p) \bmod N$. This compression function is used
		throughout the whole lifetime of the hash table, but when a new hash table
		is created, new values $a, b$ are chosen. This choice of compression function
		is called \emph{universal hashing}.
	\item Another method that uses a random process during the construction
		of the hash table to determine the compression function is
		called \emph{tabulation hashing}. Tabulation hashing works
		for the situation where the hash code is presented
		as a finite bit string (as is usually the case). 

		For concreteness, suppose that
		the input hash code for the compression function 
		is given as a 64-bit string. Then, a possible tabulation
		hashing scheme works as follows: at construction time, 
		we create $8$ arrays $A_1, \dots, A_8$ 
		with $256$ entries each. We initialize these entries with $8 \cdot 256 = 2048$ random
		numbers, each chosen uniformly from $\{0, \dots N - 1\}$. 

		To compute the compression function, we proceed as follows: given an input hash code
		$z$ with 64-bits, we partition $z$ into $8$ blocks $B_1, \dots, B_8$ of $8$
		bits each. We can interpret the bits in each block as a number between 
		$0$ and $255$, which we use to index the array $A_1, \dots, A_8$. We look
		up the corresponding entries, and we take the logical XOR of the results. This
		gives a number between $0$ and $N-1$, the result of the compression function: 
		\[
			c(z) = A_1[B_1] \texttt{ xor } A_2[B_2] \texttt{ xor } \cdots \texttt{ xor } A_8[B_8].
		\]
		Of course, the choice of $8$ blocks of $8$ bits is arbitrary, and we could define
		the compression function in different ways, e.g., using $4$ blocks of $16$ bits
		each (in which case we need to prepare $4 \cdot 2^{16} = 262.144$ random entries)
		or using $16$ blocks with $4$bits each (in which case we need to prepare 
		$16 \cdot 2^4 = 256$ random entries). The idea of tabulation hashing is to approximate
		the notion that $h$ is a random function, but instead of storing a random function
		for all $2^{64}$ possible input values, we compose it of (smaller) random functions,
		with a trade-off between the number of random tables (more tables means
		less space) and the block size (larger block size means more randomness).

		Just like universal hashing, tabulation hashing leads to a construction of hash
		functions that is provably good for hash tables.
\end{itemize}
