%!tex root = ./skript.tex

\chapter{String Search}

Now, we turn to the problem of \emph{string search}.
This is the following problem:
let $\Sigma$ be an alphabet.
We are given a (relatively short) string
$t = \tau_1 \tau_2 \dots \tau_\ell$, the 
\emph{pattern}, and a (much longer) string $s = \sigma_1 \sigma_2 \dots \sigma_k$,
the \emph{text}. Both $s$ and $t$ are strings over $\Sigma$, and we have $\ell \leq k$.
The goal is to determine whether $t$ \emph{occurs as a substring} in $s$, and, if
so, to find the index of the first such occurrence. 
Formally, this means that we would like to find the smallest 
index $i$  between $1$ and $k - \ell + 1$ 
such that $\sigma_{i + j - 1} = \tau_{j}$, for all $j = 1, \dots, \ell$, or to
determine that no such index exists.



\paragraph{The naive algorithm.}
The string search problem can be solved by a very simple algorithm that is
immediate from the problem definition -- the \emph{naive} algorithm. 
For each index $i$ between $1$ and $k - \ell + 1$,
we check whether $t$ occurs as a substring in $s$ \emph{at position $i$}. For this,
we compare the characters of $s$, starting with $\sigma_i$, with the corresponding
characters in $t$, starting with $\tau_1$, one by one.
If two corresponding characters do not match, we stop and continue with the
next $i$ (this is called a \emph{mismatch}).
Otherwise, if all characters match, we know that $\sigma_i \dots \sigma_{i + \ell - 1} = t$,
and we report that the first occurrence of $t$ in $s$ is at index $i$.
If we find a mismatch for each index $i$, we report that $t$ does not occur in $s$.

The pseudo-code is as follows:
\begin{pascal}
for i := 1 to k - l + 1 do
    // Does s contain t at position i?
    j := 1
    while j <= l and s[i + j - 1] == t[j] do
        j++
    // If yes, return position i
    if j == l + 1 then
         return i
// Not found, return -1
return -1
\end{pascal}

The \pa|for|-loop of the naive algorithm has $O(k)$ iterations, and in each
such iteration, the \pa|while|-loop runs until the first mismatch is found.
Since in each step of the \pa|while|-loop we test check another character of $t$,
the number of iterations of the \pa|while|-loop is $O(\ell)$.
Thus, the  running time of the naive algorithm is $O(k\ell)$. 

This worst case can actually occur. For example, consider the case that $\Sigma = \{0, 1\}$
and that $s = 0^{k - 1} 1$ and $t = 0^{\ell - 1} 1$. That is, $s$ consists of $k - 1$ zeroes,
followed by a single $1$, and $t$ consists of $\ell - 1$ zeroes, followed by a single $1$.
Then, $t$ occurs as a substring in $s$, but only at the very end. However, each execution of
the \pa|while|-loop runs for $\ell$ iterations, because the mismatch occurs at the
very end of the pattern $t$. Thus, in the worst case, the naive algorithm has running time
$\Theta (k \ell)$. We will now see two algorithms that improve this.


\paragraph{Knuth-Morris-Pratt.}
When looking at the naive algorithm, we see that it has an obvious inefficiency:
whenever a mismatch is found, the naive algorithm simply goes to the next index $i$
and begins to compare $t$ with the substring $\sigma_{i} \sigma_{i + 1}\dots$ from
scratch. However, suppose that the \pa|while|-loop finds a mismatch after $j$ iterations.
Then, we have learned that $\sigma_{i} \dots \sigma_{i + j - 1} = \tau_1 \dots \tau_{j - 1}$.
Can we somehow take advantage of this information?

For example, suppose that $t = \text{STUDENT}$ and that $s = \text{STUDIENSTUDENT}$.
Then, the \pa|while|-loop will encounter the first mismatch when comparing 
$\sigma_5 = \text{I}$ to $\tau_5 = \text{E}$. Now, because we started comparing at $\sigma_1$ and
because we countered the first mismatch at $\sigma_5$, we have learned that 
$\sigma_1 \dots \sigma_4 = \tau_1 \dots \tau_4 = \text{STUD}$. Furthermore, the first
character of $t$, $\tau_1 = \text{S}$ does not occur anywhere else in $t$. Thus, from the
comparisons so far, we can not only conclude that the pattern $t$ does not occur at position
$1$ in $t$, but also that $t$ cannot occur at positions $2$, $3$, and $4$. Thus, 
we can save the comparisons between $\tau_1$ and $\sigma_2$, $\sigma_3$, and $\sigma_4$ that
the naive algorithm would make. Instead, we can continue with comparing
$\sigma_5$ to $\tau_1$, because the next possible index where $t$ can occur in $s$ is $5$
(this possibility was not excluded by the information that $\sigma_5 \neq \tau_5$).

Now, to generalize this idea, suppose that $t = \text{BARBARA}$ and $s = \text{BARBARBARBARA}$.
The first mismatch occurs when comparing $\sigma_7 = \text{B}$ to $\tau_7 = {A}$.
Now, we know that $t$ cannot occur at index $1$ in $s$. What is the smallest index where $t$
could occur? Since we have learned that 
$\sigma_1 \dots \sigma_6 = \tau_1 \dots \tau_6=\text{BARBAR}$, we can deduce that the first
possible occurrence of $t$ in $s$ can be at index $4$: this is the first position after $1$ where
there is a prefix of $t = \text{BARBARA}$ that goes until the end of substring of successful
comparisons. Thus, we can continue by comparing $\sigma_7 = \text{B}$ with
$\tau_{4} = \text{B}$. If we do this, the next mismatch occurs when comparing 
$\sigma_{10} = \text{B}$ with $\tau_7 = \text{A}$. Similarly to before, we have learned from the
comparisons so far that 
$\sigma_4 \dots \sigma_{10} = \tau_1 \dots \tau_6=\text{BARBAR}$. From this, we can now conclude
that the next possible occurrence of $t$ in $s$ can only be at index $7$, and we can continue by
comparing $\sigma_{11}$ to $\tau_4$. From here, no more mismatches occur, and we have discovered
$t$ in $s$ at index $7$.

Thus, the idea is as follows: when we encounter a mismatch, we use the information from the
successful comparisons to deduce the next possible occurrence  for $t$  in $s$. We adjust the
current character in $t$ accordingly and we continue at the same position in $s$ where the
mismatch occurred.

To formalize this idea, we need the following definition: 
\begin{definition}
Let $w, r \in \Sigma^*$. We say that $r$ is a \demph{boundary} of
$w$ is $r$ is both a prefix and a suffix of $w$. 
Trivially, the string $w$ itself and the empty string  $\varepsilon$
are always boundaries of $w$. A boundary $r$ of
$w$ is called \demph{proper} if $r \neq w$.
We use $\partial(w)$ to denote the longest proper boundary of $w$.
\end{definition}

For example, we have $\partial(\text{BARBAR}) = \text{BAR}$,
$\partial(\text{UNGLEICHUNG}) = \text{UNG}$, as well as
$\partial(\text{AABBAABBAA}) = \text{AABBAA}$.

Now, suppose that we encounter a mismatch when comparing $\sigma_i$ with $\tau_j$.
Then, if $j = 1$, we simply continue by comparing $\sigma_{i + 1}$ with $\tau_1$.
Otherwise, we know that $\sigma_{i - j + 1} \dots \sigma_{i - 1} = \tau_1 \dots \tau_{j - 1}$,
and the next possible match between
$\sigma$ and $\tau$ could start with $\partial(\tau_1 \dots \tau_{j - 1})$ at position
$i - |\partial(\tau_1 \dots \tau_{j - 1})|$. To continue, we need to compare
$\sigma_{i}$ with $\tau_{|\partial(\tau_1 \dots \tau_{j - 1})| + 1}$.

To implement this, we define a function bd as $\text{bd}(j) = -1$, if $j = 1$, and 
$\text{bd}(j) = |\partial(\tau_1 \dots \tau_{j - 1})|$, for $j = 2, \dots, \ell$.
Using bd, we can write pseudocode for the algorithm as follows:
\begin{pascal}
// we start with the first character in t
j := 1
for i := 1 to k - l + 1 do
    // if we have a mismatch, we use bd to try the next boundary 
    // of t[1:j-1]. We repeat until the first successful 
    // comparison or until we have  a mismatch with the first 
    // character of t
    while j > 0 and s[i] <> t[j] do
        j := bd[j] + 1
    // advance in t 
    j++
    // if we have reached the end of t, we return the index 
    // of the occurrence 
    if j == l + 1 then
        return i - l + 1
// Not found, return -1
return -1
\end{pascal}
To analyze the running time, we note that the \pa|for|-loop needs at most $k$ 
iterations. In each iteration of the \pa|for|-loop, we spend constant time,
plus the time for the iterations of the \pa|while|-loop.
The iterations of the while loop  
are due to mismatches $\sigma_i \neq \tau_j$. Every time we encounter a mismatch, the
pattern $t$ is moved to the right, i.e., the start index $i - j + 1$ of the pattern $t$ increases.
This can happen at most $k$ times, since this is the total length of $s$. Thus, the total running
time of the algorithm is $O(s)$, provide that the function $\text{bd}$ is known.

To determine $\text{bd}$, we can use a recursive strategy. By definition, we have 
$\text{bd}(1) = -1$ and $\text{bd}(2) = 0$. Now, suppose that we know $\text{bd}(i) = j - 1$,
for some $i$ between $2$ and $\ell - 1$.  This means that $\tau_1 \dots \tau_{j - 1}$  is the longest
boundary of $\tau_1 \dots \tau_{i - 1}$. Now, if $\tau_{j} = \tau_i$, then it follows
that we can just extend the boundary and that $\tau_1 \dots \tau_{j}$ is the longest boundary of 
$\tau_1 \dots \tau_{i}$. Thus, in this case,  we can set $\text{bd}(i + 1) = j$. On the other
hand, if $\tau_{j} \neq \tau_i$, we need to try the \emph{second} longest boundary of 
$\tau_1 \dots \tau_{i - 1}$ to see if it can be extended by $\tau_i$. The crucial observation
is that the second longest boundary of $\tau_1 \dots \tau_{i - 1}$ is the longest boundary
of $\tau_1 \dots \tau_{j - 1}$. This is already computed in $\text{bd}(j)$. Thus, we can  set
$j$ to $\text{bd}(j) + 1$, and we can continue by comparing $\tau_j$ to $\tau_i$.

\begin{pascal}
bd[1] := -1
bd[2] :=  0
j := 1
for i := 2 to l - 1 do
    while j > 0 and t[i] <> t[j] do
        j := bd[j] + 1
    bd[i+1] := j
    j++
\end{pascal}

This algorithm is actually almost identical to the string search algorithm above, with 
the main difference that the pattern $t$ is compared to itself. Thus, the running time
for computing bd is $O(\ell)$, and the total running time of the KMP-algorithm
is $O(k + \ell)$.

\paragraph{Rabin-Karp.}
An alternative method for the string search problem was proposed by Rabin and Karp.

The bottleneck in the naive algorithm is that in each iteration of the \pa|for|-loop,
we might  compare almost the complete substring
$\sigma_i \dots \sigma_{i+\ell-1}$ with $t$, because the mismatch can occur very late. 
Now, to  improve this,
we could first  perform a fast \emph{heuristic} test that indicates whether
it is \emph{likely} that 
$\sigma_i\dots \sigma_{i+\ell-1} = t$. This test should fulfill three properties:
(i) it should be \emph{fast}, much faster than comparing the substring and $t$
directly; (ii) it should be \emph{complete}, that is, if 
$\sigma_i\dots \sigma_{i+\ell-1} = t$, then the test should always be positive; and
(iii) it should be \emph{sound}, that is, if 
$\sigma_i\dots \sigma_{i+\ell-1} \neq  t$, then the test should only have a small probability
of being positive.

Such a test can be implemented by using a  \emph{hash function} $h: \Sigma^* \rightarrow \mathbb{Z}$. 
Such a  hash function assigns integer numbers to
$\sigma_i\dots \sigma_{i+\ell-1}$ and to $t$. These two numbers
can be compared quickly, in one step. Furthermore, a good hash function will
make collisions unlikely, so that it will happen only rarely that
two different strings
$\sigma_i\dots \sigma_{i+\ell-1}$ and $t$ receive the same hash value.
This leads to the following approach, the algorithm by Rabin-Karp.

\begin{pascal}
for i := 1 to k - l + 1 do
    // Quick test using a hash function
    if h(s[i...i+l-1]) == h(t) then
        while j <= l and s[i+j-1] == t[j] do
            j++
        // If yes, return position i
        if j == l + 1 then
            return i
// Not found, return -1
return -1
\end{pascal}

If collisions under $h$ are rare, the time for the comparison
$\sigma_i\dots \sigma_{i+\ell-1} \stackrel{?}{=} t$ after the heuristic test 
$h(\sigma_i\dots \sigma_{i+\ell-1}) \stackrel{?}{=} h(t)$
should be negligible.
However, as presented, the algorithm poses a new problem:
how to compute 
$h(\sigma_i \dots \sigma_{i + \ell - 1})$ quickly?
(If it takes too long to evaluate $h$, the whole
approach would be pointless).

As a reminder: when talking about hash functions,
we described the following way for computing a hash function $h'$
for a string 
$a = \alpha_0 \alpha_1 \dots \alpha_{\ell-1}$: pick a prime number $p$ and interpret the individual
symbols
$\alpha_i$ as numbers $\| \alpha_i\|$ between $0$ and $|\Sigma | - 1$. Then, set 
\[
  h'(a) = \left(\sum_{j=0}^{\ell-1} \|\alpha_j\||\Sigma|^{j}\right) \bmod p.
\]
For Rabin-Karp, it turns out to be advantageous to define the hash function slightly
differently:
\[
	h(\sigma_i\dots \sigma_{i+\ell-1}) = 
	\left(\sum_{j=0}^{\ell-1} \|\sigma_{i+j}\| |\Sigma|^{\ell-1-j}\right) \bmod p.
\]
There are two main differences between $h$ and $h'$:
\begin{enumerate}
	\item the index of $\sigma$ is increased by $i$ everywhere (because we are considering
		a substring of $s$); and
 \item the powers of $|\Sigma|$ are decreasing instead of increasing (this makes the next
	 formula slightly simpler).
\end{enumerate}
Now, the main point is that 
  \emph{given $h(\sigma_i\dots \sigma_{i+\ell-1})$, we can determine
  $h(\sigma_{i+1}\dots \sigma_{i+\ell}$) in $O(1)$ steps}.
Indeed, we have
\[
  h(\sigma_{i+1}\dots \sigma_{i+\ell}) = 
  \left(|\Sigma| \cdot h(\sigma_i\dots \sigma_{i+\ell-1})- |\Sigma|^\ell \cdot \|\sigma_i\| +
  \|\sigma_{i+\ell}\|\right) \bmod p.
\]
(Note that we can precompute $|\Sigma|^\ell$  in advance and can reuse it every time
we update the hash function). We call $h$ a \emph{rolling hash}.

From this, it follows that we can compute all the
hash values  $h(\sigma_1\dots \sigma_\ell)$, $h(\sigma_2\dots \sigma_{\ell+1})$, 
$\ldots$, $h(\sigma_{k-\ell + 1}\dots \sigma_\ell)$ as well as
$h(t)$ in total time $O(k + \ell)$.\footnote{We should make sure in every step
to take all the intermediate results modulo $p$, in order to make sure that we will
deal only with numbers that are small enough.}
Thus, if $h$ is a ``good'' hash function were collisions are rare, we expect
that the total running time for the Rabin-Karp algorithm is $O(k + \ell)$.
In fact, we can make this intuition precise.

\begin{theorem}
Suppose that $p$ is a random prime number between $2$ and $\ell^2 \log^2 (|\Sigma|\ell)$.
	Then, the expected running time of the Rabin-Karp algorithm is $O(k + \ell + \log |\Sigma|)$
\end{theorem}

\begin{proof}
First, we need $O(\ell)$ time to compute $|\Sigma|^\ell \bmod p$, $h(t)$, and 
$h(\sigma_1 \dots \sigma_\ell)$.

Now, for $i = 1, \dots, k - \ell + 1$, let $X_\ell$ be the random variable
that indicates the time that is needed for the $i$-th iteration of the \pa|for|-loop.
As discussed, we can  find  $h(\sigma_i \dots \sigma_{i + \ell - 1})$ in
	$O(1)$ time. Thus, we have that $X_\ell = O(1)$, if 
	$h(\sigma_i \dots \sigma_{i + \ell - 1}) \neq h(t)$, and
	$X_\ell = O(\ell)$, if 
        $h(\sigma_i \dots \sigma_{i + \ell - 1}) = h(t)$.

Thus, we need to estimate the probability that
$h(\sigma_i \dots \sigma_{i + \ell - 1}) = h(t)$, over the choice of $p$. 
	By construction, we have
$h(\sigma_i \dots \sigma_{i + \ell - 1}) = h(t)$ if and only if
	\begin{align*}
	\left(\sum_{j=0}^{\ell-1} \sigma_{i+j} |\Sigma|^{\ell-1-j}\right)
		&\equiv 
	\left(\sum_{j=0}^{\ell} \tau_{1 + j} |\Sigma|^{\ell-1-j}\right)
	\pmod p\\
		\Leftrightarrow
		\left(\sum_{j=0}^{\ell-1} \left(\sigma_{i+j} - \tau_{1 + j} \right) 
		|\Sigma|^{\ell-1-j}\right)
		&\equiv  0
	\pmod p
	\end{align*}
Now, let $A = \sum_{j=0}^{\ell-1} \left(\sigma_{i+j} - \tau_{1 + j} \right) |\Sigma|^{\ell-1-j}$.
Then, $A$ is a fixed number between $-|\Sigma|^\ell$ and $|\Sigma|^\ell$, and we have
that $A \equiv 0 \pmod p$ if and only if  $p$ is a prime factor of $A$. Now, we observe that
a natural number $n$ can have at most $\log n$ distinct prime factors: each prime factor is
at least $2$, and if $n$ has $k$ distinct prime factors, then $n \geq 2^k$.
Thus, it follows that 
$h(\sigma_i \dots \sigma_{i + \ell - 1}) = h(t)$ if and only if $p$ happens to be one
of the at most $\log |A| \leq \Sigma^\ell =  \ell \log |\Sigma|$ many prime factors of $A$.
Now, by the prime number theorem, the number of distinct prime numbers between $2$ and
$\ell^2 \log (|\Sigma|\ell)$ is $\Omega(\ell^2 \log |\Sigma|)$. Thus, the probability that a random
prime number between $2$ and 
$\ell^2 \log (|\Sigma|\ell)$ is a prime factor of $A$ is $O(1/\ell)$.
It follows that $\textbf{E}[X_i] = O(1)$. By linearity of expectation, the total running time
of the algorithm is $O(\ell + k)$.

It remains to explain how to find a random prime number between 
$2$ and $\ell^2 \log (|\Sigma|\ell)$. For this, we simply take a random
number between $2$ and $\ell^2 \log (|\Sigma|\ell)$ and check if it is a prime number.
If the test fails, we repeat. By the prime number theorem, the probability that we find a
	prime number is $\Omega(1/\log(\ell |\Sigma|))$. 
	Thus, we need $O(\log(\ell |\Sigma|))$ attempts in expectation to 
find such a number. There are very efficient algorithms testing whether a given number is a prime
number. Thus, the time for this step is negligible.
\end{proof}

The topic of string search is a vast topic in algorithms, and many other variants and solutions
for the problem exist. This will be covered in a later class, in particular in the classes
that deal with algorithmic bioinformatics.
 \Knut{There you will learn about the 
 Horspool-Algorithm as well as 
 bitvector-accelerated algorithms like the 
 Shift-Or Algorithm.}
