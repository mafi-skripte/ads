%!tex root = ./skript.tex


\chapter{Skip Lists}

\paragraph{Motivation.}
The main goal of skip lists is to implement the ADT ordered dictionary
with the help of a linked list, as we saw them in \emph{Konzepte der Programmierung}.
In \emph{Konzepted der Programmierung}, we saw an implementation of ordered dictionaries
using an unsorted and a sorted linked list. The implementation is relatively straightforward,
but the performance is not very good: to access an element of the list, we may need
to traverse almost the whole list, taking $\Omega(n)$ steps in the worst case. 

To improve this, we take an idea from public transportation. On some train routes in
Germany, there are two kinds of trains: the \emph{express train} 
that services only the ``important'' stops
(e.g., the EC81 that stops only at München Hauptbahnhof, München Ostbahnhof, Rosenheim, and Kufstein) and 
the \emph{local train} that stops everyhwere (e.g., the RB 54 that stops at München Hauptbahnhof,
München Ostbahnhof, Grafing, Aßling, Ostermünchen, Großkarolinenfeld and Rosenheim, Raubling,
Brannenburg, Flintsbach, Oberaudorf, Kiefersfelden and Kufstein). Thus, ignoring time-table issues,
and efficient way to get from München Hauptbahnhof to Brannenburg would be to take the
EC81 from München Hauptbahnhof to Rosenheim, and then the RB54 from Rosenheim to Brannenburg.

Thus, in public transportation, we have an option of \emph{skipping} certain stops by taking a 
different line, expediting the travel to our desired destination.
Can we find an analogue of this idea in the realm of linked lists?

\paragraph{Express list structure.}
In principle, this can be done in a relatively straightforward manner: we begin with a sorted
linked list $L_0$ that contains our entries, and we add two \emph{pseudo-nodes}: one at the beginning,
storing the key $-\infty$ (and no value), and one at the end, storing the key $\infty$ (and no value).
Then, we create the \emph{first express list} $L_1$ that contains the pseudo-nodes and a node for 
every second key from $L_0$ (the complete entries that contain also the values are stored only in
$L_0$), a \emph{second express list} $L_2$ that contains the pseudo-nodes
and a node for every second key from $L_1$ (and hence for every fourth key from $L_0$),
a \emph{third express list} $L_3$ that contains the pseudo-nodes and a node for every second key 
from $L_2$ (and hence for every eighth key from $L_0$), and so on. We stop when our current express
list consists only of two pseudo-nodes.
Between consecutive lists, we create \emph{down-pointers} from each node in the
higher list to the corresponding node (with the same key) in the lower list.
These down-pointers allow us to change from one express list to the next lower one.

\begin{center}
	\includegraphics{figs/02-ordereddict/24-skiplist example.pdf}
\end{center}

Now, the look-up procedure for a key $k$ is as follows: we start in the highest express list from the
left pseudo-node, and we walk right until the last node whose key is smaller than or equal to
$k$. Then, we follow the down pointer to the corresponding node in the next express list. From 
there, we walk right until the last node whose key is smaller than or equal to $k$, and so on,
until we reach the node with the entry for $k$ in the lowest list $L_0$, or determine that
$L_0$ does not contain $k$ (pseudo-code is given below).

\begin{center}
	\includegraphics{figs/02-ordereddict/24-lookup.pdf}
\end{center}

We call this structure the \emph{express list structure}.
To analyze it, suppose that we would like to store $n$ entries.
First, we analyze the number of express lists. The first express list $L_1$
contains every second key from $L_0$. Thus $L_1$ contains $\lfloor n/2 \rfloor$
keys. The second express list contains every fourth key from $L_0$, i.e., 
$\lfloor n/4 \rfloor$ keys. In general, the $i$-th express list contains
every $2^i$-th key from $L_0$, i.e., $\lfloor n/2^i \rfloor$ keys.
The procedure stops for the first $i$ where 
$\lfloor n/2^i \rfloor$ is $0$, i.e., for the first $i$ where
$n/2^i$ is less than $1$. This is  exactly $i_\text{max} = \lfloor \log n \rfloor + 1$.
Thus, the total number of lists is $\Theta(\log n)$. During the look-up procedure, we visit
a constant number of nodes in each list (we make at most one step to the right), to the time
for a look-up is $\Theta(\log n)$.

Finally, we consider the total number of nodes in our lists. 
Let $|L_i|$ denote the total number of nodes in the list $L_i$.
Then, the total number of nodes is
\begin{align*}
	\sum_{i = 0}^{i_\text{max}} |L_i| &= \sum_{i = 0}^{i_\text{max}}  (2 +  \lfloor n/2^i \rfloor)
	&\text{(two pseudo-nodes and the keys)}\\
	&\leq 2 \cdot (i_\text{max} + 1)  +  \sum_{i = 0}^{i_\text{max}}  \frac{n}{2^i} 
	&\text{(splitting the sum, bounding $\lfloor \cdot \rfloor$ from above)}\\
	\\
	&\leq O(\log n) + n \cdot   \sum_{i = 0}^{\infty}  \frac{1}{2^i} 
	&\text{(bound on $i_\text{max}$, bound the sum from above)}\\
	\\
	&= O(\log n) + n \cdot 2 & \text{(geometric sum)}\\
	&= O(n). & \text{(the linear term dominates)}
\end{align*}
Thus, all the lists combined are larger only by a constant factor than 
the initial list $L_0$.

This express list structure looks very much like a perfect binary search tree, and just like
a perfect binary search tree, it is difficult to maintain under insertions and deletions: adding
or deleting a single entry can lead to significant changes in the overall structure, because we might
need to rebuild all the express lists completely.

\paragraph{Skip lists.}
However, there is a way to maintain the express list structure \emph{approximately}, by using
\emph{randomness}. In the express list structure, we insist that each express list $L_{i}$ contains
exactly every second key from the list $L_{i - 1}$ below it. Instead, suppose we have a fair coin that
comes up heads with probability $1/2$ and tails with probability $1/2$. Then, we can 
derive $L_{i}$  from $L_{i - 1}$ as follows: we create two pseudo-nodes that will be the first
and the last node of $L_i$. Then, we go through the keys in $L_{i - 1}$ from left to right, and
for each key $k$ we flip our coin. If the coin comes up heads, we create a node for $k$in $L_i$.
If the coin comes up tails, we skip $k$. Then, we \emph{expect} that $L_i$ contains every second
key from $L_{i - 1}$, but there may be some errors due to the random process. Below, we will see that
this will not affect the (expected) running time. 

The main insight now is that
due to the independence of the coin flips, this procedure is completely equivalent to the following
procedure: for each key $k$ in $L_0$, we flip our coin until it comes up tails for the first time.
We count the number of heads $j$, that we see before this, and we add key to the express lists
$L_1, \dots, L_j$ (if $j = 0$, then  $k$ is stored only in $L_0$). Even more, if does not matter
in which order we perform this experiment for each key: if we add a new key $k$ to $L_0$ and perform
the coin flipping experiment for $k$, we get the same distribution on the lists as if $k$ hat been
there all along. If we delete a key $k$ from $L_0$ and all the express lists that lie above it,
we get the same distribution as if $k$ hat never been there. Thus, randomness gives us a way to
treat each entry individually and to still get a structure that is ``balanced'' overall (at least 
in some expected sense,  to be made precise below.

Thus, the main strategy is now clear: we implement a data structure that approximates the
express list structure, using randomness. Lookups work exactly as before, with the only difference
that we may look at more than two nodes in a single express list. To delete an entry, we perform
a search for the key and then remove the corresponding nodes from all lists in which the key occurs. 
The perform an insertion, we perform a lookup to determine the insertion locations in all
the lists. Then, we keep flipping a fair coin until the first occurrence of tails, and we insert
the key into as many lists as we have used coin flips (we insert always in the bottom list, and 
in as many express lists as the number of heads).

Next, we look at the details.


\paragraph{Implementation Details.}
In memory, a skip list consists of several linked lists. 
Each linked lists consists of nodes.
A node \texttt{n} is represented by an object that contains the following fields:
\begin{itemize}
  \item \texttt{k}, \texttt{v}: the key and value stored in this node,
  \item \texttt{pred}, \texttt{succ}: the predecessor and successor node
	  of \texttt{n} (in the same linked list, horizontally);
  \item \texttt{down}: the corresponding node to \texttt{n} in the list below 
	  (vertically). The \texttt{down}-pointer is NULL in the lowest list $L_0$.
\end{itemize}

Each list has two  \emph{pseudo-nodes}, one at the beginning and one at the end, 
storing the pseudo-keys
$-\infty$ and $+\infty$.
Initially, the skip list consists of a single linked list (the lowest list),
that stores only the two pseudo-nodes.

\paragraph{Obtaining a list of predecessors.}
First, we describe an internal helper operation that 
determines for a given key $k$ the list of predecessor nodes
for $k$ in all the constituent lists lists of the skip list.
More precisely, the function \texttt{search}$(k)$ produces a 
stack \texttt{Q} that contains the predecessor nodes for
the key  $k$, 
(or, if it exists, the nodes that contain the key $k$
from all lists that are part of the skip list
(ordered from the node in the lowest list to the node in the highest list).

The operation starts in the highest list and walks to the right until
the last node whose key is smaller or equal to $k$. Then, it stores this
node in the stack, and follows the \texttt{down}-pointer to the next list.
This continues until the lowest list.

\begin{verbatim}
search(k)
  Q <- new Stack
  n <- -INFTY pseudo-node of the highest list 
  do
    while n.next.k <= k do
      n <- n.next 
    Q.push(n)
    n <- n.down
  while n != NULL
  return Q 
\end{verbatim}

\paragraph{Lookup}
To perform a lookup for a key $k$, we use the operation \texttt{search}$(k)$.
W consider the top-most node in the resulting stack \texttt{Q} and check if
it stores the key $k$. If so, we can return the corresponding value. If not,
we know that they key $k$ does not occur.

\begin{verbatim}
get(k)
  Q <- search(k)
  n <- Q.pop
  if n.k == k then
    return n.v 
  else
    throw NoSuchElementException
\end{verbatim}


\paragraph{Insertion}

To perform an insertion, we first look for the key $k$, 
using \texttt{search}$(k)$.  If the key exists, we just
update the corresponding value. If not, we use the
stack of predecessor nodes that is returned by \texttt{search}
to insert $k$ into the express lists. The number of express lists
into which we insert is determined by flipping a coin, as described
above. It may happen that the coin shows heads more often than
the number of lists in our current structure. In this case, we 
create more express lists that are added at the top of the list.

\begin{verbatim}
put(k, v)
  Q <- search(k)
  n <- Q.pop
  if n.k == k then
    n.v <- v
    return
  insert a new node for (k, v) after n
  while coinFlip == heads do
    if Q.isEmpty then
      create a new list with -INFTY, a node for k, +INFTY
      create down links
    else
      n <- Q.pop
      insert a new node for k after n, add a down link
\end{verbatim}

\paragraph{Deletion.}
To perform a deletion, we first look for the key $k$, 
using \texttt{search}$(k)$.  If the key does not exist, we raise an
exception. Otherwise, we use the 
stack of nodes that is returned by \texttt{search}
to remove the node for $k$ from the express lists.
It may happen that some express lists at the top of the
structure become empty (consisting only of the pseudo-nodes). 
If so, they are removed.


\begin{verbatim}
remove(k)
  Q <- search(k)
  n <- Q.pop
  if n.k != k then
    throw NoSuchElementException
  while n != NULL and n.k == k do
    remove n from the list
    if !Q.isEmpty do
      n <- Q.pop
    else
      n <-NULL
  remove pseudonodes for empty express lists, if necessary
\end{verbatim}

The operations 
\texttt{pred}$(k)$, \texttt{succ}$(k)$,
\texttt{min} and \texttt{max} are left as an exercise.

\begin{center}
	\includegraphics{figs/02-ordereddict/24-inserting, removing example.pdf}
\end{center}

\paragraph{Analysis}. Suppose that we have a skip list $L$ that stores
$n$ entries. Since we use randomness when maintaining $L$, the exact structure
of $L$, and hence the performance guarantees for $L$ (i.e., running time of the
operations, space requirement) are \emph{random variables} whose exact value
depends on the random choices of the insertion algorithm. As such, we need
to rely on notions from probability theory to understand these random variables.
In probability theory, we see many different ways to analyze a random variable,
e.g., expectation, variance, tail bounds, etc. We will focus on the most
basic way: computing the \emph{expectation} of a random variable.

Thus, our goal will be to computed the \emph{worst-case} \emph{expected} performance of a
skip list with $n$ entries. That is, for every possible set of $n$ entries, we analyze
the expected value of the running time of the operations and of the space requirement, and
we take the maximum. This is a \emph{worst-case} guarantee, because we take the maximum
over all possible inputs, and it is an \emph{expected} guarantee, because for every
input we estimate the expected value.

First, we state that for any set of $n$ entries, 
the expected number of key-nodes that are stored in the skip list is $O(n)$.
Furthermore, the expected number of express lists is $O(\log n)$. The proof of these
two facts are left as an exercise.

Here, we will analyze the expected number of steps that are needed to locate
a key $k$ in the skip list.

\begin{theorem}
Let $S \subseteq K \times V$ a set of $n$ entries, and let $L$s be a skip list
that stores $S$. Let $k_0 \in K$ be a key. Then, the expected number of steps
	(to the right and down)
	to locate $k_0$ in $L$ is $O(\log n)$.
\end{theorem}
\begin{proof}
For $i = 0, 1, \dots$, let $L_i$ be the random variable that counts the number of
steps that the search performs in $L_i$. Note that we have infinitely many random
variables, because we cannot know in advance how many lists there are in $L$.
If a list $L_i$ does not appear in $L$, we set $L_i$ to $0$. 

Now, the total number of steps is $T = \sum_{i = 0}^\infty T_i$.
To compute the expected value of $T$, we can use linearity of expectation,
as follows:
	\[
		\textbf{E}[T] = \textbf{E}\left[ \sum_{i = 0}^\infty T_i\right]
		=
		\sum_{i = 0}^\infty \textbf{E}\left[ T_i\right].
	\]
We split the sum into two parts:
	\[
		\sum_{i = 0}^\infty \textbf{E}\left[ T_i\right] = 
		\sum_{i = 0}^{\lceil \log n \rceil - 1} \textbf{E}\left[ T_i\right] + 
\sum_{i = \lceil \log n \rceil}^{\infty} \textbf{E}\left[ T_i\right]. 
\]
We analyze the two sums separately. For the first sum, we consider the first $\lceil \log n  \rceil$
	lists of $L$. We argue that in each such list, we expected number of steps will be $O(1)$.
	This gives a total of $O(\log n)$ steps for the first sum.
	For the second sum, we will argue that it is increasingly unlikely that there is a list
	in $L$ that has level $i \geq \lceil \log n \rceil$. Thus, even if we bound
	the expected number of steps in such a list by its length, this will result in only
	a negligible contribution to the expected value.

	We begin with the second sum. Fix an  $i \geq \lceil \log n \rceil$.
	Let $S_i$ be the number of keys from $S$ that are stored in $L_i$ ($S_i$ is $0$
	if $L_i$ is not present). Then, the number of steps in $L_i$ is at most $2 S_i$
	(we take one step to the right for each key that is smaller than $k$, plus one step
	down, so at most $2$ steps for each key from $S$ that is present in $S_i$.
	Thus, using linearity of expectation, we have 
	\[
		\textbf{E} \left[ T_i \right] 
\leq \textbf{E} \left[2 \cdot S_i \right]
		 2 \cdot \textbf{E} \left[ S_i \right],
	\]
	and we must compute $\textbf{E} \left[ S_i \right]$. For this, we again use linearity
	of expectation. For each key $k'$ that appears in $S$, define an \emph{indicator random
	variable} $X_{k}$ to be $1$, if $k'$ appears in $L_i$, and $0$, otherwise.
	Then, we have $S_i = \sum_{k \in S} X_{k}$, and, by linearity of expectation,
	\[
		\textbf{E} \left[ S_i \right]  = 
\textbf{E} \left[ \sum_{k \in S} X_{k} \right] = 
	\sum_{k \in S}
\textbf{E} \left[ X_{k} \right].  
	\]
		Now, using the definition of an expected value and the definition of the
	random variable $X_k$, we have
	\[
		\textbf{E} \left[ X_{k} \right] = 0 \cdot \Pr [X_k = 0] + 
	1 \cdot \Pr [X_k = 1] = \Pr [X_k = 1] = \Pr [\text{$k$ appears in $L_i$}].
\]
The probability that $k$ appears in $L_i$ is exactly the probability of the even that
then inserting $k$, we our coin came up at least $L_i$ times heads. This probability
is exactly $1/2^i$, because this is the probability that when flipping a coin $i$ times,
we see $i$ heads. Thus, we have
	\[
		\textbf{E} \left[ X_{k} \right] = \frac{1}{2^i},
	\]
	and going back to $T_i$, we get
	\[
\textbf{E} \left[ T_i \right] 
\leq 2 \cdot \textbf{E} \left[ S_i \right] = 2 \cdot 
\sum_{k \in S} \textbf{E} \left[ X_k \right]  = 2 \cdot 
\sum_{k \in S} \frac{1}{2^i} = \frac{2n}{2^i},
\]
since $|S| = n$.
Hence, we can bound the second sum as
\[
\sum_{i = \lceil \log n \rceil}^{\infty} \textbf{E}\left[ T_i\right]
\leq
\sum_{i = \lceil \log n \rceil}^{\infty} 
\frac{2n}{2^i}
=
\frac{2n}{2^{\lceil \log n \rceil}}  \cdot
\sum_{i = 0}^{\infty} 
\frac{1}{2^i} \leq
\frac{2n}{n} \cdot 2 = 4,
\]
since $2^{\lceil \log n \rceil} \geq
2^{\log n}  = n$, and
$\sum_{i = 0}^{\infty} 1/2^i = 2$, by a geometric series.

Next, we bound the first sum. For this, fix an $i$ between $0$ and $\lceil \log n \rceil - 1$.
Our goal is to bound the probability $\Pr[T_i \geq j]$ that $T_i$,
the number of steps in $L_i$, is at least
$j$, for every $j \geq 1$. Then, we can 
estimate the expected value of $T_i$ using the well known formula
\[
	\textbf{E}[T_i] = \sum_{j = 1}^\infty \Pr[T_i \geq j]
\]
which holds for every random variable whose range are the natural numbers.

First, suppose that $i > 0$ (the situation for $T_0$ is only slightly different,
see below).
The search path takes always at least one step in $L_i$, namely
the down step from the last node. Thus, we have $\Pr[T_i \geq 1] = 1$.
Now, fix an integer $j \geq 2$, and
suppose that the search path 
takes at least $j$ steps in $L_i$. Consider the
last $j - 1$ nodes of the search path in $L_i$. Since the search path
enters each of these nodes by following at step in $L_i$, it must be
the case that none of these $j - 1$ nodes  contains a key that is also present in
$L_{i + 1}$ (because otherwise the search path would enter such a node through
a step from $L_{i + 1}$). The probability that this happens is at most $1/2^{j -  1}$,
since the coin flips in the insertion procedures 
are independent.\footnote{It may happen that this probability is $0$,
of the search path enters $L_i$ through a node that is too close to the left pseudo-node.}
Thus, for all $j \geq 1$, we have $\Pr[T_i \geq j] \leq 1/2^{j - 1}$.

For $T_0$, the situation is similar, except that the final down step does not exist.
Thus, we even get $\Pr[T_0 \geq j] \leq 1/2^{j}$, for all $j \geq 1$, and since
$1/2^{j} \leq ^/2^{j - 1}$, we can treat $T_0$ in the same was as the other $T_i$.

Thus, we get for all $i \geq 0$.
\[
	\textbf{E}[T_i] = \sum_{j = 1}^\infty \Pr[T_i \geq j] \leq 
	\sum_{j = 1}^\infty \frac{1}{2^{j - 1}} = 
	\sum_{j = 0}^\infty \frac{1}{2^{j}} = 2
\] 
using the geometric series formula.

In total, we get
	\[
		\textbf{E}[T] = 
\sum_{i = 0}^{\lceil \log n \rceil - 1} \textbf{E}\left[ T_i\right] + 
\sum_{i = \lceil \log n \rceil}^{\infty} \textbf{E}\left[ T_i\right]
\leq
\sum_{i = 0}^{\lceil \log n \rceil - 1} 2 +
4 
= 
2 \cdot \lceil \log n \rceil  +
4 
= O(\log n),
\]
as claimed.
\end{proof}

To conclude, skip-lists have the advantage of a relatively simple implementation and a running
time that is as good as AVL-trees. Possible disadvantages are that the running times are only
in the expected sense (there is always a certain probability that everything can be very slow),
and that we need additional space for the express lists, while, say, AVL-trees need only one
node per entry.

In the next chapter, we will get back to data structures that are based on binary search trees,
showing another way to relax the notion of a perfect binary tree to achieve an efficient implementation
of ordered dictionaries.
