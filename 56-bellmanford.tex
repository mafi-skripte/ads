%!tex root = ./skript.tex

\chapter{Shortest Paths with Negative Weights}

We now consider the SSSP-problem with negative edge weights.

\paragraph{Currency exchange.}
First, we consider an example where negative edge weights occur naturally:
suppose we have a set of $n$ currencies, $C_1, c_2, \dots, C_n$ (e.g., 
Euro, British Pound, Danish Kroner, Japanese Yen, Turkish Lira, etc.). 
For each pair of currencies $C_i$, $C_j$, we have an \emph{exchange rate}
$r_{ij} \geq 0$ that tells us how man units of currency $C_j$ we get for one
unit of currency $C_i$ (the exchange rate $r_{ji}$ is not necessarily related
to the exchange rate $r_{ij}$.\footnote{An exchange rate of $0$ tells us that
for some reason a direct change is not possible.}
Suppose further that we have one unit of currency
$C_1$, and we would like to change this to currency $C_n$. 

Of course, we can do this with a direct exchange, obtaining $r_{1n}$ units of
currency $C_n$. However, conceivably, there may be a better way of doing this.
Possibly, one can realize a better exchange rate by going through a longer 
sequence of exchanges.\footnote{In reality,
this would not be possible for a private person, because the transaction fees
would eliminate the advantage of a sequence of multiple transactions, and the 
rates could fluctuate too quickly for the transactions to go through on time.
For larger entities with a better access to the market, however, this may be 
a feasible strategy.}
\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/25-01-arbitrage}
	\end{center}
	\caption{Arbitrage.}
\end{figure}


In fact, we could be very lucky and discover a sequence of transactions that
starts and ends in the same currency and that results in more units of currency than 
we started with. This would, at least in theory, allows us to generate an infinite
amount of money.

Now, let us formalize this problem. We define a weighted directed graph $G = (V, E)$
whose vertex set $V$ is the set of currencies: $V = \{C_1, C_2, \dots, C_n\}$.
There is a directed edge $(C_i, C_j)$ for every pair of currencies that have
a positive exchange rate $r_{ij} > 0$: $E = \{(C_i, C_j) \mid r_{ij} > 0 \}$.
The edges are weighted by the exchange rates $r_{ij}$.
Now, a \emph{transaction sequence} $\pi$ from $C_1$ to $C_n$ is a sequence of
currencies $C_0 = C_{i_0}, C_{i_1}, \dots, C_{i_k} = C_n$ that starts
with $C_1$, ends with $C_n$, and such that there is an edge from $C_{i_{j - 1}}$
to $C_{i_j}$, for all $j = 1, \dots, k$. The \emph{overall exchange rate} of 
$\pi$, denoted by $r(\pi)$ is the product
\[
	r(\pi) = \prod_{j = 1}^{k} r_{i_{j - 1}i_j}.
\]
By assumption, we always have $r(\pi) > 0$.
The goal is to find a transaction sequence from $C_1$ to $C_n$ that 
\emph{maximizes} the overall exchange rate. This problem is very similar
to a shortest path problem, but not quite. There are two differences: (i) 
our goal is to \emph{maximize} the weight of a path, not \emph{minimize} it; and 
(ii) the weight of a path is the \emph{product} of the individual weights, not
the \emph{sum}. However, it turns out that these differences are only superficial,
and that we can interpret this problem as a shortest path problem in exactly the
sense that we have seen in the previous chapters (but with negative edge weights).

First, we address difference (i) and show how to go from a maximization problem
to a minimization problem. For this, we define a new weight function 
$r': E \rightarrow \mathbb{R}^+$
as
\[
	r'(C_i, C_j) = \frac{1}{r(C_i, C_j)}, \quad \text{for all $(C_i, C_j) \in E$}.
\]
Since our graph $G$ contains directed edges only for pairs of currencies with positive
weights, the weight function $r'$ is well defined. Furthermore, let
$\pi: C_0 = C_{i_0}, C_{i_1}, \dots, C_{i_k} = C_n$ be a transaction sequence 
from $C_1$ to $C_n$, and define the modified weight $r'(\pi)$ of $\pi$ as
\[
	r'(\pi) = \prod_{j = 1}^{k} r'(C_{i_{j - 1}}, C_{i_j}).
\]
By definition, we have
\[
	r'(\pi) = \prod_{j = 1}^{k} r'(C_{i_{j - 1}}, C_{i_j})
=
\prod_{j = 1}^{k} \frac{1}{r(C_{i_{j - 1}}, C_{i_j})} =
\frac{1}{\prod_{j = 1}^{k} r(C_{i_{j - 1}}, C_{i_j})} 
= 
\frac{1}{r(\pi)},
\]
and for any two transaction sequences $\pi$, $\pi'$, we have $r'(\pi_1) \leq r'(\pi_2)$
if  and only if 
$r(\pi_1) \geq r(\pi_2)$.

This means that $\pi*$ is a transaction sequence with \emph{maximum} weight with respect to $r$,
then $\pi*$ is a transaction sequence with \emph{minimum} weight with respect to $r'$.
By changing the weights from $r$ to $r'$, we have changed our original problem into an equivalent
minimization problem. This addresses (i).

To address (ii), we need to go from multiplication to addition. To do that, we again
define a new weight function $r'': E \rightarrow \mathbb{R}$ as
\[
	r''(C_i, C_j) = \log r'(C_i, C_j), \quad \text{for all $(C_i, C_j) \in E$}.
\]
Now, we may have negative edge weights, because the logarithm of a number strictly 
between $0$ and $1$ is negative. Let
$\pi: C_0 = C_{i_0}, C_{i_1}, \dots, C_{i_k} = C_n$ be a transaction sequence 
from $C_1$ to $C_n$, and define the weight $r''(\pi)$ of $\pi$ as
\[
	r''(\pi) = \sum_{j = 1}^{k} r''(C_{i_{j - 1}}, C_{i_j}).
\]
With these weights, we have
\[
	r''(\pi) = 
 \sum_{j = 1}^{k} r''(C_{i_{j - 1}}, C_{i_j})
=
\sum_{j = 1}^{k} \log{r'(C_{i_{j - 1}}, C_{i_j})} =
\log \left(\prod_{j = 1}^{k} r'(C_{i_{j - 1}}, C_{i_j})\right) 
= 
\log{r'(\pi)}.
\]
Since the logarithm is monotone increasing, it follows for any
two transaction sequences $\pi_1$ and $\pi_2$ that
$r''(\pi_1) \leq r''(\pi_2)$ if and only if $r'(p_1) \leq r'(p_2)$.
This means that a shortest path with respect to the weight function $r''$
is also a shortest path with respect to the weight function $r'$. So finding
an additive shortest path for $r''$ is equivalent to finding a multiplicative
shortest path for $r'$. Thus, we have also addressed (ii) and turned our original
problem into a classic shortest path problem with negative edge weights.\footnote{This
general process of transforming an instance of a new problem by stepwise modifications into
an equivalent instance of a  known problem is called a \emph{reduction}. We will learn
much more about reductions in \emph{Grundlagen der Theoretischen Informatik}.}

\textbf{Remark}: In the second step, we have seen a general trick that uses
the logarithm in order to turn multiplication into addition, based on the
well-known rule $\log (a \cdot b) = \log a + \log b$. In other words, instead
of multiplying two numbers, we can take their logarithms and then perform an addition.
This trick is used extensively in the field of artificial intelligence. There,
we often need to deal with probabilities, and it happens often that probabilities
need to be multiplied together. The resulting numbers can get very small very quickly,
leading to problems with floating point precision. Thus, we often deal with the logarithms
of the probabilities instead. This has two advantages: (i) we can use addition instead of
multiplication; and (ii) the magnitudes of the numbers involved do not get too small.

\paragraph{Bellman-Ford-Algorithm.}
Now, let us see how to solve the SSSP-algorithm for graphs with negative edges weights.
Let $G = (V, E)$ be a directed graph, and let $s \in V$ be the source node.
Let $\ell: E \rightarrow \mathbb{R}$ be a weight functions that may have
negative edge weights, and suppose that $G$ does not contain any negative cycles.

In this setting, Dijkstra's algorithm does not work. The reason is that
in Dijkstra's algorithm, we require that once a vertex $v$ is removed from
the priority queue, the shortest path to $v$ is known. However, this does not
need to be the case if we have negative edge weights. Now, there may be a shorter
path to $v$ that has a prefix that is longer than $d_G(s, v)$, followed by a negative
part. In this case, the shortest path to $v$ is discovered only after the prefix has
been computed, which may be after $v$ has been removed from the priority queue. In such
a case, it may be that shortest paths that involve $v$ are not computed correctly.
\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/25-02-Dijkstrafail}
	\end{center}
	\caption{Dijkstra fail.}
\end{figure}

To address this, we take a more abstract view of Dijkstra's algorithm, as follows:
for every vertex $v \in V$, we have two attributes: the \emph{tentative distance} $v.\texttt{d}$
and the \emph{tentative predecessor} $v.\texttt{pred}$. Initially, all tentative predecessors
are $\perp$, the tentative distance $s.\texttt{d}$ is $0$, and all other tentative distances
$v.\texttt{d}$ are $\infty$. Our goal is to slowly improve the tentative distances and the predecessors
until we have a shortest path tree for $s$. The main tool for this is a function
\texttt{improve}$(v, w)$, that can be called for any edge $(v, w) \in E$.
The function \texttt{improve} uses the fact that there is an edge from $v$ to
$w$ to improve our current guess for the shortest path to $w$, given our current
guess for the shortest path to $v$:
\begin{verbatim}
  improve(v, w):
      if v.d + l(v, w) < w.d then
          w.d <- v.d + l(v, w)
	  w.pred <- v
\end{verbatim}
If, according to our current guesses, it is better to first go to $v$ and then follow
the edge $(v, w)$, then we update the attributes for $w$ accordingly. Otherwise,
we do nothing.

We can think of Dijkstra's algorithm as a clever way to coordinate the calls to 
\texttt{improve}. We use a priority queue and the fact that the edge weights are nonnegative  
to call \texttt{improve} exactly once for every edge $(v, w) \in E$, namely at the time when we can
be sure that a shortest path to $v$ has been found.

If negative edge weights are allows, it is no longer clear how to identify this moment for
an edge $(v, w) \in E$ when the shortest path for $v$ has been found. But the solution is
simple: simply call \texttt{improve} \emph{multiple times} for each edge $e \in E$.
A simple strategy is to just call \texttt{improve} for every edge, and to repeat this until
no more changes take place. The pseudocode is as follows:
\begin{verbatim}
// Initialization, all distances are INFTY,
// all predecessors are NULL
for v in vertices() do
  v.d <- INFTY; v.pred <- NULL
// only s has distance 0
s.d <- 0
do
  // call improve on every edge
  for e = (v, w) in edges() do
     improve(v, w)
while at least one call to improve had an effect 
\end{verbatim}

\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/25-03-bellmanford}
	\end{center}
	\caption{Bellman-Ford.}
\end{figure}


Each iteration of the \texttt{do}-\texttt{while}-loop takes
$O(|E|)$ time. To analyze the correctness, and the running time,
we need to understand how many iterations are necessary until the
attributes converge (and to show that they actually correspond to shortest
paths). 
For this, we first need an invariant for the \texttt{do}-\texttt{while}-loop.
Recall from the previous chapter the definition of $\Pi(v)$ for a vertex 
$v \in V$: the path that is obtained by following the \texttt{pred}-pointers 
from $v$. With this definition, we can state and prove the invariant.

\begin{lemma}
	\label{lem:bf-invariant}
        The \texttt{do}-\texttt{while}-loop maintains the following invariant
	for all $v \in V$:
	\begin{enumerate}
		\item  we always have $v.\texttt{d} \geq d_G(s, v)$;
		\item  if $v.\texttt{d} = d_G(s, v)$, then $v.\texttt{d}$ and
	$v.\texttt{pred}$ do not change again; 
                 \item if $v.\texttt{d} = d_G(s, v)$ and if $d_G(s, v) < \infty$, then $\Pi(v)$ 
			 is a shortest path from $s$ to $v$,
			 and for all nodes $w \in \Pi(v)$, we have $w.\texttt{d} = d_G(s, w) < \infty$. 

	\end{enumerate}
\end{lemma}

\begin{proof}
	The invariant holds initially: for the start node $s$, we have 
	$s.\texttt{d} = 0 = d_G(s, s)$ and $\Pi(s) = s$, which is a shortest path
	from $s$ to $s$.
	For all other nodes 
        $v \in V \setminus \{s \}$,
	we have $v.\texttt{d} = \infty \geq d_G(s, v)$, and invariants (2) and (3)
	are vacuously true at the beginning.

	Now, we show that the invariant is maintained throughout the loop.
	For this, we need to consider the effect of an
	invocation of \texttt{improve}.
	Thus, we suppose that the invariant holds for all nodes in $V$,
	and that 
        we call the function \texttt{improve}$(u, v)$
	on an edge $(u, v) \in E$. 
	The call changes only the attributes of $v$, 
	and we show that it maintains the invariant.

        First, we consider Invariant~(1).
	Since only the attributes of $v$ can change in the
	call to \texttt{improve}, Invariant~(1) is maintained for all
	other nodes in $V \setminus \{v\}$, because it held before. 
	If the call does not change
	anything, this also holds for $v$. Thus, it remains to consider the case that
	the call changes the attributes of $v$, and we need to 
	show that the invariant is maintained for $v$. 
	If the attributes of $v$ change, then after \texttt{improve}$(u, v)$, we have
	\begin{equation}
		\label{equ:v_after}
	v.\texttt{d} = u.\texttt{d} + \ell(u, v).
	\end{equation}
	Since Invariant~(1) holds before the call,
        we know that the other endpoint $u$ fulfills
	\begin{equation}
		\label{equ:u_lower}
		u.\texttt{d} \geq d_G(s, u).
	\end{equation}
	Combining (\ref{equ:v_after}) and (\ref{equ:u_lower}), we get
	that after the call, we have
	\begin{equation}
		\label{equ:v_after2}
		v.\texttt{d} \geq  d_G(s, u) + \ell(u, v).
	\end{equation}
	Furthermore, the shortest path distance $d_G(\cdot, \cdot)$ fulfills
	\begin{equation}
		\label{equ:sp_triangle}
	d_G(s, u) + \ell(u, v) \geq d_G(s, v),
	\end{equation}
	because a
	shortest path from $s$ to $v$ is not longer
	than the path obtained by following a shortest path from $s$ to $u$
	and then taking the edge from $u$ to $v$. 
	Thus, combining (\ref{equ:v_after2}) and (\ref{equ:sp_triangle}), we
	get that after the call, we have
	\[
		v.\texttt{d} \geq d_G(u, v).
	\]
	This means that Invariant~(1) is also maintained for $v$.

	Second, we look at Invariant~(2).
	It is certainly maintained for all nodes
	in $V \setminus \{v\}$, because these nodes do not change.
	Furthermore, if $v.\texttt{d} = d_G(s, v)$ before \texttt{improve}$(u, v)$, 
	then $v.\texttt{d}$ and $v.\texttt{pred}$
	do not change: otherwise, the call \texttt{improve}$(u, v)$
	would strictly decrease $v.\texttt{d}$.  By Invariant~(1), 
	this is not possible if $v.\texttt{d} = d_G(s, v)$.

	Third, we consider Invariant~(3). First, we note that
	for all nodes $a \in V$, the following holds:
	suppose that before \texttt{improve}$(u, v)$, we have that (i) 
	$a.\texttt{d} = d_G(s, a) < \infty$ and  (ii) $\Pi(a)$ is a shortest path from $s$ to $a$
	with $b.\texttt{d} = d_G(s, b) < \infty$, for all nodes $b \in \Pi(a)$. Then, this also holds 
	after the call (by Invariant~(2), none of these attributes changes).
	Thus, we only need to consider the situation that 
         $v.\texttt{d} > d_G(s, v)$ before the call
	and $v.\texttt{d} = d_G(s, v)$ after the call. In this case, after the call,
	we have 
	\begin{align*}
		d_G(s, u) + \ell(u, v) &\geq d_G(s, v)  & \text{(by (\ref{equ:sp_triangle}))}\\ 
		&= v.\texttt{d} & \text{(by assumption)}\\
		&= u.\texttt{d} + \ell(u, v). & \text{($v.\texttt{d}$ was changed by \texttt{improve})},
	\end{align*}
	Subtracting $\ell(u, v)$, this gives  
	\[
		d_G(s, u) \geq u.\texttt{d}.
	\]
	By Invariant~(1), we also have 
	$d_G(s, u) \leq u.\texttt{d}$, and hence $d_G(s, u) = u.\texttt{d}$.
	Applying Invariant~(3) to $u$, we see that $\Pi(u)$ is a shortest path
	from $s$ to $u$ with $w.\texttt{d} = d_G(s, w)$ for all $w \in \Pi(u)$.. 
	After \texttt{improve}$(u, v)$, we have that 
	$\Pi(v)$ is obtained by extending $\Pi(u)$ by the edge $(u, v)$. This is a shortest
	path from $s$ to $v$, as claimed, and Invariant~(3) holds also for $v$.
\end{proof}

Now, the main insight is that after $i$ iterations of 
the \texttt{do}-\texttt{while}-loop, we have found all shortest paths that 
consist of at most $i$ edges: 

\begin{lemma} 
\label{lem:edgeinduction}
For all vertices $v \in V$, the following holds: 
suppose that there is a shortest path from $s$ to $v$ that consists 
of at most $i$ edges. Then, after $i$ iterations of the 
\texttt{do}-\texttt{while}-loop, we have that $v.d = d_G(s, v)$.
\end{lemma} 

\begin{proof}
The proof proceeds by induction on $i$, the number of iterations of the 
\texttt{do}-\texttt{while}-loop.

For the base case, we have $i = 0$, and we consider the situation right
before the first iteration of the \texttt{do}-\texttt{while}-loop.
The only node $v$ for which the shortest path from $s$ to $v$ consists
of $0$ edges is $v = s$, and by the initialization we have that 
$s.d = 0 = d_G(s, s)$.

Now, suppose that the claim holds after $i$ iterations of the \texttt{do}-\texttt{while}-loop,
and we need to show that it also holds after $i + 1$ iterations.
Thus, let $v$ be a vertex such that there is a shortest path $\pi$ from $s$ to $v$ that has
at most $i + 1$ edges. If $\pi$ has at most $i$ edges, then, by the inductive hypothesis,
the claim was already true before iteration $i + 1$ of the \texttt{do}-\texttt{while}-loop,
and by Invariant~(2), it  also holds after iteration $i + 1$.
Thus, suppose that $\pi$ has $i + 1$ edges, and let $(u, v)$ be the 
last edge of $\pi$, i.e., $u$ is the predecessor of $v$ on $\pi$.
Then, by the subpath-optimality property, the prefix of $\pi$ that
goes from $s$ to $u$ is a shortest path. This shortest path has
$i$ edges. Thus, be the inductive hypothesis, at the beginning
of iteration $i + 1$, we have $u.\texttt{d} = d_G(s, u)$. 
During iteration $i + 1$, we call
\texttt{improve}$(u, v)$, and after this call will, we can be  sure that 
$v.\texttt{d} \leq u.\texttt{d} + \ell(u, v) = |\pi| = d_G(s, v)$.  
By Invariant~(1), we always have $v.\texttt{d} \geq d_G(s, v)$,
so at the end of iteration $i + 1$, we have $v.\texttt{d} = d_G(s, v)$,
as claimed.
\end{proof}

The following theorem summarizes the properties of the Bellman-Ford algorithm.
\begin{theorem}
Let $G = (V, E)$ be a directed graph with weight function $\ell: E \rightarrow \mathbb{R}$,
such that $G$ contains no negative cycles. Set $s \in V$ be a source node.
Then, the Bellman-Ford algorithm terminates after at most $|V| - 1$ iterations
and computes a shortest path tree for $s$.
The running time is $O(|V| \cdot |E|)$.
\end{theorem}

\begin{proof}
For any node $v \in V$, a shortest path from $s$ to $v$ has at most $|V| -  1$
edges. Thus, the claim follows from Lemma~\ref{lem:edgeinduction}
and the fact that each iteration of the \texttt{do}-\texttt{while}-loop takes
$O(|E|)$ steps.
\end{proof}

The algorithm can also be adapted to find a negative cycle in $G$, if it exists.
\textbf{TODO:} Explain this.
