%!tex root = ./skript.tex

\chapter{Hash Tables}

To develop a fast an simple implementation for the ADT dictionary,
we start by considering a simple case: Suppose that our key set
$K$ consists of the integers $\{0, \dots, 99\}$, and suppose that
our values $V$ is the set of strings.
In this scenario, a simple implementation of a dictionary is as follows:
we create an array $T$ of Strings, with 100 positions.

Initially, all positions of $T$ are set to the null value, $\perp$.
Now, the idea is as follows:
for any key $k \in \{0, \dots, 99\}$, if there is an
entry $(k, v)$ for $k$ in $S$, we store the value $v$ 
at the array location $T[k]$. If there is no entry for $k$,
the array location $T[k]$ contains the null value $\perp$.

The dictionary operations can easily be implmeneted as follows:
\begin{verbatim}
  put(k, v): T[k] <- v
  get(v):    if T[k] != NULL then 
               return T[k] 
             else 
               throw NoSuchElementException
  remove(k): T[k] <- NULL
\end{verbatim}
All operations take $O(1)$ time,
the space requirement is $N$ plus the total size for all entries in the
data structure. The main drawback is that we need to
provide an array that has space for all possible keys. Thus, if the number 
of entries that we store is much smaller than the number of keys, we are wasting space.

Now, let us see what we can do if the key set becomes more complicated.
We look at some examples for inspiration:
\begin{itemize}
	\item Suppose that $K = \{1, 2, \dots, 100\}$. If we use the keys from 
		$K$ directly as indices in the table $T$, we waste space (position
		$0$ in $T$ would never be used).
		Thus, instead of storing the value for key $k$ in $T[k]$, we store
		$k$ in position $T[k - 1]$. We need to perform an additional operation,
		but the space is used optimally.
	\item Suppose that $K = \{-10, -8, -6, \dots, 0, 2, 4, \dots,  90\}$. 
		There are two issues: first, we cannot use the keys directly to 
		index the table $T$, because there are no negative indices. Second,
		we are again wasting space, because there all the keys are even numbers.
		The solution is similar to the solution before:
                instead of storing the value for key $k$ in $T[k]$, we store
		$k$ in position $T[(k + 10)/2]$. This is a somewhat more complex transformation,
		but it allows us to use the space in the table  $T$ optimally.
	\item Suppose that $K = \{\text{red}, \text{blue}, \text{yellow}, \text{cyan}, \dots,
		\text{black}\}$, for a (small) selection of colors.
		Now, the issue is that the keys are not numbers, so at first glance
		it is not clear how to use them to refer to places in the array $T$. 
		However, to represent the colors in our computer, we anyway need to use a numeric
		encoding. For example, one can encode the numbers using the \texttt{RGB}-encoding,
		which represents each color by giving the \emph{red}, \emph{green}, and \emph{blue}
		components of the color. If we encode each color component by, say, three bits (which
		is feasible for a small set of colors), we can encode the colors with 9 bits
		overall, and we can use a table $T$ with $2^9 = 512$ positions, which is quite
		feasible.
\end{itemize}
These three examples lead to the following insight: even if the key set $K$ is a bit more 
complicated, it is often possible to get our scheme to work. All we need is a way to map
the keys from $K$ to positions in the table $T$. Such a way is called \emph{hash function}.

\textbf{Definition}: Let $K$ be a set of keys, and let $N \in \mathbb{N}$ be a number.
A function $h : K \rightarrow \{0, \dots, N-1\}$ is called \emph{hash function} for $K$
(with table size $N$).

Now, the discussion so far shows: Suppose that given a set $K$ of keys and a number $N$ such that
we can find a hash function $h: K \rightarrow \{0, \dots, N-1\}$ that 
(i) is injective (i.e., different keys are mapped to different
table locations, $k_1 \neq k_2 \Rightarrow h(k_1) \neq h(k_2)$); and (ii) can be
evaluated quickly. Then, we have a fast and simple implementation for the ADT dictionary
with key set $K$ and arbitrary value set $V$. We create an array $T$ with $N$ entries,
and we implement the operations as follows:
\begin{verbatim}
  put(k, v): T[(h(k)] <- v
  get(v):    if T[h(k)] != NULL then 
               return T[h(k)] 
             else 
               throw NoSuchElementException
  remove(k): T[h(k)] <- NULL
\end{verbatim}
The running time for all operations is $O(1)$ plus the time to compute the
hash function. The space requirement is $N$ plus the total size for all entries in the
data structure.

Now, the question is how to extend this idea to more general scenarios.
The main requirements on the hash function $h$ are that $h$ is \emph{injective} and
that $N$ is \emph{small}. Typically, we cannot achieve both requirements at the same time.
It may well be the case that $K$ is very large (e.g., if $K$ consists of all 64-bit
integers) or even infinite (e.g., if $K$ is the set of all Strings). Then, we cannot
expect to obtain an injective function for reasonably small values of $N$ (i.e., values
of $N$ such that we are willing to create an array of size $N$ with potentially
many empty positions.) However, if $K$ is large, we typically do not want to store
entries for all possible keys in $K$, but our set of actual entries will be quite
small, compared to thee set of all possible keys $K$. Thus, there is still hope that
we can get away with hash-function that is not injective, because for the actual 
set of keys in our table, if $h$ is chosen well (and if $N$ is reasonably
large compared to the number of entries that are present in the table), 
the function $h$ could behave like a function
that  is ``almost injective''. Still, we typically  need to commit to a hash function before
we know the actual set of entries. Thus, we need to be prepared for dealing with the effects
of missing injectivity. For the, we use the following definition:

\textbf{Definition}: Let $K$ be a key set, $N \in \mathbb{N}$, and $h: K \rightarrow \{0, \dots, N-1\}$
a hash function. Let $k_1, k_2 \in K$, $k_1 \neq k_2$, be two distinct keys from $K$.
Then, we call the keys $k_1, k_2$ a \emph{collision} (for $h$) if and only if they hash to
the same location in the table, i.e., if $h(k_1) = h(k_2)$.

Thus, in the general setting, we must be prepared to handle collisions in our current
set of entries. There are different strategies how this can be done, with different names.
We briefly introduce these strategies. More details will be given in the following chapters.

\begin{itemize}
	\item First, we could store multiple entries at a given location of the
		table. That is, we simple store \emph{all} entries $(k, v)$ with $h(k) = i$ at 
		$T[i]$. For this, the fields of $T$ do not directly store the entries, but 
		they store (a reference to) an auxiliary data structure that contains
		all the entries that hash to the respective field. This auxiliary structure is
		typically a linked list. This collision resolution strategy is called
		\emph{chaining}.
	\item Second, if, upon inserting an entry $(k, v)$, we see that the location
		 $T[h(k)]$ in the hash table is already occupied, we can search
		 through $T$ for a ``close-by'' location that is still free. 
		 This strategy is called \emph{open addressing}, because the location
		 where an entry is stored is not fixed. There are different strategies
		 for finding the  ``close-by'' free location, with different names
		 (e.g., \emph{linear probing}, \emph{quadratic probing}, 
		 \emph{double hashing}).
	 \item Third, if the location $T[h(k)]$ for an entry $(k, v)$ is already
		 occupied, we could still insist on storing the entry $(k, v)$ in
		 this location. In this case, the entry that is already present will
		 have to move somewhere else. There are different ways to implement
		 this strategy, again with different names (e.g., \emph{cuckoo}, 
		 \emph{Robin Hood}, \emph{hopscotch}).
\end{itemize}
Note that once we allow for collisions, it is not enough to just store the
value in a given entry in $T$. Instead, we now need to store the whole
entry $(k, v)$, and during any operation, we must compare the given key
with the key in the entry that is store in the table (because it may be possible
that our current location is occupied by an entry whose key hashes to the
same location). This is where the requirement comes from that we can
check elements of $K$ for equality.

To summarize, a hash table consists of three parts:
\begin{itemize}
	\item an array $T$ with $N$ entries, where $N$ is called the \emph{size}
		of the hash table (and is typically chosen when creating the
		hash table);
	\item a suitable hash function $h: K \rightarrow \{0, \dots, N - 1\}$.
		The hash function depends on $N$, the table size, and
		it is typically chosen when creating the hash table; and
	\item a strategy for collision resolution.
\end{itemize}
In the next chapters, we will discuss in more detail how a hash function 
is chosen and how different strategies for collision resolution work.

