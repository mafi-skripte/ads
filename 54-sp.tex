%!tex root = ./skript.tex

\chapter{Shortest Paths--Definitions and Unweighted Case}

We will now look at the problem of finding a \emph{shortest path}
in a graph $G$. 

For starters, we consider the  following scenario:
let $G$ be an \emph{unweighted} 
and \emph{directed}, and let $s, t \in V$ be two 
nodes in $V$. 
We call $s$ the \emph{source} and
$t$ the \emph{target}.
The task is to find a \emph{path} in $G$ from $s$ to $t$ that 
has the minimum \emph{length}. 

A path $\pi$ from $s$ to $t$ is a 
sequence $\pi: s = v_0, v_1, \dots, v_k =  t$ of nodes that (i) starts
with $v_1 = s$; (ii) ends with $v_k = t$; and  (iii) has the property that for each pair
$v_{i}, v_{i + 1}$ of consecutive nodes in $\pi$, there is the directed edge $(v_i, v_{i + 1})$
from $v_i$ to $v_{i + 1}$ in $E$.\footnote{Note that in our definition,
a vertex may appear multiple times along the path. In different texts on graph theory, this is
handled differently. Since our focus here is on \emph{shortest} paths, this choice does not matter
much. In  a shortest path, every vertex appears at most once: 
if a vertex is repeated, we can omit the part between the two repetitions
and we obtain a shorter path. However, this is no longer true when have graphs with negative
edge weights. We will discuss this more in the next chapter.}
The \emph{length} of $\pi$, denoted by $| \pi|$, is the 
number of edges of $\pi$ (i.e., $|\pi| = k$). The \emph{distance}
between $s$ and $t$ in $G$, denoted by $d_G(s, t)$, is the minimum length 
of any path from $s$ to $t$.

\textbf{TODO}: Example

\textbf{Remarks}:
\begin{itemize}
	\item We can also consider shortest paths in \emph{undirected} graphs. The 
		directed case is more general, because we can turn every undirected graph
		into a directed graph by replacing each undirected edge with two opposing
		directed edges.
	\item  There is also a weighted version of the shortest path problem. We will look
		at it in the following chapters.
	\item This problem is also called the \emph{single-pair-shortest-path} problem (SPSP),
		because we are looking for a shortest path between to given vertices $s$ and $t$.
		A related problem is the \emph{single-source-shortest-paths} problem (SSSP), which
		we will describe next.
\end{itemize}

\paragraph{Single-source-shortest-path problem.}
In the single-source-shortest-paths problem (SSSP), we are given
an \emph{unweighted},
\emph{directed} graph $G$ and  a \emph{source} vertex $s  \in V$.
The task is to find, for every $v \in V$, a shortest path from
$s$ to $v$.

The SSSP is more general than the SPSP: if the know a shortest path
from $s$ to every other vertex of $G$, then we also know a shortest
path from $s$ to a desired target vertex $t$. Conversely, even if we are
only interested in only a single shortest path, it may be inevitable
to solve a version of the SSSP. The reason for this is toe following
important property of shortest paths, the \emph{subpath-optimality-property}:
subpaths of shortest paths are also shortest paths.
More formally:

\textbf{Observation}: Let $G = (V, E)$ be a graph, and let 
$s = v_0, v_1, \dots, v_k = t$ be a shortest path from
$s$ to $t$. Then, for every $j = 0, \dots, k$, we have
that $s = v_0, v_1, \dots, v_j$ is a shortest path from
$s$ to $v_j$.

The reason for this observation is simple: if there were a $j \in \{0, \dots, k\}$
and a path $\pi'$ from $s$ to $v_j$ that is shorter than the path $v_0, \dots, v_j$,
then we would have a shorter path from $s$ to $t$ by taking the path $\pi'$ followed
by the suffix $v_j, \dots, v_k$. This is a contradiction to the fact that 
$v_0, \dots, v_k$ is a shortest path from $s$ to $t$. 

The subpath-optimality-property implies that to find a shortest path from $s$ to $t$, we
also need to find all shortest paths for the intermediate vertices. Thus, in the following,
we will focus on solving the SSSP-problem in an unweighted graph.

From the discussion in the previous chapter, it is plausible that BFS is well suited
to solve the SSSP-problem: we start at the source $s$, then we visit all the vertices
that can be reached from $s$ with a single edge, then we visit all vertices that
can be reached from $s$ with two exactly two edges, etc. Thus, BFS already encounters
all the vertices in the correct order. All that we still need to do is to keep track 
of the distances to $s$ and of the shortest paths to $s$ throughout the algorithm.

Before we can do this, there is one final question: when solving the SSSP-problem, how
do we keep track of all the different shortest paths? Of course, we could store, individually
for each vertex $v \in V$, an explicit representation of a shortest path from $s$ to $v$.
However, this is not very efficient: in a graph with $n$ vertices, we may need $\Omega(n^2)$ 
space to represent all the shortest paths explicitly.

To solve this problem, we use the subpath-optimality property to
represent all the shortest paths from $s$ \emph{implicitly}: suppose that $\pi$ is
a shortest path from $s$ to $v$, for a vertex $v \in V$, and suppose that we know that
$w$ is the last vertex on $\pi$ before $v$. Then, we know that the prefix of $\pi$ from
$s$ to $w$ is also a shortest path. Thus, we can obtain a shortest path from $s$ to $v$
by constructing a shortest path from $s$ to $v$ and by adding the edge $(v, w)$.
In other words, we need to know only that $w$ is the \emph{predecessor} of $v$
on a shortest path from $s$ to $v$. The, we can proceed recursively.

After this discussion, it is straightforward to extend BFS in order to solve
the SSSP-problem from $s$. With each vertex $v \in V$, we store two additional
properties: the \emph{distance} $v.\texttt{d}$ from $s$ to $v$, and the \emph{predecessor}
$v.\texttt{pred}$ of $v$. The distance and the predecessor are set as soon as a vertex
is encountered for the first time by the BFS.
\begin{verbatim}
Q <- new Queue
for v in vertices() do
  v.found <- false
  // NEW: Initially, the distances
  // from s are infinite and the predecessor 
  // does not exist (we have not yet found a
  // path from s to v)
  v.d <- INFTY
  v.pred <- NULL
// we have seen s, and the distance from s to 
// itself is 0
s.found <- true
s.d <- 0
Q.enqueue(s)
while not Q.isEmpty() do
  v <- Q.dequeue()
  for w in v.neighbors() do
    if not w.found then
      w.found <- true
      // NEW: Update the distance
      // and the predecessor
      w.d <- v.d + 1
      w.pred <- v
      Q.enqueue(w)
\end{verbatim}

\textbf{TODO:} Add example
\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/23-01_SSSP}
	\end{center}
	\caption{SSSP example.}
\end{figure}



As before, the running time of the modified version of BFS is $O(|V| + |E|)$, if the graph
is given as an adjacency list representation. 

One can show that BFS solves the SSSP-problem in unweighted graphs: when the BFS-algorithm
terminates, for every $v \in V$, we have that (i) $v.\texttt{d} = d_G(s, v)$ (the distances
have been computed correctly); and (ii) $v.\texttt{pred}$ is the predecessor of $v$ on a shortest
path from $s$ to $v$ (if $v$ is reachable from $s$). If we draw all the $v.\texttt{pred}$-pointers
in $G$, we obtain a rooted tree with root $s$ that encodes all the shortest paths in $G$. This
tree is called a \emph{shortest path tree} for $s$ in $G$.

We will not do the
correctness proof here. Instead, in the next chapter, we look at at Dijkstra's algorithm, a generalization
of BFS for graphs with nonnegative edge weights. We will prove that Dijkstra's algorithm is
correct. This also implies the correctness of BFS.


