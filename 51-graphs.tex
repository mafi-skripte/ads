%!tex root = ./skript.tex

\chapter{Graphs}


In the final section of this class, we will look at algorithms
for \emph{graphs}. A graph is a mathematical structure that
formalizes \emph{relationships} between objects.
Formally, a graph consists of \emph{nodes} and \emph{edges}.
Nodes are arbitrary objects that represent entities of interest.
Edges represent relationships between nodes and show that two
nodes are connected in some way.
Small graphs can be visualized by a diagram: the nodes are typically
represented by small disks, sometimes annotated with additional information
such as the name or content of the node. The edges are represented by line
segments or curves that connect two nodes.

\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/20-01_graph_example}
		\caption{An example graph.}
	\end{center}
\end{figure}

Graphs are very general mathematical objects. They are used in many places
in computer science and mathematics to model a wide variety of real world scenarios.
Here are a few examples:

\begin{itemize}
	\item \textbf{Road networks}. Graphs can be used to model all kinds of road networks.
		For example, a simple way to represent the road network of
		Berlin would be as follows: the nodes are all intersections between
		roads in Berlin, and the edges are the direct connections between
		the intersections.

		One can imagine a wide variety of information that could be associated
		with the nodes and the edges: for example, for each intersection, we
		could store the name, the geographic coordinates, the elevation, etc.
		For each edge we could store the distance, the inclination, whether it is
		a one-way street, whether the road has special regulations for bikes,
		children, noise-protection, etc.
        \item \textbf{Public transportation networks}: Graphs can also model 
		public transportation networks. For example, the nodes could
		be all subway stops in Berlin, and the edges could be direct
		subway connections between the stops. 

		Again, a lot of information could be stored with the individual
		nodes and edges, e.g., the name of the station, the name of the 
		subway lines that service a connection, the travel time between
		to stops, etc.
	\item \textbf{Computer networks}: The nodes in a computer network
		could be individual computers or servers, the edges could 
		represent direct connections between servers, e.g., through
		physical communication cables.
        \item \textbf{Web graph}: In the web graph, the nodes represent individual
		websites. The edges represent links between websites. Note that
		in the web graph, edges are \emph{directed}: we can follow a link,
		but when we are on a website, we cannot see which other sites link to
		this website (unless, we use additional tools that provide this
		information).\footnote{Interestingly, \emph{Project Xanadu}, an early attempt to
		develop a networked hypertext model that predates the world-wide-web by 30 years,
		envisioned \emph{bidirectional} links between individual pages. To date, Project
		Xanadu is not finished.}
	\item \textbf{The bridges of Königsberg (Kaliningrad)}:
		The foundational problem of graph theory is the following: 
		the city of Königsberg is traversed by the river Pregel. Within the
		city, there are two islands and seven bridges
		that connect the islands to each other and to the sides of the river.
		The question is to determine whether there is a way to walk through Königsberg
		so that each of the seven bridges is crossed exactly once. In 1736, Leonhard
		Euler showed that this is not possible. For this, he developed the formalism
		that now forms the basis of graph theory: The nodes correspond to the 
		two sides of the river Pregel and the two islands, the edges represent
		the bridges.

		Note that in this problem, there are nodes that are connected by more than
		one edge (since there may be two bridges between an island and a river side).
		Note also that the bridges in today's Kaliningrad are different than in the 18th
		century. Today, it is actually possible to walk through the city so that
		every bridge is crossed exactly once. However, this walk will need to have different
		start and end points.
	\item \textbf{Bank transactions}:
		Banks use graphs to analyze transactions between their customers.
		The nodes are bank accounts, and the edges represent transactions between
		the accounts.

	\item \textbf{Social graph}:
		Graphs can also represent relationships between humans. A typical social
		graph is as follows: the nodes represent individual people, and edges correspond
		to a relationship between the people, e.g., that person $A$ \emph{knows} person
		$B$.
	\item \textbf{Protein interaction graphs}:
          In bioinformatics, we often encounter graphs that reflect some biological
	  reality. For example in \emph{protein interaction graphs}, the nodes are certain
	  proteins that occur in the human body, and there is an edge between two proteins
	  if an only if the two proteins typically engage in a chemical reaction. 
	\item \textbf{Linked data structures}:
		Throughout this class, we have already encountered many examples 
		of graphs: linked lists, binary trees, and tries are all special types
		of graphs with additional structure (e.g., directed edges, certain additional
		information in the vertices and in the edges, a designated root node).
\end{itemize}

\paragraph{Formal definitions, notation, and terminology.}
%TODO: Cross-reference definitions with Diskrete Strukturen
Formally, a graph $G$ has two components: a finite, nonempty set $V$
of \emph{vertices}, and a set $E$ of \emph{edges}. The set $E$ consists of two-element
subsets of distinct vertices, i.e., 
\[
	E \subseteq \{ \{v, w\} \mid v, w \in V, v \neq w\}.
\]
The vertices are sometimes also called \emph{nodes}. The edges are sometimes
also called \emph{arcs}. We write $G = (V, E)$ to indicate that the graph $G$ consists
of vertex set $V$ and edge set $E$.

We say that two vertices $v, w \in V$ are \emph{adjacent} if and only if 
there is an edge between $v$ and $w$. For an edge $e = \{v, w\} \in E$, we call
$v$ and $w$ the \emph{endpoints} of $e$, and we say that $e$ and
the endpoints $u$, $v$ are \emph{incident} to each other.

Given a node $v \in V$, the \emph{neighborhood} $\Gamma(v)$ of $v$ consists of
all nodes that are adjacent to $v$:
\[
	\Gamma(v) = \{ w \mid \{v, w\} \in E\}.
\]
The \emph{degree} of $v$, denoted by $\deg(v)$, is the number of neighbors of $v$:
\[
	\deg(v) = |\Gamma(v)|.
\]
The well-known hand-shake lemma states that the total degree in a graph is twice
its number of edges:
\[
	\sum_{v \in V} \deg(v) = 2|E|.
\]
\begin{proof}
For a vertex $v \in V$ and an edge $e \in E$, let $I_{ve} = 1$, if
$v$ is an endpoint of $e$, and $I_{ve} = 0$, otherwise.
Then, we have
\begin{align*}
	\sum_{v \in V} \deg(v)  
	&= \sum_{v \in V} \sum_{e \in E} I_{ve}\\
	&= \sum_{e \in E} \sum_{v \in V} I_{ve}\\
	&= \sum_{e \in E} 2\\
	&= 2|E|.
\end{align*}
\end{proof}

We saw that there are many objects that can be modeled with graphs. Thus, it will
come as no surprise that there are many variants of the definitions that adapt the
notion to different scenarios.
The graphs that we have defined so far are also called \emph{simple}, \emph{undirected},
\emph{unweighted} graphs.
Common variations of the definition (some of which we will use later) include:
\begin{itemize}
	\item \textbf{Directed graphs.} In a \emph{directed} graph (as opposed to
		an \emph{undirected} graph), the edges have an orientation, 
		going \emph{from} one endpoint \emph{to} the other. 

		Formally,
		the edge set of a directed graph $E$ is a subset of the Cartesian product
		$E \times E$, so the edges are ordered pairs of vertices.
		An edge $e = (v, w)$ is \emph{directed} from $v$ to $w$. This is visualized
		as an arrow from $v$ to $w$. We call $v$ 
		the \emph{tail} of $e$ and $w$ the \emph{head} of $e$ (and, collectively, we still
		call $v$ and $w$ the \emph{endpoints} of $e$).

		Given a vertex $v \in V$, the \emph{outgoing neighbors} $\Gamma^+(v)$ of $v$ 
		are the heads
		of the edges that have $v$ as a tail:
		\[
                   \Gamma^+(v) = \{w \mid (v, w) \in E\}.
		\]
		The \emph{outdegree} of $v$, denoted by $\deg^+(v)$, is the number of outgoing neighbors
		of $v$:
                \[
			 \deg^+(v) = |\Gamma^+(v)|.
		 \]
		 Similarly, the \emph{incoming neighbors} $\Gamma^-(v)$ of $v$ are 
		 the tails of edges  that have $v$ as a head:
		\[
                   \Gamma^-(v) = \{w \mid (w, v) \in E\}.
		\]
		The \emph{indegree} of $v$, denoted by $\deg^-(v)$, is the number of incoming neighbors
		of $v$:
                \[
			 \deg^-(v) = |\Gamma^-(v)|.
		 \]
		 Since every edge has exactly one head and one tail, we have
		 \[
			 \sum_{v \in V} \deg^-(v) = \sum_{v \in V} \deg^+ (v) = |E|.
		 \]
	 \item \textbf{Multigraphs.} In a \emph{multigraph} (as opposed to a \emph{simple} graph),
		 there can be multiple edges between two vertices. Furthermore, in a mutigraph
		 there can also be \emph{loops}, i.e., edges that connect a vertex to itself.

		 Formally, the edge set $E$ of a multigraph is a \emph{multiset} of subsets
		 of vertices, such that every in $E§$ has \emph{at most} two elements.
		 In a multset, the same element can occur multiple times. A set in $E$ that
		 contains only a single vertex $v$ represents a loop that connects $v$ to itself.
	 \item \textbf{Weighted graphs.} In a \emph{weighted} graph (as opposed to an
		 \emph{unweighted} graph), we have additional information
		 associated with the parts of the graph. 

		 There are several variants, but in this class, we will focus on \emph{edge-weighted}
		 graphs where each edge has an associated real number (e.g., travel time,
		 distance, inclination, etc.). This is formalized by a \emph{weight function}
		 $w: E \rightarrow \mathbb{R}$, where $w(e)$ is called the \emph{weight}
		 of edge $e$.\footnote{Depending on the context, the value $w(e)$ may 
		 also be called differently, e.g., the \emph{length} of edge $e$ or the
		 \emph{cost} of edge $e$).}
\end{itemize}



\paragraph{Algorithmic problems on graphs.}
Graphs are a versatile modeling tool in Computer Science, Mathematics, and beyond.
As such, there are many interesting algorithmic problems that arise in the study
of graphs. Here are a few examples, some of which we will consider more closely in the
following chapters.
\begin{itemize}
	\item \textbf{Connectivity.} Let $G = (V, E)$ be a graph, and let $v, w \in V$ be
		two vertices. The general connectivity problem asks whether
		$v$ and $w$ are \emph{connected} in $G$, i.e., whether there is a
		\emph{path} in $G$ that goes between $v$ and $w$. A path between
		$v$ and $w$ is a sequence of vertices that starts with $v$, ends with $w$,
		and that has the property that two consecutive vertices are adjacent in $G$.

		There is also a directed version of the problem. Here, $G$ is a directed
		graph, and the question is whether there is a \emph{directed} path
		from $v$ to $w$. A directed path from $v$ to $w$ is a sequence of vertices 
		that starts at $v$, ends at $w$, and that has the property that for any
		two consecutive vertices $x$, $y$ on the path, there is an edge from
		$x$ to $y$ in $G$.
	\item \textbf{Unweighted shortest path.} 
		Let $G = (V, E)$ be a graph, and let $s, t \in V$ be two nodes
		in $G$. In the \emph{shortest path} problem, we would like to find
		a path from $s$ to $t$ that has the minimum number of edges among
		all such paths. 

		As with connectivity, this problem can be asked in both undirected and
		directed graphs.
	\item \textbf{Weighted shortest path.} 
		Let $G = (V, E)$, $w: E \rightarrow \mathbb{R}$  be a weighted  graph, and let 
		$s, t \in V$ two nodes in $G$.  Let $\pi$ be a path from $s$ to $t$.
		The \emph{total weight} of $\pi$ is the sum of the weights of the edges in
		$\pi$. In the \emph{weighted shortest path} problem, we would like to find
		a path from $s$ to $t$ that minimizes the total weight. As usual, there is
		both an undirected and a directed version.
	\item \textbf{Minimum cut.}  Let $G = (V, E)$ be a simple undirected graph.
		Let $s, t \in V$ be two nodes in $G$. The task is to find a minimum set
		$F \subseteq E$ of edges such that by deleting $F$ from $G$, we can
		disconnect $s$ from $t$ in $G$.
	\item \textbf{Minimum spanning tree.}
		Let $G = (V, E)$, $w: E \rightarrow \mathbb{R}$ be a weighted, undirected, connected
		graph. The task is to find set $F \subset E$ such that $(V, F)$ is a tree and
		such that the sum of the edge weights in $F$ is minimum.
\end{itemize}
In the following chapters, we will look at some of these problems in more detail. First,
however, we need to talk about how graphs are represented in a computer.
