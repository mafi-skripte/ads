%!tex root = ./skript.tex

\chapter{Cuckoo Hashing}

In \emph{cuckoo hashing}, we try to combine the advantages
of chaining and of linear probing. Like in linear probing,
all
the entries are stored directly in the hash table $T$. There is no secondary
data structure. Like in chaining, the position
of the entries is determined by the hash function. It cannot happen
that an entry ends up arbitrarily far from its assigned position.
The main idea for making this possible is to use \emph{two} hash functions
$h_1$ and $h_2$ that assign possible positions to the keys.
That is, an entry with key $k$ can be stored \emph{either} at
position $h_1(k)$ \emph{or} at position $h_2(k)$ (and nowhere else).

The option of having two possible positions for each key adds 
flexibility to the structure and allows for very efficient lookups
and deletions. The main challenge is to implement insertions. Here, 
the approach is as follows: when inserting a new entry $(k, v)$,
we first consider the position $h_1(k)$ that is assigned to $k$
by the first hash function. If $T[h_1(k)]$ is free, we simply write 
the entry and finish. Otherwise, $T[h_1(k)]$ is already occupied
by an entry $(k', v')$. Now, we take inspiration from the cuckoo bird:
we remove the entry $(k',v')$ from $T[h_1(k)]$ to make space for our
own entry $(k, v)$, which is now stored in this position.  
Unlike the cuckoo, however, we cannot just discard the entry $(k', v')$,
but we need to insert it at another position in $T$. For this,
we compute $h_1(k')$, and if $h_1(k') \neq h_1(k)$, we relocate 
$(k', v')$ to $h_1(k')$. Otherwise, we relocate $(k', v')$ to
$h_2(k')$. If this other position is empty, we are done. If it is again
occupied, we need to relocate the entry at this other position to a new position.
This continues until we either find an empty position or until we can be certain
that our relocation scheme cannot be successful. In the latter case, the complete
structure is rebuilt, using new hash functions $h_1$ and $h_2$.

We now look at the pseudocode for the operations.

\paragraph{Lookup.} As mentioned above, a lookup for a key
$k$ is easy: we only need to consider the positions $h_1(k)$ and
$h_2(k)$ to see if the key is present. If so, we return the value,
if not, we report that $k$ is not present.
\begin{verbatim}
get(k):
  if (T[h1(k)].k == k)
    return T[h1(k)].v
  if (T[h2(k)].k == k)
    return T[h2(k)].v
  throw NoSuchElementException
\end{verbatim}

\paragraph{Deletion.} Deletion works in the same way as a lookup.
We check the positions $h_1(k)$ and $h_2(k)$. If key is present,
we delete the entry by overwriting it with $\perp$. If not,
we report that the key has not been found.
\begin{verbatim}
remove(k):
  if (T[h1(k)].k == k)
    T[h1(k)] <- NULL
    return
  if (T[h2(k)].k == k)
    T[h2(k)] <- NULL
    return
  throw NoSuchElementException
\end{verbatim}


\paragraph{Put.}
In a put, we first check if the key $k$ is present in the table.
If so, we simply update the value. If not, we start the insertion process.
The insertion process is implemented in a \texttt{for}-loop that makes
$N$ attempts of inserting the current entry. In each iteration of the loop,
we have a current insertion position \texttt{pos} and a current entry $(k, v)$.
We check if the position \texttt{pos} in $T$ is empty. If so, we simply put
$(k, v)$ into $T[\texttt{pos}]$ and are done. If not, we exchange our current
entry $(k, v)$ with the entry $(k', v')$ that is already present in $T[\texttt{pos]}]$.
We set \texttt{pos} the second possible position for $(k', v')$, and we proceed to the
next iteration of the loop.

Note that it may happen for a key $k$ that $h_1(k) = h_2(k)$, i.e., that the two possible 
positions coincide. This is not necessarily a problem, the insertion algorithm can still succeed.
Note also that we make $N$ attempts for the insertion. This is just for simplicity,
it may well happen that the insertion algorithm is caught in a look before that, but
it would be more complicated to check for this.

If the insertion does not succeed, we 
reconstruct the whole table with new hash functions $h_1$ and $h_2$. This  means that
if we want to use cuckoo hashing, we need to use a compression function that is chosen
randomly from a larger set (as, e.g., in universal hashing or in tabulation hashing),
so that this step can be carried out.
\begin{verbatim}
put(k, v):
  // if k is present, we simply update the value
  if (T[h1(k)].k == k)
    T[h1(k)].v <- v
    return
  if (T[h2(k)].k == k)
    T[h2(k)].v <- v 
    return
  // if not, we must insert (k, v)
  // if the table is already full, we cannot insert $k$
  if (|S| == N)
    throw TableFullException
  // we try to insert the entry at the first position
  pos <- h1(k)
  for i := 1 to N do
    // if the position is empty, we insert the entry
    // and are done
    if (T[pos] == NULL)
      T[pos] <- (k,v)
      return
    // otherwise, we exchange the current entry and
    // determine the second possible position
    (k,v) <-> T[pos]
    if (pos == h1(k))
      pos <- h2(k)
    else
      pos <- h1(k)
  // if the insertion does not succeed, we need to rebuild the table
  Choose new hash functions and rebuild the table
  put(k,v)
\end{verbatim}

\paragraph{Analysis.}
A major advantage of cuckoo hashing is that lookups (and deletions) take
$O(1)$ time \emph{in the worst case}. Thus, in a scenario where insertions
are rather infrequent, but lookups occur very often, cuckoo hashing can be superior
to chaining or linear probing.\footnote{Such a situation occurs, e.g., in network routers
where the next hop for a given address needs to be determined very quickly, while
changes in the network topology are relatively rare.}

One can show that if the load factor is small enough (e.g., $1/2$), and if
the hash functions behave randomly, then the expected
time for an insertion operation is $O(1)$, so that in theory, cuckoo hashing 
performs as well as chaining and linear probing. This analysis is beyond the scope
of these lecture notes and is deferred to later classes.

There are several variants of cuckoo hashing, e.g., we could use more than two 
hash functions, or we could introduce a \emph{stash}, by storing more than one
entry in a position of the hash table.

Cuckoo is a relatively new technique for collision resolution. After that, there
have been further variants and improvements, e.g., Robin Hood hashing or hopscotch
hashing. We will not discuss these here, but the interested reader is invited to
investigate them further.

This chapter concludes our discussion of collision resolution strategies.
In the next chapter, we will talk a bit more about further uses of hash functions
beyond the implementation of hash tables.
