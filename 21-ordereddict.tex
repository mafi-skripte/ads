%!tex root = ./skript.tex

\chapter{Ordered Dictionaries}

We now describe the ADT \emph{ordered dictionary}, also called 
\emph{ordered map}. The goal of an ordered dictionary is to maintain
a set of \emph{entries}. An entry consists of two parts, a \emph{key}
and a \emph{value}. For each key, there is at most one entry, and there is
an order among the keys. We would like to be able to create new entries for
new keys, update values for existing keys, lookup the value for a given key,
delete existing entries, and to determine the predecessor and the successor
entry for a given key.

More precisely, in an ordered dictionary, we have two underlying sets,
the \emph{key set} $K$ and the \emph{value set} $V$. The key set is \emph{ordered},
that is, for any two distinct keys, we can determine which one is smaller (and the
results of these comparisons are consistent).
An \emph{entry} is an ordered pair $(k, v)$ from the Cartesian product $K \times V$, 
and our goal is to maintain a dynamic set $S \subseteq K \times V$ such that 
each key appears at most once in an entry of $S$.

In an ordered dictionary, we would like to support the following operations:

\begin{itemize}
  \item \texttt{put}$(k, v)$, where $k \in K$ is a key and $y \in V$ is a value.
	  This operation has no precondition and does not have a return value.
	  The effect is as follows: if $S$ does not contain an entry with key $k$,
	  we create add to $S$ the new entry $(k, v)$; if $S$ already has an entry
  	 with key $k$, then this entry is deleted and replaced by the entry $(k, v)$.
		Mathematically, the desired effect of \texttt{put} can be described as
		$S_\text{new} = (S_\text{old} \setminus (\{ k \} \times V)) \cup (k, v)$.
  \item \texttt{get}$(k)$, where $k \in K$ is a key. The precondition is that
	  $S$ contains an entry with key $k$ (otherwise, \texttt{get}) will raise an
  	  exception). There is no effect. The return value is the value $v$ of the
	  (unique) entry $(k, v) \in S$ that has key $k$.
  \item \texttt{remove}$(k)$, where $k \in K$ is a key.  
	  There is no precondition and no return value. The effect is as
  	  follows: if $S$ contains an entry 
	  with key $k$, then this entry is removed in $S$. Otherwise, $S$ remains
	  unchanged.
        Mathematically, the desired effect of \texttt{remove} can be described as
		$S_\text{new} = S_\text{old} \setminus (\{ k \} \times V)$.
  \item \texttt{isEmpty}:,
	  There is no precondition and no effect.
		The operation returns \texttt{true}, if $S = \emptyset$, and
		\texttt{false}, if $S \neq \emptyset$.
  \item \texttt{size}.
	  There is no precondition and no effect.
   	The operation returns $|S|$, the number of entries in $S$.
  \item \texttt{min}.
	  The precondition is that $S$ is not empty, $S \neq \emptyset$.
	  There is no effect.
   	The operation returns the smallest key that appears in $S$, 
		$k_\text{min} = \min \{ k \mid (k, v) \in S \}$.
  \item \texttt{max}.
	  The precondition is that $S$ is not empty, $S \neq \emptyset$.
	  There is no effect.
   	The operation returns the largest key that appears in $S$, 
		$k_\text{max} = \max \{ k \mid (k, v) \in S \}$.
	\item \texttt{pred}$(k)$, where $k \in K$ is a key.
	  The precondition is that $k$ is larger than the smallest key in $S$,
   	  $k >  \min \{ k \mid (k, v) \in S \}$.
	  There is no effect.
		The operation returns the \emph{predecessor} of $k$ in $S$,
		that is, the largest key that appears in $S$ and that is smaller than $k$. 
		Formally, the return value is
		$k_\text{pred} = \max \{ k' \mid (k', v) \in S, k' < k \}$.
	\item \texttt{succ}$(k)$, where $k \in K$ is a key.
	  The precondition is that $k$ is smaller than the largest key in $S$,
   	  $k <  \max \{ k \mid (k, v) \in S \}$.
	  There is no effect.
		The operation returns the \emph{successor} of $k$ in $S$,
		that is, the smallest key that appears in $S$ and that is larger than $k$. 
		Formally, the return value is
		$k_\text{succ} = \min \{ k' \mid (k', v) \in S, k' > k \}$.
\end{itemize}
In the following chapters, we will see several ways of implementing the ADT ordered dictionary.
