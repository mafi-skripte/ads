%!tex root = ./skript.tex

\chapter{Dictionaries for Strings -- Tries}

In the previous sections, we saw many different ways to implement the
abstract data types dictionary and ordered dictionary. Now, we will
look at the problem of implementing the ADT ordered dictionary for
the case that the key set $K = \Sigma^*$, for a fixed
alphabet $\Sigma$.

\textbf{TODO:} Say something about lexicographic order.

Of course, we could just use any of the existing implementations
that we saw in the previous chapters, e.g., AVL-trees, skiplists,
$(a, b)$-trees, etc. These implementations work for general ordered
key sets, so they can be used also for strings.

However, there are several reasons why one might want to construct
special data structures for the case $K = \Sigma^*$. First, strings
can become  very long. Thus, it is not a realistic assumption that
a comparison between two strings takes constant time, and we would
like to have a dictionary structure that takes this special structure
into account (and that is faster if the strings become very long). Second,
it may be possible to obtain a simpler solution for this special case. Since
the case of string keys is quite common, it makes sense to have special
structures that are easier to understand and implement. Third, once we have
a special dictionary structure for strings, one may extend it for more operations
and applications. Since this case occurs often in application (particularly in
computational biology), this can be very useful.

\paragraph{Tries.} \emph{Tries} are a simple way to implement an ordered dictionary for
strings.\footnote{The name \emph{trie} derives from the word re\emph{trie}val, but it
is pronounced like ``try'' and not like ``tree''. Tries are sometimes also called
\emph{radix trees} or \emph{digital search trees}.}

Let $\Sigma$ be an alphabet. A trie for $\Sigma$ is a rooted multi-way tree $T$. 
Each inner node
in $T$ has at least $1$ child and at most $|\Sigma|$ children, were the number
of children can differ from node to node. For each inner node $v$, each edge that
connects $v$ to a child is labeled with a symbol from $\Sigma$, so that each
symbol from $\Sigma$ occurs at most once among the edges for $v$. For each key $s$ that is
stored in $T$, there is exactly one leaf $w$ in $T$ that represents $s$. We can obtain
$s$ by concatenating the labels along the path from the root of $T$ to $w$. The value
for the key $s$ is stored in $w$.

\textbf{TODO}: Example

The structure as described has an obvious problem:  suppose we have two keys $s_1$ and $s_2$
such that $s_1$ is a \emph{prefix} of $s_2$, i.e., that $s_1$ coincides with a substring
at the beginning of $s_2$. For example, this is the case for  
$s_1 = \text{HAND}$ and $s_2 = \text{HANDSCHUH}$. Now, if we try to store
both $s_1$ and $s_2$ in a trie $T$, we encounter a problem, because $T$ is supposed to contain
a leaf $w$ for $s_1$, but then, the leaf $w$ would also lie on the path to the leaf for
$s_2$, thus we expect that $w$ is both a leaf and an inner node, which is impossible.

There are at least two ways to solve this problem. First, we could simply abandon the
requirement that the keys correspond to the leaves in the trie. Instead, we introduce
an additional bit in each node that determines whether the node is the endpoint of
a string or not. Then, the values can be stored both in the leaves and in the inner nodes.
This is a feasible solution, but it creates special cases and complicates the algorithms.
Thus, we prefer to use a second solution.

The second solution is to introduce a special symbol (usually denoted by \$) that does
not appear in the original alphabet $\Sigma$. Whenever we insert a key $s$ into the trie,
we add the symbol \$ to the end of $s$. This ensure that now key can be a prefix of
another key (because if $s_1\$$ were a prefix of $s_2\$$, it would be the case
that $s_2$ contains the symbol \$ in the middle, which is impossible, since $\$  \not \in \Sigma$).
The advantage of this solution is that it keeps the structure and the algorithms simple. 
It is another example of the \emph{sentinel technique} that we have already encountered
in \emph{Konzepte der Programmierung} when we talked about different implementations of
linked lists.\footnote{The symbol \$ is used in several places in computer science
to mark the end of a string. Historically, this comes from the old PDP-minicomputers that used a very
restricted character set, so that \$ was a very natural choice for this purpose.}

\textbf{TODO}: Add an example of a trie with \$.

\paragraph{Implementation issues.} There are different ways to implement a trie.
In particular, we need to have a way to represent the children of a node. If $\Sigma$
is small, it is feasible to store a fixed array that has a position for every symbol in
$\Sigma$ and that stores a reference to the child node for that symbol, if it exists, and
$\perp$, otherwise. If $\Sigma$ is large, this solution becomes infeasible, and we may need
to store a small dictionary that stores entries of the form $(\sigma, v)$, where
$\sigma$ is a symbol for which a child exists, and $v$ is a reference to the child node for $\sigma$.
When analyzing our algorithms, we assume that in each node, we spend $|\Sigma|$ steps
to locate a given child node. Using a better dictionary structure, this running 
time can be be improved.

\paragraph{Operations.} Tries support all the operations of the ADT ordered dictionary. The
details are left as an exercise.

\paragraph{Analysis.}
Suppose we have a trie $T$ that stores a set $S$ of entries.
Then, the total space for  $T$ is at most $O(\sum_{(s, v) \in S} |s|)$, the total
length of the keys that appear in $S$. The space can be better if the keys in $S$
share many prefixes.

The operations \texttt{put}$(s, v)$, \texttt{get}$(s)$, and
\texttt{remove}$(s)$ can be implemented in time $O(|s| \cdot |\Sigma|)$. The operations
\texttt{pred}$(s)$ and \texttt{succ}$(s)$ can be implemented in time
$O((|s| + |s'|) \cdot |\Sigma|)$, where $s'$ is the predecessor or successor of
$s$, depending on whether we consider \texttt{pred} or \texttt{succ}.

\paragraph{Compressed tries.}
One possible issue with tries is that they create a node for every symbol in an
input key, even if there are no branches in the tree. Thus, when resolving a key, 
may need to follow a long chain of node objects, even though there is no branch 
that needs to be resolved. This may cause unnecessary overhead in practice, because
following an object reference can be slow in modern computer systems (compared to, say,
scanning an array).

\emph{Compressed tries} are a way to address this issue. In a compressed trie, we
contract every long path that consists of nodes that have only one child each into
a single edge. This edge is labelled with the whole substring that corresponds to the
path. Nodes are only introduced when a branch happens. As before, we introduce a special
symbol $\$ \not\in \Sigma$ to mark the end of the strings in the compressed trie, to
make sure that every key corresponds to a leaf.

The operations are implemented as before, but now we may need to split an edge after
an insertion, if a new branch becomes necessary, or to merge an edge after a deletion,
if a branch disappears. Since we still need to store all the keys, the space requirement
for a compressed trie  that stores a set $S$ of entries is still $O(\sum_{(s, v) \in S} |s|)$,
but now the tree has only $O(|S|)$ nodes, which is potentially more efficient in practice.

To distinguish them from compressed tries, we also refer to the usual tries from before
as \emph{uncompressed} tries. An alternative name for compressed tries is PATRICIA tries,
where PATRICIA is an acronym for Practical Algorithm To Retrieve Information Coded In Alphanumeric.

\Knut{In the bioinformatics lectures you will learn about string matching for multiple 
strings. One algorithm, the Aho-Corasick Algoriothms uses a trie for searching 
a string simultaneously for many patterns.}

\paragraph{Suffix trees.} Let $s \in \Sigma^*$ be a long string over some alphabet
$\Sigma$. A \emph{suffix tree} for $s$ is a compressed trie that stores all the
suffixes (i.e., all end strings) of the string $s\$$. 

\textbf{TODO}: Example

Suffix trees are a very useful data structure that allow us to solve many tasks
that concern the string $s$ efficiently. For example, suppose that $s$ is a very
long string, and that we would like to carry out many \emph{string searches} on $s$.
That is, we would like to be able to perform the following query: given a string $t \in \Sigma^*$,
does $t$ occur in $s$ as a substring?\footnote{We will talk more about string searching 
in Chapter~\ref{ch:ssearch}.} If a suffix tree $T$ for $s$ is available, 
this is easy: the crucial 
observation is that $t$ is contained in
$s$ if any only if $s$ has a suffix that begins with $t$. Thus, all we need to
do is search for $t$ in $T$. If this search ends in an inner node (or a leaf),
then $t$ appears in $s$. Even more, the leaves under the node where our search
ends give us the locations of \emph{all} occurrences of $t$ in $s$. This is only one
example, there are many other applications of suffix trees that will be treated
in later classes.

Since the suffix tree $T$ is a compressed trie and since $s\$$ as $|s| + 1$ suffixes,
the number of nodes in $T$ is $O(|s|)$. However, if we store the edge labels explicitly,
the space requirement for $T$ can become $\Omega(|s|^2)$, which is too much if $s$ is long.
To fix this, we observe that all edge labels in $T$ are substrings of $T$. Thus, instead
of storing the edge labels explicitly, it suffices to store the start and the end position
of the corresponding substring in $s$. This reduces the space requirement for the suffix
tree to $O(|s|)$, which is asymptotically the same as storing $s$ itself.

Given $s$, the naive construction algorithm that successively inserts 
all suffixes of $s$ into a compressed trie may take time $\Omega(|s|^2)$. 
However, there are much better ways to construct a suffix tree for $s$.
In fact, it is possible to construct the tree in time $O(|s|)$, which is
as good as we can hope for. This method will be discussed in a later class.

\Knut{Note that the suffix tree has a rather high memory consumption in practice. 
As an alternative for it, the bioinformaticians will learn about the suffix array 
and later about compact suffix arrays.}
