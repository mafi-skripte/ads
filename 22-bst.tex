%!tex root = ./skript.tex

\chapter{Binary Search Trees}

A simple way to implement the ADT ordered dictionary is via binary search trees.

\textbf{Definition:} An (ordered) \emph{binary tree} is defined inductively 
as follows: (i) the \emph{empty tree} $\perp$ is a binary tree; and
(ii) if $T_\ell$ and $T_r$ are binary trees, the we can obtain a binary
tree by creating a \emph{root node} $v$ and  by making $T_\ell$ the \emph{left subtree}
of $v$ and $T_r$ the \emph{right subtree} of $v$. If $T_\ell$ is not empty, we create
an \emph{edge} between $v$ and the root node $w_\ell$ of $T_\ell$, and we call $w_\ell$ 
the \emph{left child} of $v$. Similarly, if $T_\ell$ is not empty, we
create an edge between $v$ and the root node $w_r$ of $T_r$, and we call $w_r$ the
\emph{right child} of $v$. We call $v$ the \emph{parent node} of $w_\ell$ and $w_r$ (if
they exist). 

Unravelling the definition, we see that a binary tree $T$ consists of nodes and edges,
that are connected in a hierarchical fashion. A node $v$ that does not have any children (that is, both
subtrees of $v$ are empty) is called a \emph{leaf}. A node $v$ that  is not a leaf (that is, $v$
has at least one child) is called an \emph{interior node}.
For every node $v$ in $T$, the number of edges that connect $v$ to the root of $T$ is called the
\emph{depth} of $v$ or the \emph{level} of $v$ (note that the root has level $0$). The maximum depth
over all nodes in $T$ is called the \emph{height} of $T$.
If a node $w$ lies in a subtree of a node $v$, then $w$ is called a \emph{descendant} node of
$v$, and $v$  is called an \emph{ancestor} node of $w$. Otherwise, if $w$ does not lie in in a subtree
of $v$, and if $v$ does not lie in a subtree of $w$, we call $v$ and $w$ \emph{unrelated}.

\textbf{Definition:} Let $K$ be an ordered set of keys and $V$ be a set of entries. 
A \emph{binary search tree} is an (ordered) binary tree $T$ in which the nodes store
entries from $K \times V$. The keys of the entries must fulfill the 
\emph{binary search tree property}: For every node $v$ of $T$, let $k$ be the key of
the entry that is stored in $v$. Then, for all keys $k'$ in the
left subtree of $v$, we have $k' < k$, and for all keys $k'$ in the 
right subtree of $v$, we have $k' > k$.

\begin{center}
	\includegraphics{figs/02-ordereddict/22-01-bst_example}
\end{center}

\paragraph{Implementation.}
We can implement a binary search tree as a collection of \emph{node objects} that
are connected through references. A node object has \texttt{n} has the following attributes:
\begin{itemize}
	\item \texttt{k}, \texttt{v}: the key and the value stored in \texttt{n}.
	\item \texttt{parent}: a reference to the node object for the parent \texttt{n}
		(NULL if \texttt{n} is the root).
  \item \texttt{left}, \texttt{right}: references to the objects for the left and the right
	  child of \texttt{n} (NULL if these children do not exist).
\end{itemize}

Now we can implement the operations as follows:

\paragraph{The operation \texttt{get}.}
To search for a key $k$ in the binary search tree, we start at the
root, and in each step, we compare $k$ to the key $k'$ in the current node \texttt{n}.
If $k = k'$, we have found the desired entry. If $k < k'$, we proceed to the left
child of \texttt{n}, and if $k > k'$, we proceed to the right child of \texttt{n}.
If we reach an empty tree (that is, if the current node becomes NULL, we know that
$k$ is not present in the tree.
The pseudo-code is as follows:
\begin{verbatim}
get(k)
    // start at the root
    n <- root
    // as long as we have not reached an empty tree
    while n != NULL do
        // is k in the current node?
        if n.k == k then
            // return the corresponding value
            return n.v
        // is k smaller than the current key?
        if k < n.k then
            // go to the left subtree
            n <- n.left
        // otherwise, k is larger than the current key?
        else
           // go to the right subtree 
           n <- n.right
  // we have reached an empty tree. This means that k is not in the tree
  throw NoSuchElementException
\end{verbatim}

\begin{center}
	\includegraphics{figs/02-ordereddict/22-02-bst_get.pdf}
\end{center}

\paragraph{The operation \texttt{put}.} 
To update/insert an entry $(k v)$, we first
locate the key as in \texttt{get}. If we find $k$, we simply update the value and
return. If we do not find $k$, the search ends at and empty tree that marks precisely
the location where the entry $(k, v)$ should be inserted. We create a new node for
$(k, v)$ and add it to the tree.

\begin{verbatim}
put(k,v)
    // is the tree empty?
    if root == NULL then
        // create a new root node and return
        root <- new node for (k, v)
       return
    // otherwise, we locate the position for k, starting at the root
    n <- root
    while true do
        // is k in the current node?
        if n.k == k then
            // update the value and return
            n.v <- v
            return
         // is k smaller than the current key?
         if k < n.k then
             // if the left subtree is not empty, go there
             if n.left != NULL then
                 n <- n.left
             // if not, create a new node for (k, v) and return
             else
                 n.left <- new node for (k, v)
                 return
         // otherwise, k is larger than the current key
         else
             // if the right subtree is not empty, go there
             if n.right != NULL then
                 n <- n.right
             // if not, create a new node for (k, v) and return
             else
                n.right <- new node for (k, v)
                return
\end{verbatim}

\begin{center}
    \includegraphics{figs/02-ordereddict/22-put example.pdf}
\end{center}

\paragraph{The operation \texttt{remove}.} To delete an entry for a key $k$ from a
binary search tree $T$, we first locate the node $n$ that stores the entry for $k$.
If no such node exists, $k$ is not in $T$, and we raise an error.
If $n$ is a leaf, we can simply remove $n$ from $T$ by replacing it with an empty tree
(if $n$ is also the root, this means that $T$ becomes empty). 
If $n$ has only one non-empty subtree, we replace $n$ by this subtree.
If $n$ has two non-empty subtrees, we find the node $m$ with the predecessor entry for $k$ in the
left subtree of $n$. We replace the entry in $n$ with the entry in $m$, and
we remove $m$ from $T$. The latter removal operation is easy, because now we know
that $m$ has at most one non-empty subtree.

\begin{verbatim}
remove(k)
    // locate the node n that contains k
    n <- root
    while n != NULL && n.k != k do
        if k < n.k then
            n <- n.left
        else
            n <- n.right
    if n == NULL then
        throw NoSuchElementException
    // if at least one subtree of n is empty, replace n by the other one
    if n.left == NULL then
        // replace n by the right subtree
        // we show the necessary pointer operations only once
        // Is the right subtree of n nonempty?
        if n.right != NULL then
            // If so, update the parent pointer for the right child
            n.right.parent <- n.parent
        // Does n have a parent?
        if n.parent != NULL then
            // If so, update the correct child of this parent
            if n.parent.left == n then
                n.parent.left <- n.right
            else
                n.parent.right <- n.right
        // if n does not have a parent, it is the root
        else
            root = n.right
        return
    else if n.right == NULL then
        replace n by its left child
        return
    // now, n does not have an empty subtree
    // we locate the node m that contains the
    // predecessor entry for k
    m <- n.left
    while m.right != NULL do
        m <- m.right
    // now, m contains the predecessor entry for k,
    // and m does not have a right child
    move the predecessor entry from m to n 
      (overwriting the entry for k)
    replace m by its left subtree
\end{verbatim}

\begin{center}
    \includegraphics[page=1, width=0.8\textwidth]{figs/02-ordereddict/22-remove example.pdf}\\
    \vspace{1em}
    \includegraphics[page=2, width=0.8\textwidth]{figs/02-ordereddict/22-remove example.pdf}\\
    \vspace{1em}
    \includegraphics[page=3, width=0.8\textwidth]{figs/02-ordereddict/22-remove example.pdf}\\
    \vspace{1em}
    \includegraphics[page=4, width=0.8\textwidth]{figs/02-ordereddict/22-remove example.pdf}
\end{center}

The details of the implementation for the remaining operations of the ADT
ordered dictionary on a binary search tree are left as an exercise.

\paragraph{Analysis.}
Let us now analyze the performance of the binary search tree operations. First, correctness can be shown
in a relatively easy manner: to see that the look-up operation \texttt{get} is correct,
we can proceed by an induction on the height of the binary search tree to show that \texttt{get}
will find the entry for the given $k$, if it exists in the tree. From this, we can also
derive that \texttt{put} is correct, using that the position of the 
empty tree where the new node is inserted preserves the binary search tree property.
Finally, to show correctness of \texttt{remove}, we distinguish cases to show that after the
deletion, the binary search tree property is preserved.
The details are left as an exercise.

What can we say about the running time of the binary search tree operations? An inspection of the
code shows that all three operations \texttt{get}, \texttt{put}, \texttt{remove} proceed by
a single descent down the tree, possibly backtracking along the path in the case of \texttt{remove}.
The running time of the operations is proportional to the number of nodes along this path.
Thus, in the worst case, we need to proceed to a leaf that has the largest depth in the tree. 
In this case, the running time is proportional to the \emph{height} of the tree.
Thus, we can conclude that in the worst case, the running time of \texttt{get}, \texttt{put}
and \texttt{remove} in a binary search tree $T$ is $\Theta(h)$, where $h$ is the height of $T$.

What can we say about the height of a binary search tree $T$ that stores $n$ entries?
In the worst-case, not much: it can happen that every node in $T$ has only one child, in which
case the height is $\Theta(n)$. For example, this happens if we insert entries with keys $1, 2, \dots, n$
in this order into an initially empty binary search tree. In this case, we might as well store
our entries in a linked list and do without the additional complications of implementing a binary
search tree.

\begin{center}
    \includegraphics*{figs/02-ordereddict/22-worst case tree.pdf}
\end{center}

However, the situation can be much better: we say that $T$ is a \emph{perfect} binary tree
if all levels of $T$ (except possibly the last level) contain as many nodes as possible
(we already saw perfect binary trees when we talked about binary heaps in \emph{Konzepte der
Programmierung}). In this case, the height of $T$ will be $\Theta(\log n)$, where $n$ is the
number of entries, and the running time for our operations will be \emph{exponentially faster}
than in the worst case. Even more encouragingly, we can show that, e.g., if the keys inserted
in a \emph{random order} into an initially empty binary search tree, the \emph{expected}
height of $T$ will be $\Theta(\log n)$.

\begin{center}
    \includegraphics{figs/02-ordereddict/22- perfect tree.pdf}
\end{center}

Now, we have two options: first, we can accept that the \emph{worst-case} performance 
of our ordered dictionary implementation can be bad, and seek solace in the knowledge
that the \emph{average case} will be much better and hope that our operation sequence
will contain enough randomness so that the average case is more representative for what
happens. From this, we gain a simpler implementation, but at the cost of more uncertainty
of the algorithmic performance. Second, we can try to adapt our binary search tree implementation
to make sure that the worst-case cannot occur. In the next few chapters, we will see 
ways of how to achieve this.
