%!tex root = ./skript.tex

\chapter{String Similarity--Longest Common Subsequence}
\label{ch:lcs}

Suppose we are given two strings $s, t \in \Sigma^*$,
and we would like to determine how \emph{similar} the two
strings are. This task occurs often in applications, e.g.,
when comparing DNA or protein sequences of different species or when
trying to determine the history of a text file. 
\Knut{The bioinformaticians will address many variations of computing 
similarity or distance between biological sequences in the next semester. 
They will especially learn about the evolutionary context of similarity and distance}.

Lets look at one possible, basic formulation  for determining the similarity of two strings.

To answer this question algorithmically, we first need
to have a formal mathematical definition of \emph{similarity}.
Only when we know exactly what we are talking about will we
be able to design an efficient algorithm for the problem.

There are different ways to define the notion of string similarity.
One approach is to identify a large portion of text that occurs in
both $s$ and $t$. This is formalized be the notion of a \emph{common
subsequence}.

\begin{definition} Let $s$ and $t$ be two strings over $\Sigma$.
A \demph{common subsequence} of $s$ and $t$ is a sequence of symbols
from $\Sigma$ that appears both in $s$ and $t$ in the same order, but not
necessarily consecutively. Formally, let $s = \sigma_1 \dots \sigma_k$ and
$t = \tau_1 \dots \tau_\ell$. Then, a common subsequence of $s$ and $t$
of length $n$ consists of a sequence of  $n$ index pairs
$(i_1, j_1), (i_2, j_2), \dots, (i_n, j_n)$ such that
\begin{enumerate}
\item $1 \leq i_1 < i_2 < \dots < i_n \leq k$;  
\item
$1 \leq j_1 < j_2 < \dots < j_n \leq \ell$; and 
\item 
$\sigma_{i_a} = \tau_{j_a}$, for all $a = 1, \dots, n$.
\end{enumerate}

A common subsequence of $s$ and $t$ is a \demph{longest common subsequence}
if there exists no common subsequence that contains more pairs.
\end{definition}

This definition leads to a simple way to determine the similarity of
two strings $s$ and $t$: first, we compute a longest common subsequence of $s$ and
$t$. Second, we find the symbols that occur in $s$ but not in $t$, and
the symbols that occur in $t$ and not in $s$. Then, we can visualize the strings next to each
other, marking the symbols that do not occur in the longest common subsequence with
special colors. This approach is implemented, for example, in the standard operating
system utilities \texttt{diff}, \texttt{fc}, or \texttt{cmp}.

We will now discuss an efficient algorithm to compute the longest common
subsequence of $s$ and $t$.

\paragraph{Finding the length of a longest common subsequence.}
Let 
$s = \sigma_1 \sigma_2 \dots \sigma_k$ and 
$t = \tau_1 \tau_2 \dots \tau_\ell$
two strings. 
First, we develop an algorithm to compute the \emph{length} of the 
longest common subsequence of $s$ and $t$.
After that, we will discuss how to extend this to determine an actual LCS.

The main idea is to use a recursive approach. The main challenge
is to find an appropriate recursion that can be used to solve the problem.
For the LCS-problem, it is useful to consider the following subproblems:
given two integers $i \in \{0, \dots, k\}$ and $j \in \{0, \dots, \ell\}$,
find the length of the  longest common subsequence for the \emph{prefixes} 
$\sigma_1 \dots \sigma_i$ and $\tau_1 \dots \tau_j$ of $s$ and $t$ (if $i = 0$
or $j = 0$, the respective prefix is the empty string $\varepsilon$).
We call this function $\text{llcs}(i, j)$.

Now, we would like to find a recursion for $\text{llcs}(i, j)$.
First, we have that $\text{llcs}(0, j) = 0$, for all $j = 0, \dots, \ell$,
since the empty string does not contain any non-empty subsequence.
Similarly, we have $\text{llcs}(i, 0) = 0$, for all $i = 0, \dots, k$.

For $i, j \geq 1$, we need to consider two cases:
first, if $\sigma_i = \tau_j$, then we can obtain an LCS
for $\sigma_1 \dots \sigma_i$ and $\tau_1 \dots \tau_j$ by taking an LCS
for $\sigma_1 \dots \sigma_{i - 1}$ and $\tau_1 \dots \tau_{j - 1}$ and by adding
$(i, j)$. Second, if $\sigma_i \neq \tau_j$, then we can
find an LCS for $\sigma_1 \dots \sigma_i$ and $\tau_1 \dots \tau_j$ as follows:
take an LCS for $\sigma_1 \dots \sigma_{i - 1}$ and $\tau_1 \dots \tau_j$ and
an LCS for for $\sigma_1 \dots \sigma_i$ and $\tau_1 \dots \tau_{j - 1}$. 
Then, the longer of the two LCS's will also be an  LCS for
$\sigma_1 \dots \sigma_i$ and $\tau_1 \dots \tau_j$.
% TODO: Make this a formal proof.}

This means that $\text{llcs}$ fulfills the following recurrence:
\begin{align*}
	\text{llcs}(i, j) &= 1 + \text{llcs}(i - 1, j - 1),\text{ if $\sigma_i = \tau_j$, and}\\
	\text{llcs}(i, j) &= \max\left\{\text{llcs}(i, j - 1), \text{llcs}(i - 1, j)\right\}, 
	\text{ if $\sigma_i \neq \tau_j$.}
\end{align*}

We can implement this recursion directly. However, this will not be very efficient.
In fact, the running time could become \emph{exponential} in $k$ and $\ell$,
because the second case in the recursion performs \emph{two} recursive calls instances
that are only smaller by a single character. Nonetheless,  when unravelling the recursion
tree, we see that there are actually only $(k + 1) \cdot (\ell + 1)$ subproblems, and that
many subproblems are considered repeatedly during the recursion.

One way to take advantage of this structure is called \emph{memoization}: we implement the recursion
directly, but we maintain a dictionary that contains the solutions to all the subproblems that
we have already considered. Before performing a recursive call, we first consult the dictionary,
and if the answer has already be computed, we refrain from repeating the calculation.
This leads to an algorithm with a polynomial running time, using a dictionary structure.

A second, conceptually simpler way to reduce the running time is called \emph{dynamic programming}.
Here, we do not implement the recursion directly, but we systematically build a table with
the solutions for all the subproblems, from small to large.
Let \pa|LLCS| be a two-dimensional array of
\pa|Integer|'s with dimension
$(k + 1) \times (\ell + 1)$.
We fill  \pa|LLCS| systematically bottom-up,
using the recurrence relation from above. This can be implemented with
simple nested \pa|for|-loops.
\begin{pascal}
// Initialization
for i := 0 to k do
    LLCS[i,0] := 0 
for j := 0 to l do
    LLCS[0,j] := 0
// Implementing the recursion
for i := 1 to k do
    for j := 1 to l do
        if s[i] == t[j] then
            LLCS[i,j] := 1 + LLCS[i-1,j-1]
        else
            LLCS[i,j] := max(LLCS[i-1,j], LLCS[i,j-1])
\end{pascal}

The result is in  $\text{\pa|LLCS|}[k,\ell]$. The running time is 
$O(k\ell)$. 

\paragraph{Computing an actual LCS}
After $\text{\pa|LLCS|}[k, \ell]$ has been found, we can
determine the longest common subsequence by 
reconstructing the path through \pa|LLCS|  that led to
the solutions.
This is done from the back of the table, starting at 
$\text{\pa|LLCS|}[k, \ell]$.
\begin{pascal}
// a stack to store the result
S := new Stack
// start at the end 
(i,j) := (k,l)
while i>0 and k>0 do
    if s[i] == t[j] then
        // if the current symbols are equal, we can include 
        // them into the sequence
        (i,j) := (i-1, j-1)
        S.push(s[i])
    else
        // otherwise we need to determine which of the two  
        // choices led to the maximum
        if LLCS[i,j-1] > LLCS[i-1,j] then
            (i,j) := (i,j-1)
        else
            (i,j) := (i-1,j)
// now S contains an optimal subsequence
while not S.isEmpty() do
    output S.pop()
\end{pascal}

Since in each iteration of the \pa|while|-loop at least one index
\pa|i| or \pa|j| decreases by one, the running time is $O(k + \ell)$.

\textbf{Remark}. The dynamic programming algorithm for finding an LCS takes
time $O(k \ell)$. As with string search, one may ask if this can be improved:
in string search, we had the naive algorithm that took $O(k \ell)$ time, but using
more sophisticated methods like, e.g., Knuth-Morris-Pratt, this could be improved to 
$O(k + \ell)$. However, it turns out that in the case of computing the LCS,
such an improvement is unlikely: one can show that, assuming the \emph{strong
exponential time hypothesis}, a popular complexity-theoretic assumption, there is
no algorithm for computing the LCS that takes time $O((k\ell)^{1-\varepsilon})$, for
any fixed $\varepsilon > 0$.

\paragraph{Dynamic programming.} The algorithm for computing the LCS is a typical
example of a general algorithmic paradigm called \emph{dynamic programming}.\footnote{The
term \emph{dynamic programming} was coined by Bellman in the early days of computer science.
Here, ``programming'' does not refer to ``writing code for a computer'', but to solving
an optimization problem (as in, ``creating a TV program''). ``Dynamic'' is a meaningless
word that was chosen to impress bureaucrats that  make decisions on research funding.
Nowadays, the technique might be called ``interdisciplinary optimization epistemology''.}

The general idea of dynamic programming is to solve an algorithmic problem by 
\emph{recursion}. Typically, in a dynamic programming scenario, the subproblems
overlap and throughout the recurrence, the same subproblem occurs multiple times.
Thus, in dynamic programming, we create a table for all possible subproblems and
we fill the table systematically from the smallest to the largest subproblem.
The resulting algorithms are often quite elegant, consisting only of a few nested
\pa|for|-loops. 

Dynamic programming is applicable to a wide array of problems, and we will see several
other examples throughout this class and in other classes (e.g., the algorithm of
Floyd-Warshall for solving the all-pairs-shortest-paths problem in graphs, the
algorithm of Kleene for converting a deterministic finite automaton to a regular
expression, the algorithm of Cocke-Younger-Kasami for the word-problem in context-free
languages, etc.). The main challenge in dynamic programming is to come up with a good
recursive formulation for the problem at hand. Once the recursion is found, the rest
is usually routine.
