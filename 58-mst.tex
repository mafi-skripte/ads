%!tex root = ./skript.tex

\chapter{Minimum Spanning Trees}

We turn to another classic algorithmic problem on graphs.
Suppose we have a collection of cities $C_1, C_2, \dots, C_n$,
and we would like to build a railroad network. 
In principle, we can build a connection between any pair of
cities, but due to geographic realities, some connections can be
much cheaper than others.
Suppose that we have already hired a construction company, and for each
pair $C_i, C_j$ of cities, the company has determined an estimate
$\ell_{ij}$ of the cost for building a direct railroad connection
between $i$ and $j$. Now, our task is as follows: we would like
to select a set of railroad connections such that (i) it is possible
to travel from each city to each other; and (ii) the total cost
of the railroad connections is as small as possible.\footnote{In this
version of the problem, we just care about connectivity. We do not
consider the travel time between cities.}

This problem can be formalized as follows: we are given 
an undirected, connected  graph $G = (V, E)$ and a 
\emph{weight function} $\ell: E \rightarrow \mathbb{R}^+$
that assigns a positive cost to each edge. The goal is to
select a set $T \subseteq E$ of edges such that (i) the
subgraph $(V, T)$ of $G$ is connected; and (ii) to total
cost $\sum_{e \in T} \ell(e)$ of $T$  is as small as possible,
among all $T \subseteq E$ that lead to a connected subgraph.

Since all weights are positive, we see that the subgraph $(V, T)$
cannot have any cycles: it there were a cycle $C$ in $(V, T)$,
we could remove an arbitrary edge $e$ of $C$. The resulting graph 
$(V, T \setminus \{e\})$ would
still be connected, and the total edge weight would be strictly smaller
than for $(V, T)$. Thus, the subgraph $(V, T)$ is a \emph{spanning tree}
for $G$. Because it has the smallest weight, 
it is called a \emph{minimum spanning tree} (MST) for $G$.

\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/27-01-mst}
	\end{center}
	\caption{MST.}
\end{figure}


We would now like to design an algorithm to construct a minimum
spanning tree for $G$. Our approach is to try a \emph{greedy} strategy.
We would like to start with the empty set $A = \emptyset$, and to add
edges to $A$, one by one, until $A$ is a minimum spanning tree. We capture this
idea in the following definition:

\textbf{Definition:} Let $G = (V, E)$ be an undirected, connected graph,
and let $\ell: E \rightarrow \mathbb{R}^+$ be a positive weight function.
A set $A \subseteq E$ of edges is called \emph{safe} if there exists an MST 
$T \subseteq E$ such that $A \subseteq T$.

In other words, an edge set $A$ is safe if and only if it is still possible
to extend $A$ to an MST for $G$ by adding more edges. Since $G$ is connected,
an MST for $G$ always exists, and hence $A = \emptyset$, the set that contains
no edges, is safe. This is our starting point.

Now, all we need is a criterion that allows us to extend a given safe set by one more edge,
resulting in a larger safe set. To state such a criterion, we need one more definition:

\textbf{Definition:} Let $G = (V, E)$ be an undirected, connected graph.
Let $S \subset V$, $S \neq \emptyset, V$ be a nontrivial
subset of vertices in $G$.
Then, the partition $(S, V \setminus S)$ is called a \emph{cut} of $G$.
We say that an edge $e \in E$ \emph{crosses} the cut $(V, V \setminus S)$
if $e$ has exactly one endpoint in $S$ and one endpoint in $V \setminus S$.
Otherwise, we say that $e$ \emph{respects} the cut.

The following lemma tells us when we can add an edge to a safe set $A$:
we choose a cut $(S, V \setminus E)$ that respects all edges in  $A$, and we choose 
an edge of minimum weight that crosses the cut. More formally, the statement is as follows:

\begin{lemma}
	\label{lem:safe_set}
Let $G = (V, E)$ be an undirected, connected graph,
and let $\ell: E \rightarrow \mathbb{R}^+$ be a positive weight function.
Let $A \subseteq E$ be a safe set of vertices. 
Let $S \subset V, S \neq \emptyset, V$ be a nontrivial subset of vertices in $V$,
such that all edges of $A$ respect the cut $(S, V \setminus S)$.

Now, the following holds: among all edges $f$ that cross the cut $(S, V \setminus S)$,
let $e$ be an edge such that the weight $\ell(e)$ is minimum. Then, the set
$A \cup \{e\}$ is safe.
\end{lemma}

\begin{proof}
Since $A$ is safe, there exists an MST $T \subseteq E$ such that $A \subseteq T$.

If $e \in T$, then we are done: in this case, we have $A \cup \{e\} \subseteq T$,
and $A \cup \{e\}$ is safe, as witnessed by $T$.

Thus, suppose that $e \not\in T$. Consider the suggraph $(V, T \cup \{e \})$.
Since $T$ is a tree, and since $e \not \in T$, 
it follows that adding $e$ to $T$ creates a cycle. We call
this cycle $C$, and we note that $e$ is an edge of $C$.
Since (i) $C$ is a cycle; (ii) $e$ is an edge of $C$;  and (iii) $e$ crosses
the cut  $(S, V \setminus S)$, it follows that $C$ contains another edge
$f \neq e$ that also crosses the cut $(S, V \setminus S)$.
By the choice of $e$, we have $\ell(e) \leq \ell(f)$. 

Now, consider the edge set $T' = (T \cup \{e\}) \setminus \{f\}$.
Then, since $e$ and $f$ lie in a common cycle, the subgraph
$(V, T')$ is connected, and hence a spanning tree for $G$. Furthermore,
because $\ell(e) \leq \ell(f)$, the total weight of $T'$ is not larger
than the total weight of $T$. Thus, $T'$ is also an MST for $G$.
Finally we have that $A \cup \{e\} \subseteq T'$. Thus, 
$A \cup \{e\}$ is safe, as claimed.
\end{proof}

As in Huffman codes, Lemma~\ref{lem:safe_set} is an \emph{exchange lemma}:
it shows that if we have an MST $T$, we can locally modify $T$ to obtain
a new MST $T'$ that contains the desired edge~$e$.

With Lemma~\ref{lem:safe_set} at hand, the strategy for a general MST-algorithm
is clear: we start with the empty set $A = \emptyset$, and we successively add
edges to $A$, until the MST is complete. In each step, we must construct a suitable
cut $(S, V \setminus S)$ that respects $A$, and to find an edge of minimum weight
that crosses this cut. There are different ways how this can be done. Depending on
our choice of the cut, there are different concrete  MST-algorithms that follow this
general strategy. We will look at a few of them in more detail.

\paragraph{The algorithm of Prim-Jarn\'ik-Dijkstra.}
The algorithm of Prim-Jarn\'ik-Dijstra lets the MST grow from a single
source vertex $s \in V$. The strategy is as follows: we pick an 
arbitrary vertex $s \in V$, and we set $A = \emptyset$ and $S = \{s\}$.
In each step, we select the lightest edge $e$ that crosses the cut $(S, V \setminus S)$.
Let $w$ be the endpoint os $e$ that lies in $V \setminus S$. We add $e$ to $A$ and
$w$ to $S$.

In this way, we have that $A$ defines a tree on the vertex set $S$, and in
each step, we add one more vertex and one more edge to the tree. Since all edges
of $A$ have their endpoints in $S$, it follows that the cut $(S, V \setminus S)$ respects
$A$. Since in each step, we pick a lightest edge that crosses the cut, we ensure that $A$
stays safe throughout the algorithm. Thus, the resulting tree $T$ is an MST for $G$.

It remains to discuss the details of the implementation. We need to maintain
the cut $(S, V \setminus S)$. Furthermore, in each step, we must be able
to determine the lightest edge $e$ that crosses the current cut.
The idea is to store the vertices of $V \setminus S$ in a priority queue $Q$.
The key for a vertex $v$ in $Q$ is the weight of the lightest edge that connects
$v$ to a vertex in $S$. We call this attribute $v.\texttt{d}$. Furthermore, for each
vertex $v$ in $Q$, we store the other endpoint of the lightest edge that connects
$v$ to $S$. We call this attribute $v.\texttt{pred}$. Initially, all the \texttt{d}-attributes
are set to $\infty$, and all the \texttt{pred}-attributes are set to $\perp$.

In each step, we remove the vertex $v$ with the smallest key from the $Q$. This means that
we add $v$ to $S$ and the edge $\{v.\texttt{pred}, v)\}$ to $A$.\footnote{Actually, the situation
in the first iteration is slightly different: there, we remove $s$ from $Q$ and add it to $S$.
In this iteration, we do not add any edge to $A$.
This is done to simplify the algorithm, so that no special treatment for $s$ is necessary. }
Since $v$ moves from
$V \setminus S$ to $S$, there are new edges that cross the cut. These are exactly those edges
$e$ that have $v$ as one endpoint and whose other endpoint remains in $Q$. For each such
edge $e = \{v, w\}$, we must check whether $e$ has now become the lightest edge that connects
$w$ to $S$. If this is the case, we must update the attributes $w.\texttt{d}$ and 
$w.\texttt{pred}$ for $w$. In the end, the edges of $A$, and hence the MST,  
can be deduced from the \texttt{pred}-attributes.

The pseudocode is as follows:
\begin{verbatim}
Q <- new PrioQueue
// initially, we put all vertices into Q
for v in vertices() do
  v.d <- INFTY; v.pred <- NULL
  Q.insert(v, v.d)
// We set s.d to 0, to ensure
// that s is removed first from Q
s.d <- 0
Q.decreaseKey(s, s.d)
while not Q.isEmpty() do
  v <- Q.extractMin()
  // we implicitly add v to S and the edge (v.pred, v) to A
  // we must check all the edges that go from v to vertices in Q
  for w in v.neighbors() do
    if w is in Q and l(v, w) < w.d then
      w.d <- l(v, w)
      w.pred <- v
      Q.decreaseKey(w, w.d)
\end{verbatim}

\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/27-02-Prim}
	\end{center}
	\caption{Prim.}
\end{figure}


We note that this algorithm is \emph{almost exactly} the same as Dijkstra's algorithm
for the SSSP-problem. There are only two differences in the inner \texttt{for}-loop:
(i) in the \texttt{if}-test, we explicitly check whether $w$ is in the priority queue,
and the condition for $w.\texttt{d}$ is slightly different 
($\ell(v, w) < w.\texttt{d}$ instead of
$v.\texttt{d}.\ell(v, w) < w.\texttt{d}$); and
(ii) the new value of $w.\texttt{d}$ is computed differently ($\ell(v, w)$ instead
of $v.\texttt{d} + \ell(v, w)$).

In Dijsktra's algorithm, we are greedily growing a shortest path tree from $s$, in
the Prim-Jarn\'k-Dijkstra-algorithm, we are greedily growing a minimum spanning
tree from $s$. Due to the different definitions of these two trees, the algorithms
are slightly different, but the main strategy is the same in both cases.

The analysis for the Prim-Jarn\'k-Dijkstra-algorithm is the same as for Dijkstra's 
algorithm. The main bottleneck lies in the implementation of the priority queue.
Using the best available results for this, we achieve a running time 
if $O(|V| \log |V| + |E|)$.

\paragraph{Kruskal's algorithm.} Kruskal's algorithm uses a more global strategy
for selecting the next safe edge. The idea is to take in each step the \emph{lightest}
edge in $E$ that crosses a cut that respects the current set $A$. How does such an
edge look like? In general, the subgraph $(V, A)$ is a \emph{forest}: a graph on
$V$ that does not contain any cycles, but that consists of several connected components
(some of them may be a single vertex). For an edge $e \in E \setminus A$, there
is a cut $(S, V \setminus S)$ that respects $A$ and that is crossed by $e$ if and only
if the endpoints of $e$ lie in different components of the forest $(V, A)$ (this cut
must separate the components that contain the endpoints of $e$, and the other components
can be assigned arbitrarily).

Thus, Kruskal's algorithm works as follows: sort the edges in $E$ by weight, from the lightest
edge to the heaviest edge. Set $A = \emptyset$. For each edge $e$ in this order, check if
$e$ connects two different components of the current forest $(V, A)$. If so, add $e$ to $A$.
Otherwise, do nothing. Continue, until all edges have been considered.
In other words: in each step, Kruskal's algorithm adds the lightest edge to $A$ that adds
a new connection.
The pseudocode is as follows:
\begin{verbatim}
  A <- {}
  Sort e by weight
  for each edge e in sorted order do
    if e connects two different components of (V, A) then
      A <- A + {e}
  return A
\end{verbatim}
\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/27-03-Kruskal}
	\end{center}
	\caption{Prim.}
\end{figure}


We still need to explain how to check whether an edge $e$ connects
two different components of $(V, A)$. For this, we need a way
to track the connected components of $(V, A)$, as edges
are added to $A$. More precisely, we need to support the
following process: initially, we have $A = \emptyset$, and
each component of $(V, A)$ consists of a single vertex. 
Then, in each round, we are given an edge $\{v, w\}$ in $E$,
and we need to \emph{find} the components of $(V, A)$ that contain
the endpoints $v$ and $w$. If these two components are distinct,
we need to construct the \emph{union} of the two connected components
for $v$ and $w$.

This is called the \emph{union-find-problem}. The abstract problem is as follows:
we are given a set $\mathcal{U} = \{1, \dots, n\}$ with $n$ elements, and our task is to
maintain a \emph{partition} $\mathcal{P} = \{U_1, U_2, \dots, U_k\}$ of $U$ under the following
operations\footnote{Recall that a partition of $U$ is a set
$\mathcal{P} = \{U_1, U_2, \dots, U_k\}$ 
of nonempty, pairwise disjoint subsets  of $U$ such that every element of $U$ occurs
in exactly one set.
Formally, we have (i)  $U_i \subseteq U$ and $U_i \neq \emptyset$,
for $i = 1, \dots, k$; (ii) $U_i \neq U_j$, for $i, j = 1, \dots, k$, $i \neq j$;
and (iii) $\bigcup_{i = 1}^k U_i = U$.
}: 
(i) \texttt{initialize}$(U)$: create a new partition $\mathcal{P} = \{\{1\}, \{2\}, \dots, \{n\}\}$
in which every set consists of a single element; (ii) \texttt{find}$(u)$, for 
$u \in \mathcal{U}$: return a \emph{representative element} $v \in U_i$, where
$U_i$ is the set in the current partition $\mathcal{P}$ that contains $u$. Crucially, we require
that for all $u \in U_i$, the function \texttt{find}$(a)$ returns
\emph{the same} representative $v \in U_i$ (note that it also follows that
for $u_1, u_2 \in U$ that lie in \emph{different} sets of the current partition,
\texttt{find}$(u_1)$ and \texttt{find}$(u_2)$ return \emph{different} representatives);
and (iii) \texttt{union}$(v_1, v_2)$, where $v_1 \in U$ is the representative element for a
set $U_i$ in the current partition and $v_2$ is the representative element for a
different set $U_j$ in the current partition: replace the sets $U_i, U_j$ in the 
current partition $\mathcal{P}$ by their union $U_i \cup U_j$. That is,
the new partition after \texttt{union}$(v_1, v_2)$  is 
$\left(\mathcal{P} \setminus \{U_i, U_j\}\right) \cup \{U_i \cup U_j\}$.

Given a data structure that supports the operations of the union-find problem,
we can now provide a more detailed pseudo-code for Kruskal's algorithm:
\begin{verbatim}
  A <- {}
  initialize(V)
  Sort E by weight
  for each edge e = {v, w} in sorted order do
    a = find(v)
    b = find(w)
    if a != b then
      A <- A + {e}
      union(a, b)
  return A
\end{verbatim}
Now, we analyze the running time of Kruskal's algorithm: it takes $O(|E|\log |E|)$ time
to sort the edges in $E$. Then, we consider all edges. For each edge, we make
two calls to \texttt{find}. Each time we perform a \texttt{union}-operation, we reduce
the number of connected components by one. Thus, there are $2|E|$ calls to \texttt{find}
and $|V| - 1$ calls  to \texttt{union} (we start with $|V|$ connected components, and
we end with one connected component). Below, we will see how to implement the union-find
structure such that each call to \texttt{find} takes $O(1)$ time and such that 
the total time for all the calls to \texttt{union} in $O(|V| \log |V|)$. Since $G$ is
connected, we have $|E| \geq |V| -  1$, so the total running time for Kurskal's algorithm
is dominated by the sorting step: $O(|E| \log |E|)$.

\paragraph{The union-find-structure.} We describe a simple way to 
implement the union-find structure. Let $\mathcal{U} = \{1, \dots, n\}$
be given. 
We store the sets of the current partition $\mathcal{P}$ as a collection of
linked lists, such that for each set, there is a linked list that contains the
elements of the sets. We assume that all the linked lists have pointers to the
first and to the last element in the list, so that we can join/concatenate two linked
lists in constant time.
Additionally, each element $u \in \mathcal{U}$ stores the following 
attributes: 
\begin{itemize}
	\item $u.\texttt{rep}$: the \emph{representative} of the set that contains $u$;
	\item $u.\texttt{setSize}$: the \emph{size} of the set that $u$ represents. This
		field is valid only if $u$ is a representative of a set.
	\item $u.\texttt{set}$: the linked list that stores all the elements 
		of the set that $u$ represents. This
		field is valid only if $u$ is a representative of a set.
\end{itemize}
Initially, each set in $\mathcal{P}$ consists of a single element. For each such
set $\{u\}$, we create a linked list that contains only $u$. The representative of
the set $\{u\}$ is $u$, and the size of the set is $1$. Thus, the pseudocode for
\texttt{initialize}$(U)$ is as follows:
\begin{verbatim}
initialize(U):
  for u in U do
    u.rep <- u
    u.setSize <- 1
    u.set <- linked list that contains only u
\end{verbatim}
The running time for \texttt{initialize} is $O(n)$.
The \texttt{find}-operation is easy: since each element in $U$ stores
a direct reference to the representative of the current set that contains
it, we can directly return the representative:
\begin{verbatim}
find(u):
  return u.rep
\end{verbatim}
The running time for \texttt{find} is $O(1)$.
The \texttt{union}-operation is slightly more complicated. We need to choose a representative
for the new set, and we need to update all the representatives accordingly. To make \texttt{union}
efficient, we choose the representative of the \emph{larger} set  as the new representative.
In this way, we only need to update the representatives for the elements in the smaller set.
Finally, we also need to concatenate the linked lists into a new list for the union.
The pseudocode is as follows:
\begin{verbatim}
union(v1, v2):
  // make sure that v1 represents the larger set
  if v1.setSize < v2.setSize then
      swap(v1, v2)
  // update all the representatives in the smaller set
  for u in v2.set do
      u.rep <- v1
  // update the attribute for the new set 
  v1.setSize <- v1.setSize + v2.setSize
  v1.set <- join(v1.set, v2.set)
  // invalidate the atributes of v2, since
  // v2 is no longer a representative of any set
  v2.setSize <- -1
  v2.set <- NULL
\end{verbatim}
The running time of \texttt{union} is $O(1)$, except for the
time that is needed to update the representatives for the
set of $v_2$. In a single call, this may take a long time.
However, we claim that over all calls to \texttt{union}, the
total time to update the representatives is $O(n \log n)$.

Indeed, consider an element $u \in U$. Every time that 
$u.\texttt{rep}$ changes, this means that the set $U_i$ that contains
$u$ is united with a set $U_j$ that is at least as large as $U_i$.
In other words, if $u.\texttt{rep}$ changes, then the size of
the set that contains $u$ increases by a factor of at least $2$.
Since $u$ starts out in a set of size $1$, and since $u$ ends in
a set of size at most $n$, this can happen at most $O(\log n)$ times.
It follows that for each element in $U$, the representative changes
at most $O(\log n)$ times, and hence the total time for all calls to
\texttt{union} is $O(n \log n)$.

\textbf{TODO}: Say something about the inverse Ackermann function.

\paragraph{Boruvka's algorithm.}
Boruvka's algorithm is the third classic MST-algorithm. It is in fact
the oldest MST-algorithm, and it can be seen as a combination of 
Kruskal's algorithm and the algorithm of Prim-Jarn\'k-Dijkstra.
Like Kruskal's algorithm, Boruva's algorithm maintains a forest $(V, A)$,
instead of a single tree. Like Prim's algoirthm, Boruvka's algorithm makes
sure that each tree in the forest $(V  A)$ grows in each step.

The details are as follows: we assume that all edge 
weights in $G$  are pairwise distinct.\footnote{By a standard
trick, we can make sure that this is always the case: we number the 
edges of $G$ arbitrarily from $1$ to $|E|$, and if two edges have
the same weight, we break the tie by preferring the edge with the smaller index.} 
As usual, we start
with the empty edge set $A = \emptyset$. In each step, we have
a current forest $(V, A)$, and for each connected component $C$ of $A$,
we pick a lightest edge $e_C$ among all edges that have  exactly one endpoint in $C$.
Then, we add all these edges $e_C$ to $A$. 

In other words, Boruvka's
algorithm picks one safe edge for each connected component of the  current
forest $(V, A)$,  and it adds all of them to $A$. Indeed, if $e_C$ is a lightest
edge that has exactly one endpoint in a connected component $C$ of $(V, A)$,
then the cut $(C, V \setminus C)$ respects $A$ and has $e_C$ as a lightest
crossing edge. This means that $e_C$ is safe for $A$. One can show that if all edge
weights in $G$ are pairwise distinct, then the MST of $G$ is unique, so that
it is correct to add all these safe edges simultaneously.
The pseudo-code for the algorithm is as follows:

\begin{verbatim}
A <- {}
while |A| < n - 1 do
  B <- {}
  for each connected component C of (V, A) do
    find the lightest edge e with exactly one endpoint in C
    add e to B (if e is not in B yet) 
  add all edges in B to A
\end{verbatim}
We argue that each iteration of the  \texttt{while}-loop can be implemented in $O(|E|)$
time:
\begin{itemize}
  \item First, we use BFS in $(V, A)$, in order to find the connected components.
      With each vertex $v \in V$, we store the id of the
      connected component that contains $v$.
    This takes  $O(|V| + |A|) = O(|E|)$ time, since  $|V|, |A| = O(|E|)$.
  \item Second, for each connected component $C$ if $(V, A)$, we find
	  the lightest edge $e_C$ that has exactly one endpoint in $C$.
	For this, we iterate over all edges $e \in C$, and we use
	the  component ids that are stored with  the endpoints of $e$ to identify
	the incident components of $e$. If these components are distinct,
	we use the component ids as indices in an array in which we store the
	lightest edge that is incident to each connected component, updating
	this edge if necessary.
        All this can be done in  $O(|E|)$ time.
\end{itemize}

Now, we show that the number of iterations in Boruvka's algorithm
is at most $O(\log |V|)$.
\begin{lemma}
	\label{lem:boruvka_double}
 After $i$ iterations of the \texttt{while}-loop,
each connected component of $(V, A)$ has at least
 $2^i$ nodes.
\end{lemma}

\begin{proof}
	We use induction on $i$. Initially, before the first iteration
	of the \texttt{while}-loop, we have $A = \emptyset$, and each
	connected component of $(V, A)$ has exactly $1 = 2^0$ vertices.
	Thus, the statement holds for $i = 0$.

	For the inductive step, we assume that before iteration $i + 1$
	 of the \texttt{while}-loop, each connected component
	in $(V, A)$ has at least $2^i$ nodes.  Then, during the iteration,
	each component is united with at least one other component (it may
	be more, but certainly at least one). Thus, after iteration $i + 1$,
	each remaining connected component has at least $2 \cdot 2^i = 2^{i + 1}$
	vertices. This finished the inductive step from $i$ to $i + 1$.
\end{proof}

Since a connected component can have it most $|V|$ vertices,
Lemma~\ref{lem:boruvka_double} implies that after 
$O(\log |V|)$ iterations of the \texttt{while}-loop, there
is only one connected component in $(V, A)$, which means that we are done.
It follows that the running time of Boruvka's algorithm is  $O(|E|\log|V|)$.

\paragraph{More on MST-algorithms.}
\textbf{TODO}
