%!tex root = ./skript.tex

\chapter{Hashing with Chaining}

In hashing with chaining, collisions are resolved by
storing all the entries $(k, v)$ that hash to a given 
location $i$  in the hash table at the same position
$T[i]$. Since we cannot know in advance how many entries
will end up at a given position of the hash table, we need
an auxiliary data structure to store these entries.
Typically, this auxiliary data structure is a singly
linked list, the simplest dynamic data structure that
can store an arbitrary number of entries. In the terminology
of hash tables, such a linked list is called a \emph{chain},
and the collision resolution method is called \emph{chaining}.

Thus, in chaining, we have an array $T$ of size $N$, and each
position $i$ of $T$ stores a reference to (the head of)  a linked list
that stores all the entries $(k, v) \in S$ with $h(k) = i$.
The operations are implemented as follows:
\begin{itemize}
\item \texttt{put}$(k,v)$:
	We set  $i$ to $h(k)$,
	and we go through all the entries that are stored
	in the linked list at $T[i]$, checking for an
        entry with key $k$.
	If we find such an entry, we update the value to
	$v$. Otherwise, we append to the linked list
	at $T[i]$ a new node for the entry $(k, v)$.
\item \texttt{get}$(k)$:
	We set  $i$ to $h(k)$,
	and we go through all the entries that are stored
	in the linked list at $T[i]$, checking for an
        entry with key $k$.
	If we find such an entry, we return the value stored
	with the entry.
	Otherwise, we throw a 
  \texttt{NoSuchElementException}.
\item \texttt{remove}$(k)$:
	We set  $i$ to $h(k)$,
	and we go through all the entries that are stored
	in the linked list at $T[i]$, checking for an
        entry with key $k$.
	If we find such an entry, we remove the corresponding
		node from the linked list.
	Otherwise, we throw a 
  \texttt{NoSuchElementException}.
\end{itemize}

\paragraph{Analysis.}
Now we analyze the performance of hashing with chaining.
First, we note that the space requirement for the whole
structure is $O(N + \text{size}(S))$, where $\text{size}(S)$ denotes
the total space that is needed for the entries in $S$. Thus, as long
as the number of positions in the hash table is not much larger than
the total size for the entries in $S$, hashing with chaining is
quite space efficient.

To analyze the running time, it is necessary to make an assumption
on the hash function $h$. Without any such assumption, we cannot make
a nontrivial statement about the running time, because otherwise, the hash function
$h$ could, e.g., be a constant function that maps all the keys to $0$. In such
a case, the running time of the operations would be $\Omega(n)$, where $n$ is the number
of entries in the structure.

Thus, for our analysis, we will assume that $h$ behaves like a random function. That is,
we imagine that when the hash table is constructed, we create a new hash function
by assigning to each key in $k$ a random position in the hash table, uniformly distributed
and independent of the others. Formally, this means that for every $k \in K$ and for
every $i \in \{0, \dots, N - 1\}$, we have
\[
	\Pr_h[h(k) = i] = 1/N,
\]
independently of the values for the other keys $k' \in K$. This assumption is not very
realistic, since in practice we will never be able to generate and store a completely
random function on the key set with reasonable effort. However, the assumption leads
to clean calculations and a first idea of the performance guarantees that we can expect
from a hash table. It often turns out that similar results can then also be derived
for more realistic ``random'' hash functions (e.g., universal hashing, tabulation hashing),
but with somewhat more complicated calculations.

Thus, consider the following situation: suppose that the hash table
stores a fixed set $S$ of $n$ entries, and suppose that next we perform
an operation on the hash table (get, put, or remove) that involves
a key $k \in K$. The key $k$ may or may not be present in $S$.
We assume further that $S$ and $k$ are independent of the choice of $h$.

Now the cost of the operation on the key $k$ is proportional to the
time to evaluate the hash function $h(k)$, plus the number of entries
in the list that is stored at $T[h(k)]$ (in the worst case,
the operations get, put, remove involve a single scan through the list
at $T[h(k)]$). Assuming that $h$ is a good hash function, the time to
compute $h(k)$ is $O(1)$. We denote the length of the list at $T[h(k)]$
by $L$. Then, the running time of the next operation is $O(1 + L)$.

What can we say about $L$?
We know that $L$ is a random variable that depends on the random choice
of $h$. As we did in the case of skip lists, our way to understand $L$
is by computing its expected value, $\textbf{E}[L]$. This gives us an 
idea of the ``typical'' behavior of $L$.

To compute $\textbf{E}[L]$, we notice that $L$ is exactly the number
of entries $(k', v') \in S$ with $h(k') = h(k)$, i.e., the number of
entries in $S$ that hash to the same position as the given key $k$.

To compute this quantity, we define \emph{indicator random variables}
$L_{k'}$, for every key $k' \in K$. The indicator random
variable $L_{k'}$ is set to  $1$ if and only if $h(k') = h(k)$, and $0$,
otherwise. Then, by our definition of $L$, we have
\[
	L = \sum_{(k', v') \in S} L_{k'},
\]
since the right hand side counts exactly the number of entries in $S$
whose keys are hashed to the same position as $k$.

Using linearity of expectation, we get
\[
	\textbf{E}_h[L] = \textbf{E}_h\left[\sum_{(k', v') \in S} L_{k'}\right]
  = \sum_{(k', v') \in S} \textbf{E}_h\left[L_{k'}\right].
\]
Thus, we need to compute the expected value 
$\textbf{E}_h\left[L_{k'}\right]$, for $k' \in K$. By definition, 
we have
\[
  \textbf{E}_h\left[L_{k'}\right] = 0 \cdot \Pr_h\left[L_{k'} = 0 \right]  + 
1 \cdot \Pr_h\left[L_{k'} = 1 \right]
= \Pr_h\left[L_{k'} = 1 \right]
= \Pr_h\left[h(k') = h(k) \right].
\]
Now, there are two cases: first, if $k' = k$, then clearly 
$Pr_h\left[h(k') = h(k) \right] = 1$. Otherwise, 
if $k' \neq k$, we claim that 
$Pr_h\left[h(k') = h(k) \right] = 1/N$. Indeed, there are $N^2$ possible
ways how $k'$ and $k$ can be hashed by $h$, and since we assume independence,
each such way occurs with probability $1/N^2$. Of these configurations, there are exactly $N$
that lead to a collision.  Thus, 
$Pr_h\left[h(k') = h(k) \right] = N/N^2= 1/N$, as claimed.\footnote{This probability can also
be argued in a slightly different way that avoids a calculation: by assumption, the positions of 
$k'$ and $k'$ in the hash table are independent. Once the position of $k'$ is chosen, there
is exactly one possibility how $k$ can collide with $k'$. Since there are $N$ possible
positions for $k$, the collision probability is thus $1/N$.}
Thus, getting back to $L$, we have
\begin{align*}
	\textbf{E}_h[L] &= 
  \sum_{\substack{(k', v') \in S\\ k' = k}} \textbf{E}_h\left[L_{k'}\right] +
  \sum_{\substack{(k', v') \in S\\ k' \neq k}} \textbf{E}_h\left[L_{k'}\right] \\
	&\leq 1 +
	\sum_{\substack{(k', v') \in S\\ k' \neq k}} \frac{1}{N}\\
	&\leq 1 + \frac{|S|}{N}.
\end{align*}
In conclusion, the expected running time for each operation on our hash table
is $O(1 + |S|/N)$.

The quantity $|S|/N$ is called the \emph{load factor} of the hash table.
It changes over the life-time of the hash table, and it measures how many entries 
there are currently per position in the hash table. Our analysis shows that if the
load factor is $O(1)$, then the expected time for the operations on the hash table
is $O(1)$. The load factor in hashing with chaining can be any 
non-negative rational number between $0$ and $\infty$.

If the load factor is small, the hash table is not space efficient, since many 
positions in the table are unused. If the load factor is too large, the performance
suffers, because the linked lists in the table become too long.
In the literature, it is recommended to keep the load factor between $1$ and $3$. This
can be done by rebuilding the hash table with a new table size, whenever the load
factor becomes too large or too small. This rebuilding operation is called \emph{rehashing}.
It entails the choice of a new compression function (since the table size changes), and it
takes time linear in the size of the hash table and in the current number of entries,
because all the entries must be placed into the new table. As in the case of implementing
a stack with a dynamic array (as discussed in \emph{Konzepte der Programmierung}, one
can show that the total cost for this rehashing operation is negligible to the total cost
of updating the hash table, provided that an appropriate strategy is chosen for determining
the time for the next rehash.

In the next chapter, we will see another collision strategy that is based on
\emph{open addressing}: all entries are stored directly in the hash table,
and if a position is already occupied, we search for a different place for
the entry.
