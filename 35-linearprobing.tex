%!tex root = ./skript.tex

\chapter{Hashing with Linear Probing}

Now, we consider a collision resolution strategy
that falls into the category of \emph{open addressing}.
This means that the position of a entry $(k,v)$ in the hash
table is not only determined by the hash value  for the
key $k$, but also by the entries that are already present in
the table. 

More precisely, in an open addressing scheme, all entries
are stored directly in the hash table $T$. There is no secondary
data structure as in chaining. If we try to insert a new entry $(k,v)$
and the position $T[h(k)]$ is already taken, the entry is stored
in a different location, according to a certain strategy.

In \emph{linear probing}, the strategy consists in a linear scan
through the $T$ starting at $T[h(k)]$, until the first free position
in $T$ is discovered. This is the position where the entry $(k, v)$ 
will be stored. Accordingly, when performing a lookup for a key $k$,
but it is not enough to consider only
the position $T[h(k)]$. Instead, if $T[h(k)]$ is occupied by another
entry, we start a linear scan until either the entry for $k$ is found
or until we encounter the first free position. Only when we see the
free position can we be sure that $k$ is not present in the table.

Now, when performing a deletion, we must be careful. To delete
an entry $(k, v)$, we cannot simply replace the entry $(k,v)$ in
the table by the null element $\perp$. If we did this, it might
happen that the lookups no longer work properly: we might finish
a linear scan, thinking that a key $k$ is not present in $T$, even though
$k$ might appear after a $\perp$ that is due to a deletion
after the entry for $k$ was inserted. (\textbf{TODO}: Example)

To resolve this, we will introduce a special third state that a position $T[i]$
in the hash table $T$ can have. In addition to (i) being \emph{occupied} by an
entry ($T[i] = (k, v)$; and (ii) being \emph{free} ($T[i] = \perp$), we now have
the third possibility (iii) that $T[i]$ has been \emph{deleted} ($T[i] = *$).
We use the third state to mark the situation that $T[i]$ was once occupied
by an entry that has been removed due to a later deletion. This third state plays
different roles during lookups and insertions. For a lookup, it is treated like
an \emph{occupied} position, and the scan continues over the deleted position.
For an insertion, it is treated like a free position, and it can be used for the
entry to be inserted. 

We now give the details for the operations.

\paragraph{Lookup.}
For a lookup of a key $k$, we computed the hash value $h(k)$ for $k$.
Starting from $h(k)$, we scan through $T$ in a linear fashion (wrapping
around if we encounter the end of the array). The scan stops when
either (i) the key is discovered (in which case we return the corresponding
value); or (ii) we encounter the first free position (in which case the key
does not exist in the table); or (iii) we scanned the whole table without
seeing $k$ (in this case, the key is also not present in $T$, and $T$ does not
have any free positions). Note that the table may contain deleted positions, 
they do not need to be handled in a special way by the algorithm.

\begin{verbatim}
get(k):
  pos <- h(k)
  // the scan takes at most N steps
  for i := 1 to N do 
    // if we see an empty position, k is not there
    if (T[pos] == NULL)
      throw NoSuchElementException
    // if we see the key, we return the value
    if (T[pos].k == k)
      return T[pos].v
    // we go to the next position, wrapping around if necessary
    pos <- (pos + 1) mod N
  // we have inspected all positions, k is not there
  throw NoSuchElementException
\end{verbatim}


\paragraph{Put.}
To perform a put on a key $k$, we first compute the hash value $h(k)$.
We start a scan from this position, in order to find the key $k$ or the
first empty position. If, along the way, we see a deleted position, we remember
this position. Now, if we encounter the key $k$, we just update the value $v$
according to the new entry. If we reach an empty position, we know that the
key $k$ is not present, and we insert he new entry either in the empty position,
or in the first deleted position that we have encountered.
\begin{verbatim}
put(k, v):
  pos <- h(k)
  // the first deleted position
  delPos <- NULL
  // the scan takes at most N steps
  for i := 1 to N do
    // if we find k, we update the value and are done
    if (T[pos].k == k)
      T[pos] <- (k,v)  
      return
    // if we see a deleted position, we remember it, if
    // it is the first one
    if (T[pos] == DELETED && delPos == NULL)
      delPos <- pos
    // if we see an empty position, k is not there
    if (T[pos] == NULL) 
      break
    pos <- (pos + 1) mod N
  // At this point, we know that k is not there
  // if there was a deleted position, put k there 
  if (delPos != NULL)
    T[delPos] <- (k,v)
  // if we stopped at a free position, put k there
  else if (T[pos] == NULL) then
    T[pos] <- (k,v)
  // otherwise, there are no free positions.
  else
    throw TableFullException
\end{verbatim}

Alternatively, we can always put the entry into the first deleted
position that we encounter. Then, we must scan further for an
entry for $k$, and mark it as deleted, if necessary.

\begin{verbatim}
put(k, v):
  pos <- h(k)
  // has the entry already been inserted?
  inserted <- False
  // the scan takes at most N steps
  for i := 1 to N do
    if (T[pos].k == k)
      // if k has not been inserted,
      // we add the entry
      if (not inserted)
        T[pos] <- (k,v)  
      // otherwise, we mark the position as deleted
      else
        T[pos] <- DELETED
      return
    // it is the first deleted position, we insert
    // the entry, but we must continue, in case
    // the k key comes later
    if (T[pos] == DELETED && not inserted)
      T[pos] <- (k,v)
      inserted <- True 
    if (T[pos] == NULL) 
      // if we see an emptry position,
      // we add the entry, unless we have
      // already done so
      if (not inserted)
        T[pos] <- (k,v)  
      return
    pos <- (pos + 1) mod N
  // At this point, we have scanned
  // the whole table
  if (not inserted)
    throw TableFullException
\end{verbatim}


\paragraph{Deletion.}
To delete an entry for a key $k$, we proceed as in a lookup. If we find the
entry, we just mark the position as deleted.
\begin{verbatim}
remove(k):
  pos <- h(k)
  for i := 1 to N do
    if (T[pos] == NULL)
      throw NoSuchElementException
    if (T[pos].k == k)
      T[pos] <- DELETED
      return 
    pos <- (pos + 1) mod N
  throw NoSuchElementException
\end{verbatim}

\paragraph{Analysis.}
How good is linear probing? As with chaining, the space requirement is
$O(N + \text{size}(S))$, where $\text{size}(S)$ is the total size
space for the entries in $S$. In practice, the space requirement will
actually be slightly better than for chaining, because there is no overhead
for a secondary data structure (i.e., we do not need any space for maintaining
the linked lists).

To analyze the running time, we again assume that the hash function $h$ behaves
like a random function that assigns to each key $k \in K$ a uniformly random
position in the hash table $T$, independently of the other keys. We again
consider the scenario that the hash table stores a fixed set $S$ of $n$ entries,
and we suppose that next we perform an operation on the hath table that involves
a fixed key $k \in K$. To simplify the proof, we further assume that $N = 6n$,
i.e., that the hash table contains six times as many positions as there are 
entries.\footnote{This assumption can be relaxed, at the cost of a 
slightly more complicated proof.} Finally, we assume that there are no deleted 
positions in the hash table, either because we use the second deletion strategy
that moves entries upon deletion, or because no deletions have taken place so far.

To analyze the cost of the operation on the key $k$, we need the notion of a 
\emph{cluster}: let $\ell \in \{0, \dots, n\}$ and $i \in \{0, \dots, N - 1\}$.
A \emph{cluster} of length $k$ with starting point $i$ in the hash table $T$ is a sequence
$i, i + 1, i + 2, \dots, i + \ell - 1$ of $\ell$ consecutive indices, such that the positions
$T[i], T[i + 1], \dots, T[i + \ell - 1]$ in $\ell$ are all occupied by entries from $S$, while
$T[i - 1]$ and $T[i + \ell]$ are free.\footnote{We take all the indices modulo $N$, e.g.,
$T[-1] = T[N-1]$ and  $T[N]  = T[0]$.}

Now, the time for an operation on the key $k$ is proportional to the time to evaluate the hash function 
$h(k)$, plus the length of the cluster (if any) that contains $h(k)$. That is, 
the running time is $O(1 + \text{length of the cluster that contains $h(k)$})$.

Thus, we need to analyze the length of the clusters in $T$, after $S$ was inserted.
For this, fix a length $\ell$ and a starting position $i$. If $T$ contains a cluster
of length $\ell$ with starting point $i$, then necessarily there must be a subset of $\ell$ entries $S'$
in $S$ such that all keys in $S'$ are hashed by $h$ to $i, i+1, \dots, i + \ell - 1$.\footnote{This is
just a necessary condition, but this is ok, because we are only looking for an upper bound.}
Thus, we can say that 
\[
	\Pr_h[\text{there is a cluster of length $\ell$ that starts at $i$]}
	\leq \binom{n}{\ell} \left(\frac{\ell}{N}\right)^\ell.
\]
This is because there are $\binom{n}{\ell}$ ways to choose the subset $S'$, and for each
key $k'$ in $S'$, the probability that $h(k') \in \{i, i + 1, \dots, i + \ell - 1\}$
is $\ell/N$, independently of the other keys.

Using  the bound $\binom{n}{\ell} \leq (ne/\ell)^\ell$ and the fact that
$N = 6n$, we conclude that
\begin{align*}
	\Pr_h[\text{there is a cluster of length $\ell$ that starts at $i$]}
	&\leq \left(\frac{ne}{\ell}\right)^\ell \cdot \left( \frac{\ell}{6n}\right)^\ell\\
	&\leq  \left(\frac{e}{6}\right)^\ell\\
	&\leq \frac{1}{2^\ell},
\end{align*}
since $e = 2.71\ldots \leq 3$.
Thus, we can also estimate the probability that there is a cluster of length
\emph{at least} $\ell$ that starts of $i$:
\begin{align*}
	\Pr_h[\text{there is a cluster of length at least $\ell$ that starts at $i$]}
	&\leq \sum_{m = \ell}^\infty \frac{1}{2^m}\\
	&= \frac{1}{2^\ell} \cdot \sum_{m = 0}^\infty \frac{1}{2^m}\\
	&= \frac{1}{2^\ell} \cdot 2\\
	&= \frac{1}{2^{\ell - 1}},
\end{align*}
using the geometric series. Thus, the probability that we have a cluster
of length at least $2\log n$ anywhere in the table can be bounded by
\begin{align*}
	&\Pr_h[\text{there is a cluster of length at least $2 \log n$}]\\
	&\leq \sum_{i = 0}^{N-1}
	\Pr_h[\text{there is a cluster of length at least $2 \log n$ that starts at $i$}]\\
	&\leq \sum_{i = 0}^{N-1} \frac{1}{2^{2\log n - 1}}\\
	&= N \cdot \frac{2}{n^2}\\
	&=  \frac{12}{n},
\end{align*}
since $N = 6n$ and $2^{2 \log n } = n^2$. Thus, with probability at least $1 - 12/n$, 
the operations in $T$ take time at most $O(\log n)$.

To analyze the expected time for an operation on the key $k$, we first consider
the case that $k \not\in S$. Then 
the expectd time for the operation on $k$ is proportional to
by the definition of the expected value. 
\begin{align*}
	&\sum_{\ell = 0}^n \ell \cdot \Pr_h[\text{$h(k)$ is in a cluster of length $\ell$}]\\
	\intertext{by distinguishing the starting point of the cluster, we see that this sum
	is equal to}
	&=
	\sum_{\ell = 0}^n \sum_{i = 0}^{N - 1} \ell \cdot 
	\Pr_h[\text{a cluster of length $\ell$ starts at $i$ and $h(k)$ is in this cluster}]\\
	\intertext{
	As we saw, the probability that a cluster
	of length $\ell$ starts at $i$ is at most $2^{-\ell}$. Furthermore, 
	the probability that $h(k)$ falls into this cluster is at most $\ell/N$. Since $k \not\in S$,
	the two events are independent of each other, so
	we can multiply the probabilities, to see that this is at most }
	&\leq
	\sum_{\ell = 0}^n \sum_{i = 0}^{N - 1} \ell \cdot 
	\frac{1}{2^\ell} \cdot \frac{\ell}{N}\\
	\intertext{Since we add the same term $N$ times, this is}
	&=
	\sum_{\ell = 0}^n N \cdot 
	\frac{1}{2^\ell} \cdot \frac{\ell}{N}\\
	\intertext{The $N$ cancels, and we add the same term $\ell$ times, so this
	becomes}
	&=
	\sum_{\ell = 0}^n \frac{\ell^2}{2^\ell} = O(1),
\end{align*}
using the fact that $\sum_{i = 0}^\infty i^2/x^i = O(1)$, for all $x \in (-1, 1)$.
The case that $k \in S$ is similar, only that we need to be a bit more careful in estimating
the probabilites.
\textbf{TODO: Add details}
Thus, the expected time for the operations is $O(1)$.

\paragraph{Conclusion.}
Hashing with linear probing is a simple and efficient hashing scheme that performs very well
in practice. In fact, on modern hardware, hashing with linear probing can be more efficient
than hashing with chaning, because the memory architecture in today's computer systems favors
algorithms that perform a linear scan through memory (as opposed to following a sequence of pointers).

As with chaining, the load factor plays an important role for the efficiency of a hash table.
Since all the entries are stored directly in the hash table, the load factor in linear probing
can be at most $1$. In practice, it seems that it is a good strategy to keep the load factor
at around $0.6$.

There are many variants of the basic idea of linear probing (e.g., 
\emph{quadratic probing}, \emph{double hashing}, etc). The main idea is to
vary the sequence in which the positions in the hash table are scanned for the
first free position, in the hope that it will be less likely that clusters occur.

In the next section, we will see a more recent strategy for resolving collisions 
that tries to combine the advantages of chaining and linear probing.
