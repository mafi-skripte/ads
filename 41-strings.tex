%!tex root = ./skript.tex

\chapter{Strings}

We will now turn our focus to data of a certain form:
\emph{strings}. Strings represent arbitrarily long  sequences 
of \emph{symbols}, e.g., text, source code, DNA information,
or binary data. When dealing with strings, we face a number
of specific algorithmic tasks and structural constraints.
Thus, it make sense to study algorithms and data structures for
strings in their own right. The present section presents only a glimpse
at the wide variety of techniques that are available for dealing
with string data. Later on, particularly in the bioinformatics 
curriculum, we will see much more on this topic.

\paragraph{Formal definitions and notation.}
First, we need to define some basic terms and fix the notation.
When talking about strings, we always have an underlying \emph{alphabet}
that contains all the symbols that may be present in a given string.

An \emph{alphabet} is a non-empty finite set. It is usually denoted by
$\Sigma$. Typical examples of alphabets are $\Sigma = \{0, 1\}$, the
\emph{binary alphabet}, containing only the symbols $0$ and $1$; the
alphabet $\Sigma = \{a, \dots, z\}$, the \emph{Latin alphabet} that consists
of 26 letters; or $\Sigma = \{C, G, T, A\}$, the alphabet that is used to
represent genome information.  

Given an alphabet $\Sigma$, a \emph{string over $\Sigma$} is a finite
(ordered) sequence $w = \sigma_1 \sigma_2 \dots \sigma_n$ of symbols from $\Sigma$.
The number of symbols in $w$ is called the  \emph{length}  of $w$. It is
denoted by $|w|$. We specifically allow the string of length $0$ that
contains no symbols, the \emph{empty string}.
It is denoted by $\varepsilon$.\footnote{It is important to note that $\varepsilon$ is a 
symbol from the mathematical metalanguage. It denotes the string that contains no symbols,
\emph{not} the string that consists of the single symbol $\varepsilon$.} The set of
all strings over $\Sigma$ is denoted by $\Sigma^*$. This includes the empty string.
If we would like to exclude the empty string, the set of all \emph{non-empty} strings
over $\Sigma$ is denoted by $\Sigma^+$.

For example, possible strings over $\Sigma = \{0, 1\}$ are $01001$, $1101001$, or $1$,
where $|01001| = 5$, $|1101001| = 7$,  and $|1| = 1$.
Possible strings over $\Sigma = \{C, G, T, A\}$ are $CGGGT$ or $CGT$, where
$|CGGGT| = 5$ and $|CGT| = 3$.

We always have $|\varepsilon| = 0$, and for any alphabet $\Sigma$, we have
$\varepsilon \in \Sigma^*$, $\varepsilon \not\in \Sigma^+$ and 
$\Sigma^+ = \Sigma^* \setminus \{\varepsilon\}$.
The set 
\[
	\{0, 1\}^* = \{\varepsilon, 0, 1, 00, 01, 10, 11, 000, 001, 010, \dots\}
\]
is the set of all \emph{bit strings} (including the empty bit string), and the set

\[
	\{0, 1\}^+ = \{0, 1, 00, 01, 10, 11, 000, 001, 010, \dots\}
\]
is the set of all \emph{non-empty bit strings}. These two sets play an important
role in computer science, since internally, all information  in a modern computer is
represented by bit strings.

\textbf{TODO:} Definitions substring, subsequence, prefix suffix

\paragraph{Problems on strings.}
In the next chapters, we will study the following chapters for strings:
\begin{itemize}
	\item \textbf{Dictionaries for strings.} We have already talked in great
		detail about the problem of implementing an (ordered) \emph{dictionary}.
		There are special dictionary structures for the case that the key
		set is a set of strings over a fixed alphabet. These
		dictionaries are called \emph{tries} (or \emph{digital search trees}).
                We will discuss them in the next chapter.
	\item \textbf{Efficient storage of strings.} Data in string form is
		typically very large. Given a string, is there a way to
		store $\Sigma$ in such a way that no (or only little) information
		gets lost, while using as little space as possible? Methods
		that address this question are called \emph{data compression}.
		We will see one approach to data compression in Chapter~\ref{ch:compression}.
	\item \textbf{String search.}
		The string search problem is the following: given a string $w$ and
		a pattern $p$, both over the same alphabet, we would like to determine 
		whether $p$ occurs in $w$ as a substring. For example, the
		pattern ``hund''  appears in the string ``schundliteratur'', but
		not in the string ``schuhband''. The problem of string search  comes in 
                many variants
		and needs to be solved in many contexts. We will talk about it in 
                Chapter~\ref{ch:ssearch}.
	\item \textbf{String similarity.} Given two strings $w_1$ and $w_2$ over the same 
alphabet,
		how \emph{similar} are $w_1$ and $w_2$? This is a natural questions that
		occurs, e.g., in bioinformatics when comparing two genome sequences, or
		when comparing two versions of a text file (say., in the linux utility
		\texttt{diff}). In order to answer this question, we first need a formal
		definition of mathematical similarity, and then we need an algorithm
		to implement this definition.  In Chapter~\ref{ch:lcs}, we will see a way how
		this can be done.
\end{itemize}
