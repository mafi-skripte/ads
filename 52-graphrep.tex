%!tex root = ./skript.tex

\chapter{Representing a Graph}

We now discuss several ways to represent a graph in a computer.

\paragraph{Representing the vertices.}
Recall that a graph $G = (V, E)$ consists of a set $V$ of vertices
and a set $E$ of edges. The vertices are an arbitrary nonempty finite set.
For representing graphs in a computer, we will simply assume that
$V = \{1, \dots, n\}$, i.e., that the vertices are numbered from $1$
to $n$, where $n$ is the number of vertices in $G$. Then, we can represent
a vertex by an integer-variable, and we can use it as an index in an array.
If we need to store additional information with the vertices, we can
use an additional array that has, at position $i$, the data for vertex $i$.

The main challenge when using a graph in a computer lies in representing
the edges. There are many ways to do this. Here are three:

\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/21-01_adjacencymatrix_list}
	\end{center}
	\caption{Adjacency maxtrix and adjacency list.}
\end{figure}
\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/21-02_incidence_list}
	\end{center}
	\caption{Incidence list.}
\end{figure}

\paragraph{Adjacency matrix.}
In an \emph{adjacency matrix representation} of a graph, the edges
are represented by a two-dimensional array \texttt{A} that has $n$
columns and $n$ rows, such that the rows and the columns of \texttt{A}
corresponds to the vertices of $V$. We call \texttt{A} the \emph{adjacency matrix}
for $G$.

To represent a simple, undirected, unweighted graph, we let \texttt{A} 
a matrix of Booleans. Then, for any two vertices $v, w \in V$,
we have $\texttt{A}[v, w] = \texttt{A}[w, v] = \texttt{true}$, if there is an
undirected edge between $v$ and $w$, and
$\texttt{A}[v, w] = \texttt{A}[w, v] = \texttt{false}$, otherwise.
Thus, is $G$ is undirected, then \texttt{A}  is a \emph{symmetric} matrix, i.e.,
for any given vertex $v$, the row and the column for $v$ are identical.

To represent a directed, unweighted graph, we again let \texttt{A} be a matrix
of Booleans. Now, for any two vertices $v, w \in V$, we let
$\texttt{A}[v, w] = \texttt{true}$, if there is a directed edge from $v$ to $w$,
and $\texttt{A}[v, w] = \texttt{false}$, otherwise. The matrix \texttt{A} is not
necessarily symmetric. For a vertex $v \in V$, the row $\texttt{A}[v, \star]$ 
corresponds to the outgoing edges from $v$, and the column 
$\texttt{A}[\star, v]$ corresponds to the incoming edges of $v$.

To represent a multigraph, we can let \texttt{A} be a matrix of Integers.
Then, for two vertices $v, w \in V$,  the entry $\texttt{A}[v, w]$ indicates
the \emph{number} of edges from $v$ and $w$ (and $0$, if there are none).
A loop at a vertex $v$ can be represented by a positive entry in $\texttt{A}[v,v]$.
If the multigraph is undirected, then \texttt{A} is symmetric.

For weighted graphs, we can use the adjacency matrix \texttt{A} to store the weights:
we let \texttt{A} be a matrix of integer or floating point values, and 
the entry $\texttt{A}[v, w]$ is the weight of the edge from $v$ to $w$.
In this case, we need a special value to indicate that there is no edge between 
$v$ and $w$, e.g., $-\infty$, or \texttt{NaN}.

\paragraph{Adjacency list.}
In an \emph{adjacency list representation} of a graph, the edges
are represented by an array \texttt{A}. 
The array \texttt{A} has $n$ positions, one for each vertex $v \in V$.

To represent a simple, undirected, unweighted graph,
the  position $\texttt{A}[v]$ for a vertex $v \in V$ stores a linked list that contains exactly
those nodes in $V$ that are adjacent to $v$.
We call $\texttt{A}[v]$ the \emph{adjacency list} for $v$.
In an undirected graph, we have that $w$ appears in the adjacency list $\texttt{A}[v]$
if and only if $v$ appears in the adjacency list $\texttt{A}[w]$.

To represent a directed graph, we can proceed in the same manner, but now the adjacency list
$\texttt{A}[v]$ stores the \emph{outgoing} edges from $v$. For a multigraph, we can allow
a vertex to occur multiple times in an adjacency list. For a weighted graph, we can store the
weights together with the endpoints in the adjacency lists.

\paragraph{Incidence list.}
An \emph{incidence list representation} of a graph is very similar
to an adjacency list.  Again, we have an array \texttt{A}. 
The array \texttt{A} has $n$ positions, one for each vertex $v \in V$.
In addition, for every edge $e \in E$, we have an object that represents
$e$ and that stores additional information for $e$ (e.g., the endpoints,
the name or the edge, a weight, etc.).

For an $v \in V$, the position $\texttt{A}[v]$ of \texttt{A} stores a linked
list that contains references to the objects that represent the edges that
are incident to $v$. In a directed graph, the list 
$\texttt{A}[v]$ contains the \emph{outgoing} edges of $v$.
We do not need any additional data for weighted graphs of mulitgraphs, 
since the relevant information can be stored with the edges.

\paragraph{Discussion.}
All three representations of graphs have advantages and disadvantages.

If we represent the graph as an adjacency matrix, the space requirement is always
$\Theta(|V|^2)$, independent of the number of edges. Adjacency matrices are particularly
useful if we have many queries of the following form:
given two vertices $v, w \in V$, is there an edge between $v$ and $w$? An adjacency matrix can
answer such a query in constant time: simply check the entry $\texttt{A}[v, w]$. On the other
hand, suppose we have many queries of the following form: given a vertex $v \in V$, list all the
vertices that are adjacent to $v$. In an adjacency matrix, this takes $\Theta(|V|)$ steps,
irrespective of the actual number of neighbors: we must scan the whole row $\texttt{A}[v, \star]$,
and collect all vertices $w$ for which $\texttt{A}[v, w]$ is \texttt{true}. If we have a graph
that contains many vertices and in which every vertex has only few neighbors (e.g., in a social graph),
this may be unacceptably slow.

If we represent the graph as an adjacency list or an incidence list, the space requirement is
$O(|V| + |E|)$ because every edge appears in exactly one (undirected case) or two (directed case)
lists. If we want to check whether two given vertices $v, w \in V$ are adjacent, we must scan
the lists. We  can scan the lists for $v$ and $w$ simultaneously. If we find the other
vertex in one of the two lists, we know that the $v$ and $w$ are adjacent. If a list ends, we know
that $v$ and $w$ are not adjacent. Thus, the running time is $O(\min \{\deg(v), \deg(w)\}$. This
can be much slower than in an adjacency list, if the degrees are large. On the other hand,
suppose that we would like to find all neighbors of a given vertex $v$. Now, the information is
directly available in the list for $v$, and we can output the neighbors in time $O(\deg(v))$,
which can be much faster than in an adjacency matrix.\footnote{In undirected graphs, this works only
for the \emph{outgoing} edges for $v$, since the lists do not directly represent the
incoming edges.}T

The differences between the adjacency list representation and the incidence list representation
are small. Adjacency lists have slightly less overhead, because they do not need additional 
objects for the vertices. In contrast, incidence lists are more convenient if we want to store
significant additional information with the edges.
