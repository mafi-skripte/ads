%!tex root = ./skript.tex

\newcommand {\ph} {\varphi}
\newcommand {\wph} {\widehat{\varphi}}

\chapter{AVL-Trees}

We will now look at a way to implement a binary search tree in such a way
that the height of the tree is $O(\log n)$ at all times, where $n$ is the
number of entries that are stored in the tree. The idea is as follows:
in the previous chapter, we saw that a perfect binary search tree always
has height $O(\log n)$. Thus, it would be nice if we could ensure that
we have a perfect binary search tree after each insertion and deletion,
possibly by restructuring the tree (as we did, e.g., with binary heaps in
\emph{Konzepte der Programmierung}). However, it turns out that perfect binary
search trees are too rigid for this. If we try to keep the binary search tree
perfect after each operation, the height would be $O(\log n)$ at all times,
but at the cost of a very expensive restructuring operation, defeating the 
purpose of an overall fast running time for all operations.

The solution is that we do not keep a perfect binary search tree,
but a binary search tree that is only \emph{approximately perfect}.
The trick is now to find a notion of ``approximately perfect'' that
simultaneously ensures that the height is $O(\log n)$ at all times
and that can be maintained with a restructuring operation that is
not too expensive.

One way to achieve this was given by Adelson-Velski and Landis.
They present a variant of binary search trees that are now called
AVL-trees, according to the initials of their inventors.
The idea is to relax the requirement of the perfect binary search
tree that all levels (except for possibly the last one) should be filled
completely to the requirement that for every node $v$ of the tree,
the subtrees of $v$ should have approximately the same height. The precise
definition is as follows:

\textbf{Definition}: Let $T$ be a binary search tree. We say that
$T$ is an \emph{AVL- tree} if for every node $v$ of $T$, the following
holds: let $h_\ell$ be the height of the left subtree of $v$ and 
$h_r$ be the height of the right subtree of $v$ (we define the height
of the empty tree as $-1$. Then, we have
\[
	|h_\ell - h_r| \leq 1
\],
that is, the heights must be either the same or differ by at most $1$.

\begin{center}
  \includegraphics{figs/02-ordereddict/23-avl example.pdf}
\end{center}

We will now see that this property ensures that 
an AVL-tree behaves approximately like a perfect tree.

\begin{theorem}
\label{thm:avl}
  Let $T$ be an AVL-tree with $n$ nodes. Then, $T$ has height $O(\log n)$.
\end{theorem}

\begin{proof}
Let $h$ be a height. Our first goal is to determine a lower bound
on the \emph{minimum} number $F_h$ of nodes that an AVL-tree of height
$h$ can have. In a binary tree that is essentially only a path, this quantity may grow 
only linearly
with $h$, whereas in a perfect tree, this quantity grows exponentially
with $h$. We will show that in an AVL-tree, $F_h$ grows exponentially
with $h$. 
First, we observe that for all $h \geq 1$, we have
\begin{equation}\label{eq:monotone}
  F_h > F_{h - 1},
\end{equation}
Second, we note that 
\begin{equation}\label{eq:base1}
F_0 = 1,
\end{equation} 
because there is only one tree of height $0$,
the tree that consists only one node that is simultaneously
the root and a leaf.
Third, we have
\begin{equation}\label{eq:base2}
F_1 = 2,
\end{equation}
because all three possible binary trees with height $1$ are
valid AVL-trees, and two of them consist of two nodes, while the
third one has three nodes.
Finally, for all $h \geq 2$, we have
\begin{equation}\label{eq:rek}
  F_h = F_{h-2} + F_{h-1} + 1.
\end{equation}
To see why, let $T$ be an AVL-tree of height $h$ that has the minimum
number of nodes.
Then, $T$ consists of a root $v$ and a left
	and a right subtree of of $v$, such that (i) both subtrees have height at most
	$h - 1$, (ii) one subtree has height exactly $h - 1$;
	and (iii) the height of the two subtrees differs
by at most $1$. Since the two subtrees are independent of each other(and of the root),
it must be the case that each subtree contains the minimum number of nodes for its respective
height.
Due to  (\ref{eq:monotone}), the second subtree of $v$ must have height
exactly $h - 1$.

Now, we use induction on $h$ to show that for all 
 $h \in \{0, 1,  \dots, \}$,
we have
\begin{equation}\label{eq:Fh}
  F_h \geq \left(\frac{3}{2}\right)^{h}.
\end{equation}
For the base of the induction, we check the cases
$h \in \{0, 1\}$. By
(\ref{eq:base1}, \ref{eq:base2}), we get
\[
F_0 = 1  \geq \left(\frac{3}{2}\right)^0 \text{ and }
F_1 = 2 \geq \left(\frac{3}{2}\right)^1.
\]

Now, for the inductive step, fix an $h \geq 2$. Using (\ref{eq:rek}) and the
inductive hypothesis, we get
\begin{align*}
F_h = F_{h-2} + F_{h-1} + 1
&\geq \left(\frac{3}{2}\right)^{h-2}
+ \left(\frac{3}{2}\right)^{h-1}
+1\\
&=
\left(\frac{3}{2}\right)^{h-2} \left(1 + \frac{3}{2}\right) + 1\\
&> 
\left(\frac{3}{2}\right)^{h-2}\cdot \frac{9}{4}  \\
&= \left(\frac{3}{2}\right)^{h},
\end{align*}
as desired. Thus, we have proven (\ref{eq:Fh}). 

Now, consider an AVL-tree $T$ with $n$ nodes, 
and suppose that $T$ has height $h$.
Then, by the definition of $F_h$, we have 
$n \geq F_h$.
By  (\ref{eq:Fh}), we also have $F_h \geq (3/2)^h$.
Thus, by combining the inequalities, we derive
 $n \geq (3/2)^h$, or, equivalently, 
$h \leq \log_{3/2} n \approx 1.71 \log_2 n = O(\log n)$, as claimed.
\end{proof}

\paragraph{Rotations on AVL-trees}
Now that we have seen that the AVL-tree property is useful to
ensure that the height of tree is $O(\log n)$, our goal is to extend
the binary search tree operations such that the AVL-tree property is maintained.

For this, we augment our binary search tree $T$ such that in each node $v$ of $T$, we store
the height of the subtree that is rooted at $v$. During an operation (insertion/deletion), 
we can update this
information, and we can use it to check if the AVL-tree property is violated. If this
happens, we need to restructure
the tree in order to restore the AVL-tree property. All of this needs to be done efficiently,
since otherwise AVL-trees do not yield an advantage.

Let us first look at the case of insertions. 
If we perform an insertion, the height of
a subtree an AVL-tree $T$ can only \emph{increase}. We can say more:
an insertion in a binary search tree always adds a new leaf $w$ to the
tree, and the only nodes $v$ whose subtrees can have a larger height
are those nodes that lie on the path $\pi$ from the root of $T$ to $w$
(all other subtrees remain unchanged). Even more, the AVL-tree property
can only be violated at a parent node of a node whose subtree height has changed.
Since the path $\pi$ goes from $w$ to the root of $T$, all these parent nodes 
automatically also lie on the path $\pi$. Thus, it is enough 
to consider only the nodes along $\pi$, and to do something about the
\emph{unbalanced} nodes, i.e., those nodes where the AVL-tree property is violated.

\begin{center}
  \includegraphics{figs/02-ordereddict/23-imbalanced tree example.pdf}
\end{center}

The operations that we use in order to fix the unbalanced nodes are called \emph{rotations}.
There are several kinds of rotations. First, we describe the \emph{left rotation} 
($\ell$-rotation): let $u$ be a node and $v$ be the right child of $v$, and let $T_\alpha$
be the left subtree of $u$ and $T_\beta$ and $T_\gamma$ the left and right subtree of $v$.
In a left rotation on $u$, we change the subtree at $u$ such that $v$ becomes the root,
$u$ becomes the left child of $v$, $T_\alpha$ and $T_\beta$ become the left and right subtree
of $u$, and $T_\gamma$ becomes the right subtree of $v$.
An $\ell$-rotation is \emph{local} (it can be performed by updating only a constant number
of edges in the tree that are close together), and it preserves the binary search tree property.
\begin{center}
  \includegraphics{figs/02-ordereddict/23-left rotation.pdf}
\end{center}

Symmetrically, there is also the \emph{right rotation} ($r$-rotation):
let $u$ be a node and $v$ be the left child of $v$, and let $T_\alpha$ and $T_\beta$
be the left and the right subtree of $v$ , and$T_\gamma$ the right subtree of $u$.
In a right rotation on $u$, we change the subtree at $u$ such that $v$ becomes the root,
$u$ becomes the right child of $v$, $T_\alpha$ becomse the left subtree of $v$, and $T_\beta$
and $T_\gamma$ become the left and right subtree
of $u$.
Again, the $r$-rotation is local
and preserves the binary search tree property.

\begin{center}
  \includegraphics{figs/02-ordereddict/23-right rotation.pdf}
\end{center}

Now, we can examine how an ($\ell$- or $r$-)rotation affects the heights of
the subtrees that are involved. First, we see that the subtrees $T_\alpha$, $T_\beta$,
and $T_\gamma$ are not changed, so their heights remain the same. The subtrees
of interest are those that are rooted at $u$ and at $v$.

Let us consider an $\ell$-rotation, and suppose that the node $u$ is unbalanced
at such a way that $T_\alpha$ has height $h$ and such that $v$ has height $h + 2$, for
some $h$. Let us further assume that $v$ is balanced (fulfills the AVL-property). Then,
one of the subtrees $T_\beta$ and $T_\gamma$ must have height $h + 1$, and the other
subtree must have height $h$ or $h + 1$. The height at $u$ is $h + 3$. 
Now, after performing an $\ell$-rotation, we see that we obtain a balanced situation
in all cases except for the case where $T_\beta$ has height $h + 1$ and $T_\gamma$
has height $h$. A symmetric situation occurs for an $r$ rotation.

\begin{center}
  \includegraphics{figs/02-ordereddict/23-rotation heights.pdf}
\end{center}

Thus, it still remains to find a solution for the final case. 
We look again at the situation for an $\ell$-rotation. The difficult case
is that $T_\alpha$ has height $h$, $T_\beta$ has height $h + 1$, and $T_\gamma$ has
height $h$. In this case, a simple $\ell$-rotation on $u$ will not lead to a balanced
situation. However, we already know that if we were lucky and $T_\beta$ has height $h$ and $T_\gamma$
has height $h + 1$, then an $\ell$-rotation on $u$ would work. Thus, the idea is to create
this situation by performing \emph{first} an $r$-rotation on $v$, and \emph{second} an $\ell$-rotation
of $u$. Such an operation is called an \emph{$r\ell$-rotation}. By examining the cases
we see that an $\ell r$-rotation resolves our final difficult case. Symmetrically, 
there is also an $r \ell$-rotation that resolves the difficult case for the $\ell$-rotation.

Collectively, $\ell$-rotations and $r$-rotations are also called \emph{simple} rotations,
and $r \ell$-rotations and $\ell r$-rotations are also called \emph{double} rotations.

\paragraph{Insertion}
Now, the insertion algorithm for an AVL-tree is as follows: we perform the usual insertion in a binary
search tree, adding a new leaf to the tree. Then, we go up along the path from this leaf to the root,
and whenever we encounter an unbalanced node, we apply the appropriate ($\ell$-, $r$-, $r \ell$-, or
$\ell r$-)rotation to rebalance the subtree at this node. In such a way, whenever we perform
a rotation, we know that all nodes in the subtree (except for the root) are balanced.
Since all the unbalanced nodes must lie on the path to the root, and since rebalancing
does not increase the height of a subtree, this procedure restores the
AVL-tree property. Furthermore, the running time for the insertion is $O(\log n)$, because it consists
of a single walk down and up the tree, with a constant number of operations being performed at
each level of the tree that is visited.

\paragraph{Deletion}
The deletion procedure is similar: we perform the usual deletion in a binary search tree, and
then we go back up to the root, from the location of the node that was deleted, to the root.
Along this path, we perform the appropriate rotations to rebalance the nodes that we encounter.
Now, it may happen that a rotation decreases the height of subtree. However, if this happens,
the height of the subtree before the rotation must have been the same as the height of the subtree
before the deletion, so it cannot happen that we ever create a situation where the heights of two
subtrees of a given node differ by more than $2$. Since the deletion operation consists of a single
pass down and up the tree, performing $O(1)$ operations at each level, the running time is $O(\log n)$.

A closer examination of the algorithms shows that during an insertion, we perform at most one 
(single or double) rotation,
whereas a deletion may perform many  rotations, all the way up the tree.

\begin{center}
  \includegraphics{figs/02-ordereddict/23-inserting, doublerotation, deleting example.pdf}
\end{center}

\paragraph{Conclusion.}
To conclude, AVL-trees are a useful and efficient implementation of the ADT ordered dictionary.
They achieve $O(\log n)$ worst-case running time and have a memory-efficient representation.
On the other hand, the implementation of an AVL-tree can be cumbersome (the rebalancing operations
require a relatively long case distinction, with a lot of symmetric cases that lead easily to
copy-paste errors), and a deletion operation may require a lot of rotations.

Next, we will see a simpler implementation of the ADT ordered dictionary that does not use
search trees, but that needs randomness to be efficient.

\textbf{Bonus remark (for the mathematically inclined reader)}. 
By solving the recurrence relation 
(\ref{eq:base1}, \ref{eq:base2}, \ref{eq:rek}) exactly, we can derive the 
precise result that
$h \leq \log_\ph (n+2) \approx 1.44 \log (n+2)$, 
where $\ph = (1 + \sqrt{5})/2$
is the golden ratio.

Let us see the proof. For this, we use the technique of
\emph{generating functions}.
We represent the sequence $F_h$, $h \geq 0$ as a function,
and we use simple algebraic methods, to obtain an
explicit representation of the elements of the sequence.
Thus,  let us write
\[
F(x) = \sum_{h=0}^\infty F_h x^h.
\]
Now, we can compute
\begin{align*}
	F(x) &=  1 + 2x + \sum_{h = 2}^\infty F_h x^h &&
\text{(by (\ref{eq:base1}, \ref{eq:base2}))}\\
	&=  1 + 2x + \sum_{h = 2}^\infty (F_{h-2}+F_{h-1}+1) x^h &&
\text{(by (\ref{eq:rek}))}\\
     &=  1 + 2x + \sum_{h = 2}^\infty F_{h-2}x^h + 
         \sum_{h=2}^\infty F_{h-1}x^h + \sum_{h=2}^\infty x^h
	 && \text{(splitting the summands)}\\
     &=  1 + 2x + x^2 \left(\sum_{h = 0}^\infty F_{h}x^h\right) + 
         x \left(\sum_{h=1}^\infty F_{h}x^h\right) + 
         \left(\sum_{h=0}^\infty x^h\right) - 1 - x
	 && \text{(shifting the index)}\\
     &=  1 + 2x + x^2 F(x) + 
         x \left(F(x) - 1\right) + \left(\sum_{h=0}^\infty x^h\right) - 1 - x
	 && \text{(definition of $F(x)$)}\\
         &=  x^2 F(x) + x F(x) + \frac{1}{1-x}.
	 && \text{(simplify, geometric series)}\\
\end{align*}
Solving for $F(x)$, we obtain the following functional
equation
\[
  F(x) = \frac{1}{(x^2 + x - 1)(x - 1)}.
\]
Now, write $\ph = (1+\sqrt{5})/2$ and 
$\wph = (1-\sqrt{5})/2$. A quick calculation shows that 
the numbers $\ph$ and $\wph$
have the following properties:
\[
\text{(i) } \ph + \wph = 1; \quad \text{(ii) } \ph - \wph = \sqrt{5}; \quad
\text{(iii) } \ph\cdot \wph = -1; \quad
\text{(iv) } \ph^2 = \ph + 1; \,
\text{ and (v) } \wph^2 = \wph + 1. 
\]
From (i, iii), it follows that $(x+\wph)(x+\ph) = x^2 + x - 1$. Thus,:
\[
  F(x) = \frac{1}{(x+\wph)(x+\ph)(x - 1)}.
\]
Now, the goal is to bring the functional equation for $F(x)$
into an explicit form that yields a formula for the
elements $F_h$.
For this, we would like to write the functional equation
in terms of the geometric series
$1/(1-ax) = \sum_{h=0}^\infty a^hx^h$.
To achieve this, we use the method of
decomposing the functional equation into 
\emph{partial fractions}.
WE write
\[
F(x) =  \frac{1}{(x+\wph)(x+\ph)(x - 1)} = 
\frac{\alpha}{x + \wph} + \frac{\beta}{x + \ph} + \frac{\gamma}{x - 1},
\]
where $\alpha, \beta, \gamma$ need to be determined. 
If we multiply this with 
 $x+\wph$ and set $x = -\wph$,
we obtain, due to (ii, v, iii),
\[
\alpha = \frac{1}{(-\wph + \ph)(-\wph - 1)}
       = \frac{1}{\sqrt{5}(-\wph^2)}
       = -\frac{\ph^2}{\sqrt{5}}.
\]
Similarly, using 
 (ii, iv, iii), we see that
\[
\beta = \frac{1}{(-\ph + \wph)(-\ph - 1)}
       = \frac{1}{-\sqrt{5}(-\ph^2)}
       = \frac{\wph^2}{\sqrt{5}},
\]
and using (iv, v, iii), we see that
\[
\gamma = \frac{1}{(1+\ph)(1+\wph)}
       = \frac{1}{(\ph\wph)^2}
       = 1.
\]
Now, we can derive the partial fraction decomposition for 
 $F(x)$, and we use it as follows:
\begin{align*}
  F(x) &= 
  - \frac{\ph^2}{\sqrt{5}}\cdot\frac{1}{x + \wph}
  + \frac{\wph^2}{\sqrt{5}}\cdot\frac{1}{x + \ph} 
  + \frac{1}{x-1} \\
&= 
  - \frac{\ph^2}{\sqrt{5}}\cdot\frac{1}{\wph(1+ x/\wph)}
  + \frac{\wph^2}{\sqrt{5}}\cdot\frac{1}{\ph(1+x/\ph)} 
  -\frac{1}{1-x} & \text{(rearranging)}\\
&= 
  \frac{\ph^2}{\sqrt{5}}\cdot\frac{\ph}{1- \ph x}
  - \frac{\wph^2}{\sqrt{5}}\cdot\frac{\wph}{1-\wph x} 
  -\frac{1}{1-x} & \text{(by (iii))}\\
  &=
  \frac{\ph^3}{\sqrt{5}}\cdot \sum_{h=0}^\infty \ph^hx^h 
  - \frac{\wph^3}{\sqrt{5}}\cdot \sum_{h=0}^\infty \wph^hx^h 
  -\sum_{h=0}^\infty x^h 
  -\frac{1}{1-x} & \text{(geometric series)}\\
&=
  \sum_{h=0}^\infty \left(
  \frac{\ph^{h+3}-\wph^{h+3}}{\sqrt{5}}
 -1 \right)x^h. & \text{(simplyfying)}
\end{align*}

By comparing the coefficients, we obtain the precise formula
\[
F_h = \frac{1}{\sqrt{5}}\left(\ph^{h+3} - \wph^{h+3}\right) - 1,
\]
for all $h \geq 0$. Thus, we have
\[
  n \geq F_h \geq \frac{1}{\sqrt{5}}\ph^{h+3} - 2
\]
and hence
\[
h \leq \log_{\ph}(n+2) + \log_{\ph}(\sqrt{5})-3,
\]
as claimed.


