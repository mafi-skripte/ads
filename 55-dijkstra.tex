%!tex root = ./skript.tex

\chapter{Shortest Paths in Weighted Graphs}

We now consider the single source shortest path problem
in a \emph{weighted} graph. The setting is as follows:
we are given a directed graph $G = (V, E)$ and a source
vertex $s \in V$. In addition, we have a function $\ell: E \rightarrow \mathbb{R}$ that
assigns a \emph{length} $\ell(e)$ to every edge $e \in E$. The goal is to find, for 
every vertex $v \in V$, a shortest path from $s$ to $v$ \emph{with respect to the
edge lengths $\ell(e)$}.

To make this formal, we need to define the notion of the length of a path.
Let $\pi: s = v_0, v_1, \dots, v_k = t$ be a path from $s$ to $t$ (with $k$
edges). The \emph{length} of $\pi$ is defined as 
\[
	|\pi| = \sum_{i = 1}^k \ell(v_{i - 1}, v_i).
\]
Note that this definition is a generalization of the definition for the unweighted
case in the previous chapter (just set $\ell(e) = 1$, for every edge $e \in E$).
The \emph{distance} between $s$ and $t$ in $G$, denoted by $d_G(s, t)$, is the smallest
length of any path from $s$ to $t$, and a \emph{shortest path} from $s$ to $t$ is a path that
realizes this distance.

In general, a shortest path from $s$ to $t$ may not exist. Suppose that $G$ contains a
vertex $w$ such that (i) $s$ can reach $v$; (ii) $v$ can reach $t$; and (iii) there is a path
$\pi'$ that starts at $v$, ends at $v$, and that has $|\pi'| < 0$. Then, we can make the distance
between $s$ and $t$ arbitrarily small by first going from $s$ to $v$, then following the path $\pi'$ 
as long as we like, and finally going from $v$ to $t$. We call $\pi'$ a 
\emph{negative cycle}.\footnote{More precisely, it should be called a \emph{cycle of negative
total weight}, but we use the more loose terminology for the sake of brevity.} In general, it holds
that if $G$ contains a negative cycle that can be reached from $s$, then the SSSP-problem from
$s$ is not well-defined. Conversely, one can show that if $s$ does not contain a negative cycle that
is reachable from $s$, then all the shortest paths from $s$ are well-defined.\footnote{One could also
address this issue by defining a \emph{path} in such a way that every node may occur at most
once. Then, a shortest path always exists, even in the presence of negative cycles. However, with this
definition, the shortest path problem becomes much more difficult, and we do not have any efficient
algorithms for it.}

Thus, in order to solve the SSSP problem from $s$ in a weighted graph $G$, we need to make sure
that $G$ does not contain a negative cycle that is reachable from $s$. One simple way to do this is 
to consider only graphs where all edges have nonnegative edge weights. 
This is a simple and well-motivated scenario that occurs often in practice (e.g., in a road network,
all edge weights are nonnegative).
We will take this approach in the current chapter.
In the next chapter we will see how to deal with the more general case.

\paragraph{Dijkstra's algorithm.}
We are faced with the following task: given a directed graph $G = (V, E)$, a nonnegative weight function
$\ell: E \rightarrow \mathbb{R}_0^+$ and a source vertex $s$, compute the shortest paths from $s$ to
all other vertices in $V$.

Our approach is to generalize breadth-first search. As we saw, BFS proceeds uniformly in all directions,
starting from $s$. More precisely, BFS maintains a queue $Q$ that contains all the vertices that may 
have edges to vertices that we have not seen yet.  In each step, the BFS extracts a vertex $v$ from 
$Q$ that is closest to $s$,
and it uses $v$ to identify new vertices that are one more step away from $v$. In a weighted graph,
the main difference is that now edges may have different lengths. Thus, when computing the distances of the
new vertices, we should take the edge lengths into account. Also, in order to process the vertices
according to their distance from $s$, we should use a \emph{priority queue} instead of a simple queue.
This leads to the following attempt for a generalized BFS:
\begin{verbatim}
// NEW: Use a priority queue instead of a queue
Q <- new PrioQueue
for v in vertices() do
  v.found <- false
  v.d <- INFTY
  v.pred <- NULL
s.found <- true
s.d <- 0
// NEW: When inserting into Q, we use the distance
//      as the key.
Q.insert(s, s.d)
while not Q.isEmpty() do
  // NEW: We take the vertex in Q with the
  //     smallest distance from s
  v <- Q.extractMin()
  for w in v.neighbors() do
    if not w.found then
      w.found <- true
      // NEW: We compute w.d using the
      //   length of the edge (v, w)
      w.d <- v.d + l(v, w)
      w.pred <- v
      //NEW: Again, we use w.d as the key for w
      Q.insert(w, w.d)
\end{verbatim}
If we try this algorithm on a simple example, we see that is does not quite work.
The problem is that unlike in a BFS, we cannot be sure that the first time
we encounter a vertex $w$ from a vertex $v$, we have also found a shortest path to $w$. If may
be the case that we see $w$ again from another vertex $v'$, and that the shortest path to $w$ goes 
through $v'$ instead of $v$. There is a simple fix: \emph{every time} we see a vertex $w$ from 
a vertex $v$, we check if the path through $v$ is better than the best path we have seen so far. 
If so, we update the distance and the predecessor for 
$w$ to account for the better path. This leads to the following attempt:
\begin{verbatim}
Q <- new PrioQueue
for v in vertices() do
  v.found <- false
  v.d <- INFTY
  v.pred <- NULL
s.found <- true
s.d <- 0
Q.insert(s, s.d)
while not Q.isEmpty() do
  v <- Q.extractMin()
  for w in v.neighbors() do
    if not w.found then
      w.found <- true
      w.d <- v.d + l(v, w)
      w.pred <- v
      Q.insert(w, w.d)
    // NEW: If the path through v is better than
    // the best path we have found so far, we 
    // update the information for w
    else if v.d + l(v, w) < w.d then
      w.d <- v.d + l(v, w)
      w.pred <- v
      Q.decreaseKey(w, w.d)
\end{verbatim}
The operation \texttt{decreaseKey} receives an element that is already present
in the priority queue and a new key that is smaller than the current key associated
with the element, and it updates the key accordingly.

It turns out that this new algorithm is now correct and computes all the shortest
paths for $s$. Before we discuss the algorithm further, we slightly simplify it using
the sentinel technique: instead of having a special \texttt{found}-attribute for all the
vertices, we can indicate that a vertex $v$ has not been discovered yet by setting $v.\texttt{d}$
to $\infty$. This simplifies a few things: (i) we can store all the nodes in the priority queue from
the beginning; and (ii) we do not need to distinguish between the first and the later encounters of a
vertex $w$. The streamlined pseudocode is as follows:
\begin{verbatim}
Q <- new PrioQueue
for v in vertices() do
  // This indicates that v has not been found yet
  v.d <- INFTY
  v.pred <- NULL
  // All vertices are inserted into Q
  Q.insert(v, v.d)
// Initially, only s has been discovered
s.d <- 0
Q.decreaseKey(s, s.d)
while not Q.isEmpty() do
  (*)
  v <- Q.extractMin()
  for w in v.neighbors() do
    // This test now covers both the
    // case that w is encountered for
    // the first time and that we find
    // an improved path to w
    if v.d + l(v, w) < w.d then
      w.d <- v.d + l(v, w)
      w.pred <- v
      Q.decreaseKey(w, w.d)
\end{verbatim}
This streamlined algorithm is called the algorithm of Dijkstra.

\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/24-01_Dijkstra}
	\end{center}
	\caption{Dijkstra example.}
\end{figure}

Let us first analyze the running time of the algorithm:
in the \texttt{for}-loop, every node is inserted into the 
priority queue, the additional time for each iteration of the
loop is constant.. Then, we call a \texttt{decreaseKey}-operation
on $s$. In the \texttt{while}-loop, each node is removed exactly
once from the priority queue. The inner \texttt{for}-loop is executed
exactly once for each directed edge. In each iteration of the
inner \texttt{for}-loop, we may perform one \texttt{decreaseKey}-operation.
The remaining overhead is constant. Thus, the running time of Dijkstra's
algorithm is dominated by running time of the operations on the priority
queue. We have $|V|$ \texttt{insert}s, $|V|$ \texttt{extractMin}s, and
at most $1 + |E|$ \texttt{decreaseKey}s. If the priority queue is implemented
with a binary heap, each operation takes $O(\log |V|)$ time, and the total 
running time is $O(|V| \log |V| + |E| \log |V|)$. This can be improved by using
a more sophisticated priority queue implementation. For example, \emph{Fibonacci 
heaps} support \texttt{insert}s and \texttt{decreaseKey}s in $O(1)$, and
\texttt{extractMin}s in $O(\log |V|)$ \emph{amortized} time. (\textbf{TODO}: 
Explain ``amortized''). This means that the
total running time of Dijkstra's algorithm  with a Fibonacci heap 
is $O(|V|\log |V| + |E|)$.

Now, we prove the correctness of Dijkstra's algorithm. For this, we first 
define for each node $v \in V$ the \emph{tentative path} $\Pi(v)$ as follows:
(i) if $v = s$, then $\Pi(s) = s$; (ii) if $v.\texttt{pred} = \perp$, then
$\Pi(v) = \perp$; (iii) if $v.\texttt{pred} \neq \perp$, then
$\Pi(v) = \Pi(v.\texttt{pred}), v$.
We claim that the algorithm
maintains the following invariant:

\textbf{Invariant}: Whenever the algorithm reaches the beginning of the \texttt{while}-loop
(position (*) in the pseudocode), the following properties hold:
\begin{enumerate}
	\item For every node $v \in V \setminus Q$ and for every node $w \in Q$,
		we have $v.\texttt{d} \leq w.\texttt{d}$. 
	\item For every node $v \in V$, we have that (i) if $\Pi(v) \neq \perp$, then
		$\Pi(v)$ is a path from $s$ to $v$ with length $v.\texttt{d}$ and such
		that all vertices of $\Pi(v)$ except for possibly $v$ lie in $Q \setminus V$; and
		(ii) if $\Pi(v) = \perp$, then
		$v.\texttt{d} = \infty$; 
	\item For every node $v \in V \setminus Q$, we have $v.\texttt{d} = d_G(s, v)$.
\end{enumerate}

Initially, this invariant holds: 
for (1) and (3), note that initially all vertices are in $Q$, so 
$V \setminus Q = \emptyset$.
For (2), 
note that the only node that has $\Pi(v) \neq \perp$ is $s$,
and $\Pi(s) = s$ is a path from $s$ to $s$ of length $s.\texttt{d} = 0$. For all
other nodes $v \in V \setminus \{s\}$, we have $\Pi(v) = \perp$ and $v.d = \infty$.

Now, we show that the invariant is maintained by the \texttt{while}-loop.
First, we consider invariant (1). In one iteration of the \texttt{while}-loop, there
is exactly one node that moves from $Q$ to $V \setminus Q$: 
the node $v$ that is returned by the \texttt{extractMin}-operation.
Since invariant (1) holds at the beginning of the \texttt{while}-loop,
we have  $v'.\texttt{d} \leq v.\texttt{d}$, for all
$v' \in V \setminus Q$. Furthermore, since $v$ is the element in $Q$ with
minimum key, we have that 
$v.\texttt{d} \leq w.\texttt{d}$, for all $w \in Q$. Thus, 
invariant (1) holds immediately after the \texttt{extractMin}-operation. 
We still need to show invariant (1) also holds at the end of the \texttt{while}-loop.
For this, we note that the \texttt{d}-attributes of some nodes $w$ may change during the
inner \texttt{for}-loop. However, an inspection of the code shows that the \texttt{d}-attribute
can only get smaller. Thus, the only critical nodes are nodes $w \in Q$ whose \texttt{d}-attribute
is decreased in the inner \texttt{for}-loop. An inspection of the code shows that
in this case, the new value of $w.\texttt{d}$ will be 
$v.\texttt{d} + \ell(v, w) \geq v.\texttt{d}$, since $\ell(v, w) \geq 0$. This means that 
after the inner \texttt{for}-loop, we still have for all nodes $w \in Q$ that
$v.\texttt{d} \leq  w.\texttt{d}$. It follows that 
invariant (1) also holds at the end of the \texttt{while}-loop.

For invariant (2), 
we first argue that during an iteration of the \texttt{while}-loop, the
\texttt{pred}- and \texttt{d}-attributes change only for nodes $w$ that are still in $Q$.
Indeed, we change the attributes only for nodes $w$ where
$w.\texttt{d} > v.\texttt{d} + \ell(v, w) \geq v.\texttt{d}$, where $v$ is the node that
is returned by the \texttt{extractMin}-operation. By invariant (1), we have 
$v'.\texttt{d} \leq v.\texttt{d}$ for all $v' \in V \setminus Q$, so no such $v'$ is affected.
Now, it follows that invariant (2) is maintained for all nodes $v \in Q \setminus V$, because
the invariant holds at the beginning of the \texttt{while}-loop and nothing changes. Similarly, 
Invariant (2) is also maintained for the node $v$ that is returned by \texttt{extractMin}, 
because it holds at the beginning and the attributes of $v$ are not changed. Finally, let
$w$ be a node from $Q$. There are two possibilities: (i) the \texttt{pred}- and \texttt{d}-attributes
of $w$ are changed in the \texttt{while}-loop. Then, $w.\texttt{pred}$ is set to $v$, and
$w.\texttt{d}$ is set to $v.\texttt{d} + \ell(v, w)$. Invariant (2) holds for $w$, because
we already saw that invariant (2) holds for $v$, and because $v$ has moved to $V \setminus Q$.
(ii) the \texttt{pred}- and \texttt{d}-attributes of $w$ do not change: either 
$w.\texttt{pred} = \infty$, in which case everything holds, or $w.\texttt{pred} \in V \setminus Q$, and 
we saw that in this case nothing changes for $w.\texttt{pred}$, so $\Pi(w)$ is still a path
from $s$ to $w$ of length $w.\texttt{d}$. In summary, we have shown that invariant (2) is
maintained for all vertices.

Finally, we consider invariant (3). Let $w$ be a vertex in $V \setminus Q$, at the beginning of
the \texttt{while}-loop. Since invariant (3) holds for $w$ at the beginning, and since we already saw
that the $\texttt{d}$-attribute of $v$ does not change, we have that invariant (3) also holds
for $w$ at the end. Thus, the main task is to show that $v.\texttt{d} = d_G(s, v)$, where
$v$ is the node returned by \texttt{extractMin}. In the first iteration of the \texttt{while}-loop,
we have $v = s$, and $s.\texttt{d} = 0 = d_G(s, s)$. Thus, suppose that $v \neq s$.
Consider the situation just before $v$ is removed from $Q$. First, suppose that $d_G(s, v) < \infty$,
and let
$\pi$ be an arbitrary path from $s$ to $v$. Since $v \neq s$, we know that $s$ has already
been removed from $Q$. Thus, the path $\pi$ starts at a vertex in $Q \setminus V$, and it
ends at a vertex in $Q$. It follows that $\pi$ must move from $V \setminus Q$ to $Q$.
Let $(a, b)$ the first edge on $\pi$ such that $a \in V \setminus Q$ and $b \in Q$,
and let $\pi'$ be the prefix of $\pi$ that goes from $s$ to $a$.
Then, we have
\begin{align*}
	|\pi| & \geq |\pi'| + \ell(a, b) & \text{(all edge lengths are nonnegative)}\\
	      & \geq d_G(s, a) + \ell(a, b) &\text{($\pi'$ is a path from $s$ to $a$)}\\
	      & = a.\texttt{d} + \ell(a, b) &\text{(Invariant (4) holds for $a$)}\\
	      & \geq b.\texttt{d} &\text{($b$ was considered when $a$ was removed from $Q$)}\\
	      & \geq v.\texttt{d}. &\text{($v$ has a minimum key in $Q$)}\\
\end{align*}
Since $\pi$ is an arbitrary path from $s$ to $v$, it follows that $v.\texttt{d} \leq d_G(s, v)$.
By invariant (2), we have that $\Pi(v)$ is a path from $s$ to $v$ of length $v.\texttt{d}$, and
hence $v.\texttt{d} \geq d_G(s, v)$. Thus, 
we have $v.\texttt{d} = d_G(s, v)$, as desired. Finally, if $d_G(s, v) = \infty$, then there
is no path from $s$ to $v$, and by invariant (2), if follows that $v.\texttt{d} = \infty$.
In any case, invariant (3) holds for $v$.

Since the invariant is maintained throughout the algorithm, it follows that in the end,
Dijkstra's algorithm correctly solves the SSSP-problem. 

\textbf{Remark}:  If we actually want to solve the SPSP-problem for two vertices $s$ and $t$,
we can run Dijkstra's algorithm from $s$ and stop as soon as $t$ is removed from $Q$. Our proof
shows that then we have found a shortest path from $s$ to $t$.

\paragraph{The A*-Algorithm.}
The A*-algorithm is a variant of Dijkstra's algorithm that is applicable for the following
scenario: suppose we are given a directed graph $G = (V, E)$ with nonnegative edge weights
$\ell: E \rightarrow \mathbb{R}_0^+$, and suppose we are given two vertices $s, t \in V$.
We would like to find a shortest path from $s$ to $t$. Now, however, we have some
\emph{additional} information on the shortest paths distances in $G$ that comes from
some understanding of the underlying structure in $G$.

For example, suppose that we would like to find a shortest path in a highway network
$G$, where the vertices are cities. Then, we have a simple and fast way of estimating the distance
between two cities $v$ and $w$ in the road network: 
simply take the geographic distance between two cities. Of course, this is not an accurate estimate. 
The distance in the highway network will usually be larger than the geographic distance. However,
the geographic distance is much faster to compute, and we expect it to be reasonably close.
Even more, when looking for a shortest path from $s$ to $t$, the geographic distance could help
us direct the search towards $t$. In its original version, Dijkstra's algorithm does not
take this information into account, but looks in all directions simultaneously, even those
that take us \emph{away} from the target

\begin{figure}
	\begin{center}
  \includegraphics{figs/05-graphs/24-02-Astar}
	\end{center}
	\caption{A*-example.}
\end{figure}


\textbf{TODO:} Example

The A*-algorithm is supposed to incorporate this additional information into account and to
help direct Dijkstra's algorithm into the direction of the target $t$. For this, we first need
to formalize the notion of \emph{additional information}. This is done in the form of a 
\emph{heuristic function} $h: V \rightarrow \mathbb{R}_0^+$ that assigns a nonnegative number to every
vertex $v \in v$. The interpretation is that $h(v)$ constitutes an estimate of the distance
from $v$ to the target $t$. Using $h$, we would like to modify Dijkstra's algorithm so that
it favors edges that take us closer to $t$ over edges that takes us away from $t$.
For this to work, the heuristic $h$ needs to satisfy a simple mathematical property.

\textbf{Definition}: Let $h: V \rightarrow \mathbb{R}_0^+$ be a heuristic function.
We say that $h$ is \emph{consistent} for $G$ if for every edge $(v, w) \in E$, we have
\[
	h(v) \leq \ell(v, w) + h(w).
\]

Consistency means that the estimated distance to $t$ is compatible with the edges in 
$G$: if there is an edge from $v$ to $w$, then our estimate for the distance from $v$ to
$t$ is not larger than the estimate we get by taking the edge from $v$ to $w$ and adding
the estimated distance from $w$ to $t$.

Now, we can describe the A*-algorithm. Suppose we are given a directed graph $G = (V, E)$
with nonnegative edge weights $\ell: E \rightarrow \mathbb{R}_0^*$ and two vertices
$s, t \in V$. Suppose further that we have a heuristic function $h: V \rightarrow \mathbb{R}^+_0$
that is consistent with $G$.
Then, we define a function $\widehat{\ell}: V \rightarrow \mathbb{R}$ of 
\emph{reduced edge weights} for $G$ as follows: for $(v, w) \in E$, set
\[
	\widehat{\ell}(v, w) = \ell(v, w) + (h(w) - h(v)).
\]
Then, we run Dijkstra's algorithm on $G$, using the reduced edge lengths $\widehat{\ell}$
instead of the original edge lengths $\ell$. We stop the algorithm as soon as $t$ is removed
from the priority queue, and we return the resulting shortest path.

Intuitively, the reduced edge weights are supposed to favor edges that take us closer
to $t$ and to penalize edges that take us away from $t$: the term $h(w) - h(v)$ represents
how much the estimated distance to $t$ changes when following the edge $(v, w)$. If we get
closer to $t$, then $h(w) - h(v) < 0$. If we move away from $t$, then $h(W) - h(v) > 0$.
Thus, in the reduced weights, edges go away from $t$ get longer, and edges that
go towards $t$ get shortest. Since Dijkstra's algorithm discovers the shortest
paths in the order of their lengths, the shortest path to $t$ will be computed earlier.

To verify that A*-search is correct, we must check two things: (i) all the reduced edge weights
are nonnegative, i.e., Dijkstra's algorithm is applicable; and (ii) a shortest path for the
reduced edge weights corresponds to a shortest path from the original edge weights.

For (i), we note that since $h$ is consistent, for every edge $(v, w) \in E$, we have
\[
	h(v) \leq \ell(v, w) + h(w) \,\Rightarrow\,
0 \leq \ell(v, w) + (h(w) - h(v)) = \widehat{\ell}(v, w),
\]
so the nonnegativity of the reduced edge lengths is a direct consequence of consistency.

For (ii), let $pi: s = v_0, v_1, \dots, v_k = t$ be an arbitrary
path from $s$ to $t$. Then, the length if $\pi$ in the reduced weights is
\begin{align*}
	&\sum_{i = 1}^k \widehat{\ell}(v_{i - 1}, v_i)\\
	&=
	\sum_{i = 1}^k \ell(v_{i - 1}, v_i) + (h(v_i) - h(v_{i - 1}))
	& \text{(by definition)}\\
	&=
	\sum_{i = 1}^k \ell(v_{i - 1}, v_i) + 
	\sum_{i = 1}^k h(v_i) -  \sum_{i = 1}^k h(v_{i - 1})
	& \text{(rearranging the sum)}\\
	&=
	\sum_{i = 1}^k \ell(v_{i - 1}, v_i) + 
	\sum_{i = 1}^k h(v_i) -  \sum_{i = 0}^{k-1} h(v_{i})
	& \text{(shifting the index)}\\
	&=
	\sum_{i = 1}^k \ell(v_{i - 1}, v_i) + 
	h(v_k) +
	\sum_{i = 1}^{k - 1} h(v_i) -  \sum_{i = 1}^{k-1} h(v_{i})
	- h(v_0)
	& \text{(rearranging the sums)}\\
	&=
	\sum_{i = 1}^k \ell(v_{i - 1}, v_i) + 
	h(v_k) 
	- h(v_0)
	& \text{(two sums cancel)}\\
	&=
	\sum_{i = 1}^k \ell(v_{i - 1}, v_i) + 
	h(t) 
	- h(s).
	& \text{($v_k = t$ and $v_0 = s$)}
\end{align*}
Thus, the reduced length of $\pi$ is the original length of $\pi$, \emph{plus} the
term $h(t) - h(s)$. Crucially, this term is \emph{the same} for \emph{all} paths from
$s$ to $t$, independent of the inner vertices. Since the lengths of all paths from
$s$ to $t$ are shifted by the same amount $h(t) - h(s)$, their relative order 
remains the same. Thus, a shortest path for the original weights is also a shortest
path for the reduced weights, and vice versa.

Thus, the A*-algorithm is correct, and its worst-case behavior is never worse
than for Dijkstra's algorithm. The hope is that if the heuristic $h$ is chosen
well, then the A*-algorithm will be much faster, because it is goes directly
towards $t$. The challenge now is to find a good heuristic that is (i) meaningful
and (ii) fast to compute.

To extreme heuristics are as follows: (a) the \emph{trivial} heuristic
$h_1: V \rightarrow \mathbb{R}^+_0$ sets $h(v) = 0$, for all $v \in V$. Then,
$h_1$ is consistent, and $h_1$ can be computed very quickly. However,
$h_1$ is not meaningful: the reduced weights are the same as the original
weights, and the A*-algorithm with $h_1$ is the same as Dijkstra's algorithm;
(b) the \emph{perfect} heuristic $h_2: V \rightarrow \mathbb{R}^*_0$ sets
$h_2(v) = d_G(v, t)$, for all $v \in V$. Then, $h_2$ is consistent, and
$h_2$ is meaningful: the reduced length of a shortest path from $s$ to $t$ is
$0$, and it will be discovered almost immediately by the A*-algorithm. However,
$h_2$ is not easy to compute. In fact, if we had $h_2$ available, we would not
need to run the A*-algorithm in the first place.

Thus, we need to find a heuristic that lies somewhere between the two extremes
$h_1$ and $h_2$. This depends on the special structure of the graph at hand, 
and it requires some creativity. As we saw, a classic example of a good heuristics is the 
geographic distance in a road network. Other examples come from the field of artificial
intelligence, where the A*-search is often used to solve planning problems.
\textbf{TODO}: Elaborate on this.
\textbf{TODO}: Say something about A* with an admissible heuristic.
