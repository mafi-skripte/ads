%!tex root = ./skript.tex

\chapter{More Applications of Hash Functions}

One major concept in computer science that we have encountered in this section
are \emph{hash functions}. Hash functions have turned out to be very useful,
with powerful applications well beyond the implementation of hash tables.

Here, we will look at further uses of hash functions. Let us recall the 
main properties of a hash function $h: K \rightarrow \{0, \dots, N-1\}$.
First, the main purpose of $h$ is to map a large
(potentially infinite) universe of keys to a (relatively) small set of numbers
Second, the function value $h(k)$ of a given key $k$ should
be ``easy to compute''.  Third, the hash function should behave \emph{randomly}.
That is, the hash value of a given key $k$ should be ``unpredictable'' and
independent of the other keys. Collisions should be ``unlikely''.

Now, suppose that we have a hash function $h$ whose range is of ``medium size'',
e.g., $N = 2^{128}$ or $N = 2^{256}$. That is, the hash values can be represented
by $128$ or by $256$ bits. Such a range is not useful for creating a hash
table (even with today's hardware, it seems very difficult to create an array
with $2^{256}$ positions). However, since $2^{256}$ is a very large number, 
we see that even though,
mathematically, collisions for $h$ exist (if $K$ is infinite, say), it is very
unlikely that we will ever encounter a collision for $h$ in practice. In other
words, if $N$ is of ``medium size'', and if $h$ behaves sufficiently randomly,
we can consider $h$ to be ``practically injective''.

Since the notion of a ``sufficiently random'' function is not a rigorous
mathematical concept, we can use ideas from complexity theory to replace
the idea of a ``random function'' by the idea of a ``difficult function''. This
leads to the notion of a \emph{cryptographic hash function}.

Suppose that our key set $K$ consists of the set of all finite binary strings,
denoted by $\{0, 1\}^*$. A function $h: K \rightarrow \{0, \dots, N-1\}$
is called a \emph{cryptographic hash function} if (i) given a key $k \in \{0, 1\}^*$,
it is ``easy'' to compute the hash value $h(k)$ ($h$ is easy to compute); (ii) given a hash value 
$x \in \{0, \dots, N- 1\}$, it is ``difficult'' to find a key $k \in \{0, 1\}^*$
such that $h(k) = x$ ($h$ is \emph{resistant} to finding pre-images); and 
(iii) it is ``difficult'' to find two distinct keys $k, k' \in \{0, 1\}^*$, $ \neq k'$, such that
 $h(k) = h(k')$ ($h$ is (strongly) resistant to finding collisions). Note that
(iii) also implies that given a key 
$k \in \{0, 1\}^*$, it is ``difficult'' to compute another key $k' \neq k$ such that
$h(k) = h(k')$ (this property  is also called ``weakly resistant to finding collisions'').\footnote{The
precise definition of a cryptographic hash function is more technical and will not be given here.
It will be treated in a class on cryptography.}

Even though we do not have any theoretical construction of a hash function that 
is \emph{provably} cryptographic, there are several \emph{heuristic} constructions
that are used in practice (e.g., SHA-2, SHA-3).

Cryptographic hash functions can be used to create short (a few bytes) \emph{fingerprints}
for arbitrary data that (i) determine the data uniquely and (ii) do not allow for any conclusions
on the original data. We stress that, mathematically, this is impossible: of course, even
a cryptographic hash function will have collisions, and of course knowing the function value 
determines with preimages are possible. The point is that it should be beyond our computational
capabilities  to obtain this information within a reasonable time frame. Then, it may as
well be impossible, for all practical purposes.

One application of cryptographic hash functions is the \emph{secure storage of
passwords}. Passwords are typically used to authenticate a user who wants to
access a system. For this, the system must know the password for each user. However,
storing these passwords in a plain text file poses a security risk. If an attacker gets 
hold of this file, the attacker could use it to impersonate every single user. Thus,
most systems do not store the passwords directly. Instead, they fix a cryptographic hash
function, and they store only the cryptographic hash values of the password. Once a user
enters a password, the system computes the hash value of the entered password and compares
it to the hash value that is stored in the passwords file. Since cryptographic hash functions
are collision resistant, an adversary will not be able to construct a password that is
accepted, \emph{even if the contents of the passwords file are known}.

A second application of cryptographic hash functions is the generation
of \emph{message digests}. That is, we can use cryptographic hash functions
to certify that two versions of a file are identical. For example, suppose we would
like to download a very large file from  the internet. After the file is downloaded, 
we would like to verify if our version of the file really matches the original.
For this, we can locally compute a cryptographic hash value for the download, and
we can quickly compare it to a hash value that the provider of the file has posted on
the internet. If these two values are identical, we can be fairly certain that our
download matches the original. Since the hash code is very small (only a few bytes),
the check can be performed manually and the data transfer is much more reliable than
for the large file. As a second example, suppose that at a certain deadline, we expect
that a large number of users each would like to upload a large file onto our server (e.g.,
when turning in a large programming assignment). To save server capacity, we could
instead require each user to upload only a cryptographic hash code of the respective
file. The real files can then be uploaded later, after the deadline, at a more
leisurely pace. This saves server capacity. Since cryptographic hash functions are
collision resistant, it is not possible for the users to ``cheat'' and to use the
additional time to produce a (supposedly improved) version of their file that differs
from the version at the deadline.

A third application of cryptographic hash functions is the creation of \emph{hash
references} that make it possible to have \emph{tamper-proof} data structures.
Recall from \emph{Konzepte der Programmierung} that in an object-oriented
programming language, data is modeled by \emph{objects} that reside
in memory. The objects are accessed via \emph{references}, variables in 
the code that can be associated with the different objects at different
times (and the same object can have multiple references associated with it).

In a hash reference, we do not just store a reference to an object, but
also a cryptographic hash value that is derived from all the attributes
of the object. This makes it possible, when using the reference to access
the object, to verify that the attributes of the object have not changed
since the last access (e.g., via a different reference that refers
to the same object). Since the hash value can be small compared to
the original object, it is feasible to store this information directly
with the reference.

A major advantage of hash references is that they can be used in
\emph{chained} data structures. For example, suppose that we have
a singly linked list $L$ whose \texttt{next}-references are implemented
with hash references. For example, suppose that the list consists of
four nodes $n_1, n_2, n_3, n_4$, in this order, and a \texttt{head}-reference
that points to $n_1$. Then, the hash reference  in $n_3$ determines
all the attributes in $n_4$, the hash reference in $n_2$ determines
all the attributes in $n_3$, and so on. Crucially, among the attributes
in $n_3$ that are determined by $n_2$, there is also the hash value for $n_4$.
Thus, it follows that the hash reference in $n_2$ does not only determine the 
attributes in $n_3$, but
also the attributes in $n_4$. Similarly, the hash reference in $n_1$ determines
all the attributes in $n_2$, and, via the hash reference from $n_2$, also all
the attributes in $n_3$ and $n_4$. The hash reference in the \texttt{head}
determines the attributes in the whole list.

In other words, given the hash value for the head reference, we can
verify that the whole linked list has not been changed, by recursively
checking the hash values for all the nodes in the list. Even more, we can
add additional nodes at the head of the list, while keeping the structure intact 
(for adding nodes, we do not need to know the hole list, just the hash value
for the head reference).

A singly linked list that uses hash references is called a \emph{block-chain}.
The main application of a block-chain is to realize a \emph{tamper-free write-only
log}, a sequence of entries that can be extended continuously such that possible
manipulations of past entries can be detected easily (using the supposed history
and the hash value for the current head).

An example application for such a tramper-free write-only log is as follows: suppose
that students take a class in algorithms, and each week, the students are supposed
to hand in an assignment. The assignments are graded, and only if the students achieve
at least 60 \% of the possible assignment credits do they get credit for the class.
Once the class is finished, the result (pass or fail) is entered into the system,
and, for data privacy reasons, the assignment credits are deleted from the central 
server. Now, suppose that a year later a student realizes that the official
entry in the transcript is wrong, and would like to effect a correction. How
can the student support this claim? The official data has been deleted. The student
can show the supposed grades, but how can we verify that they are correct? This problem
can be solved with a block-chain: each time an assignment is graded, the teaching assistant
adds a node to the block-chain with the result. After the class is over, the grades are removed
from the central system, only the hash code for the head remains. This fulfills the privacy
requirement. The students can keep their individual block-chains with the grades. If, later on,
a student wants to contest an erroneous entry, the student can produce the block-chain
as a proof for the claimed mistake. The block-chain can then be checked using the hash code in
the central system, and the goals of verifiability and privacy are achieved.

As second, much more popular application is the implementation of crypto-currencies. For example,
the crypto-currency Bitcoin relies on a block-chain that contains all the transactions in the
bit-coin universe. The hash-references in the Block-chain ensure that even transactions that lie
far in the past can be verified (and are resistant to modification) if the hash-code of the
current node is known.

Talking about Bitcoin, we mention another application of cryptographic hash functions
that is embedded in the Bitcoin block-chain: proof of work. In the bitcoin universe,
any participant can add a new block at the head of the block-chain. The only requirement
is that the hash code for the new had ends with $k$ zeroes, where $k$ is a parameter that
is adjusted regularly. Since the value of the hash code is determined by the contents
of the new head node, we need to add a new attribute to the head node whose value can be chosen
arbitrarily and whose sole purpose is to influence the value of the hash code. This value is
called \emph{nonce}. By choosing the nonce appropriately, it will be possible to make sure that
the hash value for the head block ends with a certain number of zeroes. However, for a cryptographic 
hash function, the only way to find such a value for the nonce is by trying many possible nonces and
by checking the resulting hash values (if the hash function behaves randomly, we expect that we need
to try $2^k$ nonces until we find the right value). Thus, we can only add a new block after investing
enough computational effort into the Bitcoin system (this process is also called \emph{mining}).
